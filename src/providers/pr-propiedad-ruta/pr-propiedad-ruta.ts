import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import {PrRutasProvider} from '../pr-rutas/pr-rutas';
import 'rxjs/add/operator/map';
@Injectable()
export class PrPropiedadRutaProvider {
	principal_url:any;
	constructor(public http: Http, private pr_rutas:PrRutasProvider) {
		this.principal_url=this.pr_rutas.get_route();
	}
	guardar_propiedades_ruta_id_usuario(datos){
    var variable_2=JSON.stringify(datos);
  	var url = this.principal_url+'propiedadesRuta/new';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  get_propiedad_ruta_id_usuario(datos){
    var variable_2=JSON.stringify(datos);
  	var url = this.principal_url+'propiedadesRuta/';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }

  get_images(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'imagenes/showRuta';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }


}
