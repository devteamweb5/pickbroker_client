import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import {PrRutasProvider} from '../pr-rutas/pr-rutas';
import 'rxjs/add/operator/map';
@Injectable()
export class PrLlamadasProvider {
	principal_url:any;
	constructor(public http: Http, private pr_rutas:PrRutasProvider) {
    this.principal_url=this.pr_rutas.get_route();
  }
  guardar_llamadas(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'llamadas/new';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
   get_historial_llamadas_id_usuario(user){
    var url = this.principal_url+`llamadas/${user}/all`;
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

}
