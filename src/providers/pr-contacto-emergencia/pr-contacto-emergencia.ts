import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import {PrRutasProvider} from '../pr-rutas/pr-rutas';
import 'rxjs/add/operator/map';
@Injectable()
export class PrContactoEmergenciaProvider {
	principal_url:any;
	constructor(public http: Http, private pr_rutas:PrRutasProvider) {
		this.principal_url=this.pr_rutas.get_route();
	}
  guardar_contactos_emergencia(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'contactosEmergencia/new';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  get_contactos_emergencia(user){
    var url = this.principal_url+`contactosEmergencia/${user}/all`;
    var response = this.http.get(url).map(res => res.json());
    return response;
  }
  enviar_email_contacto_emergencia(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'contactos_emergencia/enviar_email_contacto_emergencia';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }

  del(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'contactosEmergencia/del';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }

  editar(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'contactosEmergencia/edit';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response; 
  }

}
