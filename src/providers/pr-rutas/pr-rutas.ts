import { Injectable } from '@angular/core';

@Injectable()
export class PrRutasProvider {
  route:any='https://api.inmobiliariarodbrokers.com/web/api/';
  ruta_imagenes='https://api.inmobiliariarodbrokers.com/web/img/allImg/';
  // route:any='http://192.168.0.133:8000/api/';
  // ruta_imagenes='http://192.168.0.133:8000/img/allImg/';
	tipo_seleccion:any;
  constructor() {
    console.log('Hello PrRutasProvider Provider');
  }
  get_route(){
  	return this.route;
  }
  get_ruta_imagenes(){
    return this.ruta_imagenes;
  }

}
