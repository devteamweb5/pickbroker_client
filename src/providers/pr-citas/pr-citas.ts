import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import {PrRutasProvider} from '../pr-rutas/pr-rutas';
import 'rxjs/add/operator/map';
@Injectable()
export class PrCitasProvider {
	principal_url:any;
	constructor(public http: Http, private pr_rutas:PrRutasProvider) {
    this.principal_url=this.pr_rutas.get_route();
  }
  guardar_cita(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'citas/new';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  get_citas_id_cliente(user){
    var url = this.principal_url+`citas/${user}/all`;
    var response = this.http.get(url).map(res => res.json());
    return response;
  }
  enviar_mensaje(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'citas/enviar_mensaje_cliente_socio';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  get_chat(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'mensajes/getChat';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  enviar_mensaje_socio(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'mensajes/new';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  get_mensajes_pendientes(chat){
    var url = this.principal_url+`mensajes/${chat}/all`;
    var response = this.http.get(url).map(res => res.json());
    return response;
  }
  get_contactos(user){
    var url = this.principal_url+`mensajes/${user}/contactos`;
    var response = this.http.get(url).map(res => res.json());
    return response;
  }
  actualizar_mensaje_pendiente(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'citas/actualizar_mensaje_pendiente';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }

}
