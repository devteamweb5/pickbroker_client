var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { PrRutasProvider } from '../pr-rutas/pr-rutas';
import 'rxjs/add/operator/map';
var PrCitasProvider = /** @class */ (function () {
    function PrCitasProvider(http, pr_rutas) {
        this.http = http;
        this.pr_rutas = pr_rutas;
        this.principal_url = this.pr_rutas.get_route();
    }
    PrCitasProvider.prototype.guardar_cita = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'citas/new';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrCitasProvider.prototype.get_citas_id_cliente = function (user) {
        var url = this.principal_url + ("citas/" + user + "/all");
        var response = this.http.get(url).map(function (res) { return res.json(); });
        return response;
    };
    PrCitasProvider.prototype.enviar_mensaje = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'citas/enviar_mensaje_cliente_socio';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrCitasProvider.prototype.get_chat = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'mensajes/getChat';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrCitasProvider.prototype.enviar_mensaje_socio = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'mensajes/new';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrCitasProvider.prototype.get_mensajes_pendientes = function (chat) {
        var url = this.principal_url + ("mensajes/" + chat + "/all");
        var response = this.http.get(url).map(function (res) { return res.json(); });
        return response;
    };
    PrCitasProvider.prototype.get_contactos = function (user) {
        var url = this.principal_url + ("mensajes/" + user + "/contactos");
        var response = this.http.get(url).map(function (res) { return res.json(); });
        return response;
    };
    PrCitasProvider.prototype.actualizar_mensaje_pendiente = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'citas/actualizar_mensaje_pendiente';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrCitasProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http, PrRutasProvider])
    ], PrCitasProvider);
    return PrCitasProvider;
}());
export { PrCitasProvider };
//# sourceMappingURL=pr-citas.js.map