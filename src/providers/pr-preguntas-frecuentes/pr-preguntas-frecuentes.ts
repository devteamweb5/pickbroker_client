import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import {PrRutasProvider} from '../pr-rutas/pr-rutas';
import 'rxjs/add/operator/map';
@Injectable()
export class PrPreguntasFrecuentesProvider {
	principal_url:any;
	constructor(public http: Http, private pr_rutas:PrRutasProvider) {
		this.principal_url=this.pr_rutas.get_route();
	}
	get_preguntas_frecuentes(){
    var url = this.principal_url+'preguntasFrecuentes/';
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

}
