import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import {PrRutasProvider} from '../pr-rutas/pr-rutas';
import 'rxjs/add/operator/map';
@Injectable()
export class PrTarjetasProvider {
	principal_url:any;
  constructor(public http: Http, private pr_rutas:PrRutasProvider) {
    this.principal_url=this.pr_rutas.get_route();
  }
  agregar_tarjetas(datos){
    var variable_2=JSON.stringify(datos);
  	var url = this.principal_url+'tarjetas/agregar_tarjeta_open_pay';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  hacer_cargo_tarjeta(datos){
    var variable_2=JSON.stringify(datos);
  	var url = this.principal_url+'tarjetas/hacer_cargo_tarjeta_openpay';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }

}
