var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { PrRutasProvider } from '../pr-rutas/pr-rutas';
import 'rxjs/add/operator/map';
var PrTipoInmueblePublicacionProvider = /** @class */ (function () {
    function PrTipoInmueblePublicacionProvider(http, pr_rutas) {
        this.http = http;
        this.pr_rutas = pr_rutas;
        this.principal_url = this.pr_rutas.get_route();
    }
    PrTipoInmueblePublicacionProvider.prototype.get_tipo_inmueble = function () {
        var url = this.principal_url + 'tipos_inmueble/';
        var response = this.http.get(url).map(function (res) { return res.json(); });
        return response;
    };
    PrTipoInmueblePublicacionProvider.prototype.get_tipo_publicacion = function () {
        var url = this.principal_url + 'tipos_publicacion/';
        var response = this.http.get(url).map(function (res) { return res.json(); });
        return response;
    };
    PrTipoInmueblePublicacionProvider.prototype.agregar_propiedad_cliente = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'propiedad_deseada_cliente/guardar_propiedad_deseada';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrTipoInmueblePublicacionProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http, PrRutasProvider])
    ], PrTipoInmueblePublicacionProvider);
    return PrTipoInmueblePublicacionProvider;
}());
export { PrTipoInmueblePublicacionProvider };
//# sourceMappingURL=pr-tipo-inmueble-publicacion.js.map