import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import {PrRutasProvider} from '../pr-rutas/pr-rutas';
import 'rxjs/add/operator/map';
@Injectable()
export class PrTipoInmueblePublicacionProvider {
	principal_url:any;
	constructor(public http: Http, private pr_rutas:PrRutasProvider){
		this.principal_url=this.pr_rutas.get_route();
	}
	get_tipo_inmueble(){
    var url = this.principal_url+'tipos_inmueble/';
    var response = this.http.get(url).map(res => res.json());
    return response;
  }
  get_tipo_publicacion(){
    var url = this.principal_url+'tipos_publicacion/';
    var response = this.http.get(url).map(res => res.json());
    return response;
  }
  agregar_propiedad_cliente(datos){
   var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'propiedad_deseada_cliente/guardar_propiedad_deseada';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response; 
  }

}
