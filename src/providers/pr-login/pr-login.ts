import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import {PrRutasProvider} from '../pr-rutas/pr-rutas';
import 'rxjs/add/operator/map';
@Injectable()
export class PrLoginProvider {
  principal_url:any;
  serial:any;
	token:any;
  user;
  constructor(public http: Http, private pr_rutas:PrRutasProvider) {
    this.principal_url=this.pr_rutas.get_route();
    this.user = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
  }
  set_token(value){
    this.token=value;
  }
  get_token(){
    return this.token;
  }
  set_serial(value){
    this.serial=value;
  }
  get_serial(){
    return this.serial;
  }
  registro(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'users/regist';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  crear_cliente_open_pay(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'login/crear_cliente_open_pay';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  login(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'login';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  recuperar_password(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'login/recuperar_password';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }

}
