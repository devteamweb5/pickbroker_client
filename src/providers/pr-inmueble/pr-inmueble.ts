import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import {PrRutasProvider} from '../pr-rutas/pr-rutas';
import 'rxjs/add/operator/map';

@Injectable()
export class PrInmuebleProvider {
	principal_url:any;
	constructor(public http: Http, private pr_rutas:PrRutasProvider) {
		this.principal_url=this.pr_rutas.get_route();
	}
	get_inmuebles_todos(){
    var url = this.principal_url+'publicaciones';
    var response = this.http.get(url).map(res => res.json());
    return response;
  }
  get_favoritos(user){
    var url = this.principal_url+`favoritos/${user}/all`;
    var response = this.http.get(url).map(res => res.json());
    return response;
  }
  set_favoritos(datos){
    var data=JSON.stringify(datos);
    var url = this.principal_url+`favoritos/new`;
    var response = this.http.post(url,data).map(res => res.json());
    return response;
  }
  guardar_inmueble(datos){
    var data=JSON.stringify(datos);
    var url = this.principal_url+'publicaciones/new';
    var response = this.http.post(url,data).map(res => res.json());
    return response;
  }
  get_inmueble_id_usuario(id){
  	var url = this.principal_url+`publicaciones/${id}/misPropiedades`;
    var response = this.http.get(url).map(res => res.json());
    return response;
  }

  get_inmueble(datos){
    var data=JSON.stringify(datos);
    var url = this.principal_url+'publicaciones/getInmueble';
    var response = this.http.post(url,data).map(res => res.json());
    return response;
  }
  buscar_inmueble_parametros(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'publicaciones/buscar';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  buscar_inmueble_id_inmueble(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'inmueble/get_inmuebles_id_inmueble';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  guardar_caracteristica(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'caracteristica_inmueble/guardar_caracteristica_inmueble';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  guardar_servicio(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'servicio_inmueble/guardar_servicio_inmueble';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  calculo_inmueble_yals(datos){
    var data=JSON.stringify(datos);
    var url = this.principal_url+'publicaciones/calcularPrecio';
    var response = this.http.post(url,data).map(res => res.json());
    return response;
  }
  get_imagenes_inmueble(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'inmueble/get_imagenes_caracteristicas_servicios_inmueble';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  get_imagenes_caracteristicas_servicios_inmueble(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'inmueble/get_imagenes_caracteristicas_servicios_inmueble';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  del_imagenes(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'imagenes/del';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  cambiar_status_inmueble(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'publicaciones/statusChange';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  cambiar_status_inmueble_pagado(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'inmueble/cambiar_status_inmueble_pagado';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  editar_inmueble(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'publicaciones/editar';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  eliminar_caracteristica(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'caracteristica_inmueble/eliminar_caracteristica_inmueble';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }
  eliminar_servicio(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'servicio_inmueble/eliminar_servicio_inmueble';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;
  }

}
