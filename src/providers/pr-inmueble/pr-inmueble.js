var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { PrRutasProvider } from '../pr-rutas/pr-rutas';
import 'rxjs/add/operator/map';
var PrInmuebleProvider = /** @class */ (function () {
    function PrInmuebleProvider(http, pr_rutas) {
        this.http = http;
        this.pr_rutas = pr_rutas;
        this.principal_url = this.pr_rutas.get_route();
    }
    PrInmuebleProvider.prototype.get_inmuebles_todos = function () {
        var url = this.principal_url + 'publicaciones';
        var response = this.http.get(url).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.get_favoritos = function (user) {
        var url = this.principal_url + ("favoritos/" + user + "/all");
        var response = this.http.get(url).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.set_favoritos = function (datos) {
        var data = JSON.stringify(datos);
        var url = this.principal_url + "favoritos/new";
        var response = this.http.post(url, data).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.guardar_inmueble = function (datos) {
        var data = JSON.stringify(datos);
        var url = this.principal_url + 'publicaciones/new';
        var response = this.http.post(url, data).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.get_inmueble_id_usuario = function (id) {
        var url = this.principal_url + ("publicaciones/" + id + "/misPropiedades");
        var response = this.http.get(url).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.get_inmueble = function (datos) {
        var data = JSON.stringify(datos);
        var url = this.principal_url + 'publicaciones/getInmueble';
        var response = this.http.post(url, data).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.buscar_inmueble_parametros = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'publicaciones/buscar';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.buscar_inmueble_id_inmueble = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'inmueble/get_inmuebles_id_inmueble';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.guardar_caracteristica = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'caracteristica_inmueble/guardar_caracteristica_inmueble';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.guardar_servicio = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'servicio_inmueble/guardar_servicio_inmueble';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.calculo_inmueble_yals = function (datos) {
        var data = JSON.stringify(datos);
        var url = this.principal_url + 'publicaciones/calcularPrecio';
        var response = this.http.post(url, data).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.get_imagenes_inmueble = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'inmueble/get_imagenes_caracteristicas_servicios_inmueble';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.get_imagenes_caracteristicas_servicios_inmueble = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'inmueble/get_imagenes_caracteristicas_servicios_inmueble';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.del_imagenes = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'imagenes/del';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.cambiar_status_inmueble = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'publicaciones/statusChange';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.cambiar_status_inmueble_pagado = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'inmueble/cambiar_status_inmueble_pagado';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.editar_inmueble = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'publicaciones/editar';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.eliminar_caracteristica = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'caracteristica_inmueble/eliminar_caracteristica_inmueble';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider.prototype.eliminar_servicio = function (datos) {
        var variable_2 = JSON.stringify(datos);
        var url = this.principal_url + 'servicio_inmueble/eliminar_servicio_inmueble';
        var response = this.http.post(url, variable_2).map(function (res) { return res.json(); });
        return response;
    };
    PrInmuebleProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http, PrRutasProvider])
    ], PrInmuebleProvider);
    return PrInmuebleProvider;
}());
export { PrInmuebleProvider };
//# sourceMappingURL=pr-inmueble.js.map