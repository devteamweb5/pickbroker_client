import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import {PrRutasProvider} from '../pr-rutas/pr-rutas';
import 'rxjs/add/operator/map';
@Injectable()
export class PrServicioProvider{
	principal_url:any;
  constructor(public http: Http, private pr_rutas:PrRutasProvider) {
    this.principal_url=this.pr_rutas.get_route();
  }
  get_servicio(){
  	var url = this.principal_url+'tipos_servicio/';
    var response = this.http.get(url).map(res => res.json());
    return response;
  }
  get_servicio_lista(){
    var url = this.principal_url+'servicios/';
    var response = this.http.get(url).map(res => res.json());
    return response;
  }
  get_det_servicio(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'servicio/get_det_servicio';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;  
  }
  get_categoria_servicio(){
    var url = this.principal_url+'categoriaServicios/';
    var response = this.http.get(url).map(res => res.json());
    return response;  
  }
  guardar_imagen_inmueble(datos){
    var variable_2=JSON.stringify(datos);
    var url = this.principal_url+'inmueble/guardar_imagen_inmueble';
    var response = this.http.post(url,variable_2).map(res => res.json());
    return response;  
  }

}
