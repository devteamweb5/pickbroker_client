import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , Events} from 'ionic-angular';
import {PrInmuebleProvider} from '../../providers/pr-inmueble/pr-inmueble';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
	inmuebles:any;
  ruta_imagenes:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public pr_inmueble:PrInmuebleProvider,
    public pr_alert_toast:PrAlertToastProvider,
    public pr_rutas:PrRutasProvider,
    public events:Events) {

    this.ruta_imagenes=this.pr_rutas.get_ruta_imagenes();
  }
  ionViewWillEnter(){
let user=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
console.log(user);
    let item=1;
    this.events.publish('user:created', item);
  }
  ionViewDidLoad(){
    console.log('ionViewDidLoad HomePage');
    this.get_todos_inmuebles();
    let item=1;
    this.events.publish('user:created', item);
  }
  get_todos_inmuebles(){
    let mensaje='Cargando Propiedades';
    this.pr_alert_toast.show_loading(mensaje);
    this.pr_inmueble.get_inmuebles_todos().subscribe(
      result => {
        this.inmuebles = result;
        console.log(result);
        this.pr_alert_toast.dismis_loading();

        /*
	      let resultado=pr_inmueble;
	      if(resultado.mensaje=='inmuebles_registrados'){
					let data=resultado.data;    
            this.inmuebles=data;
            console.log(this.inmuebles);
	      }
        */
      },
      err => {console.log(err);
      },
      );
  }
  get_det_inmueble(item){
    let mensaje='Cargando';
    this.pr_alert_toast.show_loading(mensaje);
    let data_det_inmuebles={
      det_inmuebles:item.inmueble,
      imagenes:item.imagenes,
      caracteristicas:item.inmueble.caracteristicas ? item.inmueble.caracteristicas.split(',') : [] ,
      servicios:item.inmueble.servicios ? item.inmueble.servicios.split(',') : []
    }
    localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
    this.navCtrl.push('DetInmueblePage');
    /*
    this.pr_inmueble.get_imagenes_caracteristicas_servicios_inmueble(data_inmueble).subscribe(
      pr_inmueble => {
        this.pr_alert_toast.dismis_loading();
        let resultado=pr_inmueble;
        if(resultado.status==true){
          let data_det_inmuebles={
            det_inmuebles:data,
            imagenes:resultado.imagenes,
            caracteristicas:resultado.caracteristicas,
            servicios:resultado.servicios
          }
          localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
          this.navCtrl.push('DetInmueblePage');
        }
      },
      err => {console.log('el error '+err);
    },
    );
    */
  }
  ver_mapa(){
    this.navCtrl.setRoot('HomeMapaPage');
  }

  filtro(){
    this.navCtrl.push('BuscarPropiedadPage')
  }


}
