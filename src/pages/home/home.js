var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { PrInmuebleProvider } from '../../providers/pr-inmueble/pr-inmueble';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrRutasProvider } from '../../providers/pr-rutas/pr-rutas';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams, pr_inmueble, pr_alert_toast, pr_rutas, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_inmueble = pr_inmueble;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_rutas = pr_rutas;
        this.events = events;
        this.ruta_imagenes = this.pr_rutas.get_ruta_imagenes();
    }
    HomePage.prototype.ionViewWillEnter = function () {
        var user = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        console.log(user);
        var item = 1;
        this.events.publish('user:created', item);
    };
    HomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HomePage');
        this.get_todos_inmuebles();
        var item = 1;
        this.events.publish('user:created', item);
    };
    HomePage.prototype.get_todos_inmuebles = function () {
        var _this = this;
        var mensaje = 'Cargando Propiedades';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_inmueble.get_inmuebles_todos().subscribe(function (result) {
            _this.inmuebles = result;
            console.log(result);
            _this.pr_alert_toast.dismis_loading();
            /*
              let resultado=pr_inmueble;
              if(resultado.mensaje=='inmuebles_registrados'){
                        let data=resultado.data;
                this.inmuebles=data;
                console.log(this.inmuebles);
              }
            */
        }, function (err) {
            console.log(err);
        });
    };
    HomePage.prototype.get_det_inmueble = function (item) {
        var mensaje = 'Cargando';
        this.pr_alert_toast.show_loading(mensaje);
        var data_det_inmuebles = {
            det_inmuebles: item.inmueble,
            imagenes: item.imagenes,
            caracteristicas: item.caracteristicas ? item.caracteristicas.split(',') : [],
            servicios: item.servicios ? item.servicios.split(',') : []
        };
        localStorage.setItem('det_inmueble', JSON.stringify(data_det_inmuebles));
        this.navCtrl.push('DetInmueblePage');
        /*
        this.pr_inmueble.get_imagenes_caracteristicas_servicios_inmueble(data_inmueble).subscribe(
          pr_inmueble => {
            this.pr_alert_toast.dismis_loading();
            let resultado=pr_inmueble;
            if(resultado.status==true){
              let data_det_inmuebles={
                det_inmuebles:data,
                imagenes:resultado.imagenes,
                caracteristicas:resultado.caracteristicas,
                servicios:resultado.servicios
              }
              localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
              this.navCtrl.push('DetInmueblePage');
            }
          },
          err => {console.log('el error '+err);
        },
        );
        */
    };
    HomePage.prototype.ver_mapa = function () {
        this.navCtrl.push('HomeMapaPage');
    };
    HomePage.prototype.filtro = function () {
        this.navCtrl.push('BuscarPropiedadPage');
    };
    HomePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-home',
            templateUrl: 'home.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            PrInmuebleProvider,
            PrAlertToastProvider,
            PrRutasProvider,
            Events])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.js.map