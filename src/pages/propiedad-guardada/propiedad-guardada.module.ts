import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropiedadGuardadaPage } from './propiedad-guardada';

@NgModule({
  declarations: [
    PropiedadGuardadaPage,
  ],
  imports: [
    IonicPageModule.forChild(PropiedadGuardadaPage),
  ],
})
export class PropiedadGuardadaPageModule {}
