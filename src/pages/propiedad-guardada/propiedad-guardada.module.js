var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropiedadGuardadaPage } from './propiedad-guardada';
var PropiedadGuardadaPageModule = /** @class */ (function () {
    function PropiedadGuardadaPageModule() {
    }
    PropiedadGuardadaPageModule = __decorate([
        NgModule({
            declarations: [
                PropiedadGuardadaPage,
            ],
            imports: [
                IonicPageModule.forChild(PropiedadGuardadaPage),
            ],
        })
    ], PropiedadGuardadaPageModule);
    return PropiedadGuardadaPageModule;
}());
export { PropiedadGuardadaPageModule };
//# sourceMappingURL=propiedad-guardada.module.js.map