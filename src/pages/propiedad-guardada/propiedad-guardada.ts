import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PropiedadGuardadaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-propiedad-guardada',
  templateUrl: 'propiedad-guardada.html',
})
export class PropiedadGuardadaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PropiedadGuardadaPage');
  }
  home(){
  	this.navCtrl.setRoot('HomePage');
  }

}
