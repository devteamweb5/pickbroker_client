import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PropiedadRutaGuardadaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-propiedad-ruta-guardada',
  templateUrl: 'propiedad-ruta-guardada.html',
})
export class PropiedadRutaGuardadaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams){
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad PropiedadRutaGuardadaPage');
  }
  home(){
  	this.navCtrl.setRoot('HomePage');
  }

}
