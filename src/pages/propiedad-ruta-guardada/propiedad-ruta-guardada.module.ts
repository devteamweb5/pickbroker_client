import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropiedadRutaGuardadaPage } from './propiedad-ruta-guardada';

@NgModule({
  declarations: [
    PropiedadRutaGuardadaPage,
  ],
  imports: [
    IonicPageModule.forChild(PropiedadRutaGuardadaPage),
  ],
})
export class PropiedadRutaGuardadaPageModule {}
