var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrCitasProvider } from '../../providers/pr-citas/pr-citas';
var CitaAgendarPage = /** @class */ (function () {
    function CitaAgendarPage(navCtrl, navParams, pr_alert_toast, pr_citas) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_citas = pr_citas;
        this.usuario = {
            nombre: '',
            telf: '',
            login: '',
        };
        this.datos_guardar = {
            user: '',
            socio: '',
            inmueble: '',
            comentario: '',
            fecha: '',
            hora: '',
            lat_long: '',
        };
        var tzoffset = (new Date()).getTimezoneOffset() * 60000;
        var fecha_actual = (new Date(Date.now() - tzoffset)).toISOString().slice(0, -1) + 'Z';
        this.fecha = fecha_actual;
    }
    CitaAgendarPage.prototype.ionViewDidLoad = function () {
        var data = JSON.parse(localStorage.getItem('det_inmueble'));
        console.log(data);
        var data_inmueble = JSON.parse(localStorage.getItem('det_inmueble'));
        console.log(data_inmueble);
        console.log('ionViewDidLoad CitaAgendarPage');
        this.get_datos_cita();
    };
    CitaAgendarPage.prototype.get_datos_cita = function () {
        var data = JSON.parse(localStorage.getItem('det_inmueble'));
        var datos_usuario = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        console.log(datos_usuario);
        for (var _i = 0, datos_usuario_1 = datos_usuario; _i < datos_usuario_1.length; _i++) {
            var value = datos_usuario_1[_i];
            this.usuario.nombre = value.nombre;
            this.usuario.telf = value.telf;
            this.usuario.login = value.login;
        }
        console.log(this.usuario);
    };
    CitaAgendarPage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    CitaAgendarPage.prototype.guardar = function () {
        var _this = this;
        var data_inmueble = JSON.parse(localStorage.getItem('det_inmueble'));
        var datos_usuario = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        console.log(data_inmueble);
        if (this.datos_guardar.fecha == '') {
            var mensaje = 'Ingrese una fecha';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.datos_guardar.hora == '') {
            var mensaje = 'Ingrese una hora';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else {
            var fecha_explota = this.fecha.split('T');
            if (this.datos_guardar.fecha == fecha_explota['0'] || this.datos_guardar.fecha < fecha_explota['0']) {
                var mensaje = 'Sólo se permiten citas con 24 hora de anticipación';
                this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }
            else {
                this.datos_guardar.user = datos_usuario.id;
                this.datos_guardar.socio = data_inmueble.det_inmuebles.user.id;
                this.datos_guardar.inmueble = data_inmueble.det_inmuebles.id;
                this.datos_guardar.lat_long = data_inmueble.det_inmuebles.latLong;
                /*
                this.datos_guardar.direccion_2=data_inmueble.det_inmuebles.direccion;
                this.datos_guardar.nombre=this.usuario.nombre;
                this.datos_guardar.telf=this.usuario.telf;
                this.datos_guardar.id_inmueble=data_inmueble.det_inmuebles.id_inmueble;
                this.datos_guardar.id_usuario_agente=data_inmueble.det_inmuebles.id_usuario_agente;
                console.log(this.datos_guardar);
                debugger
                    */
                var mensaje = 'Agendando';
                this.pr_alert_toast.show_loading(mensaje);
                this.pr_citas.guardar_cita(this.datos_guardar).subscribe(function (result) {
                    _this.pr_alert_toast.dismis_loading();
                    var mensaje = 'Cita guardada, espere a que el agente Confirme la cita';
                    _this.pr_alert_toast.mensaje_toast_pie(mensaje);
                    _this.navCtrl.push('CitaEnviadaPage');
                    /*
                  let resultado=pr_citas;
                  if(resultado.status==true){
                            let mensaje='Cita guardada, espere a que el agente Confirme la cita';
                            this.pr_alert_toast.mensaje_toast_pie(mensaje);
                            this.navCtrl.push('CitaEnviadaPage');
                  }
                  */
                }, function (err) {
                    console.log(err);
                });
            }
        }
    };
    CitaAgendarPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-cita-agendar',
            templateUrl: 'cita-agendar.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrAlertToastProvider, PrCitasProvider])
    ], CitaAgendarPage);
    return CitaAgendarPage;
}());
export { CitaAgendarPage };
//# sourceMappingURL=cita-agendar.js.map