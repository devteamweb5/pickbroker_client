import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrCitasProvider} from '../../providers/pr-citas/pr-citas';

@IonicPage()
@Component({
  selector: 'page-cita-agendar',
  templateUrl: 'cita-agendar.html',
})
export class CitaAgendarPage {
	usuario:any={
		nombre:'',
		telf:'',
		login:'',
	};
	datos_guardar={
		user:'',
		socio:'',
		inmueble:'',
		comentario:'',
		fecha:'',
		hora:'',
		lat_long:'',
	}
	fecha:any;
	hora:'00:00';
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_alert_toast:PrAlertToastProvider, public pr_citas:PrCitasProvider) {
  	var tzoffset = (new Date()).getTimezoneOffset() * 60000;
    let fecha_actual=(new Date(Date.now() - tzoffset)).toISOString().slice(0, -1) + 'Z';
    this.fecha=fecha_actual;
  }

  ionViewDidLoad() {
  	let data=JSON.parse(localStorage.getItem('det_inmueble'));
  	console.log(data);
  	let data_inmueble=JSON.parse(localStorage.getItem('det_inmueble'));
  	console.log(data_inmueble);
    console.log('ionViewDidLoad CitaAgendarPage');
    this.get_datos_cita();
  }
  get_datos_cita(){
  	let data=JSON.parse(localStorage.getItem('det_inmueble'));
  	let datos_usuario=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));	
  	console.log(datos_usuario);
  	 for(let value of datos_usuario) {
	  	this.usuario.nombre=value.nombre;
	  	this.usuario.telf=value.telf;
	  	this.usuario.login=value.login;
  	 }
  	console.log(this.usuario);
  }
  atras(){
  	this.navCtrl.pop();
  }
  guardar(){
  	let data_inmueble=JSON.parse(localStorage.getItem('det_inmueble'));
  	let datos_usuario=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
  	console.log(data_inmueble);
  	if(this.datos_guardar.fecha=='') {
  		let mensaje='Ingrese una fecha';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}else if(this.datos_guardar.hora==''){
  		let mensaje='Ingrese una hora';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}else{
	  	let fecha_explota=this.fecha.split('T');
	  	if(this.datos_guardar.fecha==fecha_explota['0'] ||this.datos_guardar.fecha<fecha_explota['0']){
  		let mensaje='Sólo se permiten citas con 24 hora de anticipación';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}else{
  		this.datos_guardar.user = datos_usuario.id;
  		this.datos_guardar.socio = data_inmueble.det_inmuebles.user.id;
  		this.datos_guardar.inmueble = data_inmueble.det_inmuebles.id;
	  	this.datos_guardar.lat_long=data_inmueble.det_inmuebles.latLong;
  		/*
	  	this.datos_guardar.direccion_2=data_inmueble.det_inmuebles.direccion;
	  	this.datos_guardar.nombre=this.usuario.nombre;
	  	this.datos_guardar.telf=this.usuario.telf;
	  	this.datos_guardar.id_inmueble=data_inmueble.det_inmuebles.id_inmueble;
	  	this.datos_guardar.id_usuario_agente=data_inmueble.det_inmuebles.id_usuario_agente;
	  	console.log(this.datos_guardar);
	  	debugger
			*/
	  	let mensaje='Agendando';
	  	this.pr_alert_toast.show_loading(mensaje);
	  	 this.pr_citas.guardar_cita(this.datos_guardar).subscribe(
	  	    result => {
	  	    	this.pr_alert_toast.dismis_loading();
	  	    	let mensaje='Cita guardada, espere a que el agente Confirme la cita';
  					this.pr_alert_toast.mensaje_toast_pie(mensaje);
	  				this.navCtrl.push('CitaEnviadaPage');
	  	    	/*
	  	      let resultado=pr_citas;
	  	      if(resultado.status==true){
	  					let mensaje='Cita guardada, espere a que el agente Confirme la cita';
  						this.pr_alert_toast.mensaje_toast_pie(mensaje);
	  					this.navCtrl.push('CitaEnviadaPage');
	  	      }
	  	      */
	  	    },
	  	    err => {console.log(err);
	  	    },
	  	  );
  		}
		}
	}
}
