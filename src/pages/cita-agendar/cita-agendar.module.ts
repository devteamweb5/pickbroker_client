import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CitaAgendarPage } from './cita-agendar';

@NgModule({
  declarations: [
    CitaAgendarPage,
  ],
  imports: [
    IonicPageModule.forChild(CitaAgendarPage),
  ],
})
export class CitaAgendarPageModule {}
