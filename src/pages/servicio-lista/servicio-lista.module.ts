import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServicioListaPage } from './servicio-lista';

@NgModule({
  declarations: [
    ServicioListaPage,
  ],
  imports: [
    IonicPageModule.forChild(ServicioListaPage),
  ],
})
export class ServicioListaPageModule {}
