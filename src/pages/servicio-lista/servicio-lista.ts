import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrServicioProvider} from '../../providers/pr-servicio/pr-servicio';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';

@IonicPage()
@Component({
  selector: 'page-servicio-lista',
  templateUrl: 'servicio-lista.html',
})
export class ServicioListaPage {
	servicio:any;
	data_det_servicio:any;
	ruta_imagenes:any;
  categorias_servicios:any;
  id_categoria:any=0;
  message;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public pr_alert_toast:PrAlertToastProvider,
    public pr_servicio:PrServicioProvider,
    public pr_rutas:PrRutasProvider) {
    this.ruta_imagenes=this.pr_rutas.get_ruta_imagenes();
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad ServicioListaPage');
    this.get_categoria_servicios();
    this.get_servicio();
  }

  atras(){
    this.navCtrl.setRoot('HomePage');
  }

  get_categoria_servicios(){
    this.pr_servicio.get_categoria_servicio().subscribe(
      result => {
        this.categorias_servicios=result;
      },
      err => {console.log(err);
    },
    );
  }

  get_servicio(){
    this.pr_servicio.get_servicio_lista().subscribe(
      result => {
        this.servicio=result
      },
      err => {console.log('el error '+err);
    },
    );
  }

  get_det_servicio(item){
    localStorage.setItem('det_servicio',JSON.stringify(item));
    this.navCtrl.push('ServicioDetallePage');
  }

  getServicios(){
    let registro = this.servicio.find(result => result.categoria.id == this.id_categoria);
    console.log(registro);
    if (!registro) {
      this.message = 'No hay servicios con esta categoría';
    }else{
      this.message = null;
    }
    // console.log(this.servicio.includes(res => {return res.categoria.id == 2}));
    console.log('obteniendo servicios')
  }

}
