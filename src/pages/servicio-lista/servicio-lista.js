var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrServicioProvider } from '../../providers/pr-servicio/pr-servicio';
import { PrRutasProvider } from '../../providers/pr-rutas/pr-rutas';
var ServicioListaPage = /** @class */ (function () {
    function ServicioListaPage(navCtrl, navParams, pr_alert_toast, pr_servicio, pr_rutas) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_servicio = pr_servicio;
        this.pr_rutas = pr_rutas;
        this.id_categoria = 0;
        this.ruta_imagenes = this.pr_rutas.get_ruta_imagenes();
    }
    ServicioListaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ServicioListaPage');
        this.get_categoria_servicios();
        this.get_servicio();
    };
    ServicioListaPage.prototype.atras = function () {
        this.navCtrl.setRoot('HomePage');
    };
    ServicioListaPage.prototype.get_categoria_servicios = function () {
        var _this = this;
        this.pr_servicio.get_categoria_servicio().subscribe(function (result) {
            _this.categorias_servicios = result;
        }, function (err) {
            console.log(err);
        });
    };
    ServicioListaPage.prototype.get_servicio = function () {
        var _this = this;
        this.pr_servicio.get_servicio_lista().subscribe(function (result) {
            _this.servicio = result;
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    ServicioListaPage.prototype.get_det_servicio = function (item) {
        localStorage.setItem('det_servicio', JSON.stringify(item));
        this.navCtrl.push('ServicioDetallePage');
    };
    ServicioListaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-servicio-lista',
            templateUrl: 'servicio-lista.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            PrAlertToastProvider,
            PrServicioProvider,
            PrRutasProvider])
    ], ServicioListaPage);
    return ServicioListaPage;
}());
export { ServicioListaPage };
//# sourceMappingURL=servicio-lista.js.map