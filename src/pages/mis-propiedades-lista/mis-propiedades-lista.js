var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrInmuebleProvider } from '../../providers/pr-inmueble/pr-inmueble';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrRutasProvider } from '../../providers/pr-rutas/pr-rutas';
var MisPropiedadesListaPage = /** @class */ (function () {
    function MisPropiedadesListaPage(navCtrl, navParams, pr_inmueble, pr_alert_toast, pr_rutas) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_inmueble = pr_inmueble;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_rutas = pr_rutas;
        this.ruta_imagenes = this.pr_rutas.get_ruta_imagenes();
    }
    MisPropiedadesListaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PropiedadesListaPage');
        this.get_inmueble();
    };
    MisPropiedadesListaPage.prototype.atras = function () {
        this.navCtrl.setRoot('HomePage');
    };
    MisPropiedadesListaPage.prototype.get_inmueble = function () {
        var _this = this;
        var mensaje = 'Cargando';
        var data = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        console.log(data);
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_inmueble.get_inmueble_id_usuario(data.id).subscribe(function (result) {
            _this.inmuebles = result;
            console.log(result);
            _this.pr_alert_toast.dismis_loading();
        }, function (err) {
            console.log(err);
        });
        /*
        let data=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        let data_usuario={
          id_usuario:''
        }
        for(let value of data){
          data_usuario.id_usuario=value.id;
        }
        let mensaje='Cargando';
        this.pr_alert_toast.show_loading(mensaje);
         this.pr_inmueble.get_inmueble_id_usuario(data_usuario).subscribe(
            pr_inmueble => {
              this.pr_alert_toast.dismis_loading();
              let resultado=pr_inmueble;
              if(resultado.mensaje=='inmuebles_registrados'){
                let data=resultado.data;
                this.inmuebles=data;
                console.log(this.inmuebles);
              }else{
                this.inmuebles=null;
              }
            },
            err => {console.log('el error '+err);
            },
          );
          */
    };
    MisPropiedadesListaPage.prototype.get_det_inmueble = function (item) {
        if (item.inmueble.status.id == 3) {
            this.navCtrl.push('MembresiasListadoPage');
        }
        else {
            var mensaje = 'Cargando';
            this.pr_alert_toast.show_loading(mensaje);
            this.pr_alert_toast.dismis_loading();
            var data_det_inmuebles = {
                det_inmuebles: item.inmueble,
                imagenes: item.imagenes,
                caracteristicas: item.inmueble.caracteristicas ? item.inmueble.caracteristicas.split(',') : [],
                servicios: item.inmueble.servicios ? item.inmueble.servicios.split(',') : []
            };
            localStorage.setItem('det_inmueble', JSON.stringify(data_det_inmuebles));
            this.navCtrl.push('MisPropiedadesDetallePage');
            /*
            this.pr_inmueble.get_imagenes_caracteristicas_servicios_inmueble(data_inmueble).subscribe(
              pr_inmueble => {
                this.pr_alert_toast.dismis_loading();
                let resultado=pr_inmueble;
                if(resultado.status==true){
                  let data_det_inmuebles={
                    det_inmuebles:data,
                    imagenes:resultado.imagenes,
                    caracteristicas:resultado.caracteristicas,
                    servicios:resultado.servicios
                  }
                  localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
                  this.navCtrl.push('MisPropiedadesDetallePage');
                }
              },
              err => {console.log(err);
            },
            );
            */
        }
    };
    MisPropiedadesListaPage.prototype.publicar_propiedad = function () {
        this.navCtrl.push('MisPropiedadesAgregarPage');
    };
    MisPropiedadesListaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-mis-propiedades-lista',
            templateUrl: 'mis-propiedades-lista.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrInmuebleProvider, PrAlertToastProvider, PrRutasProvider])
    ], MisPropiedadesListaPage);
    return MisPropiedadesListaPage;
}());
export { MisPropiedadesListaPage };
//# sourceMappingURL=mis-propiedades-lista.js.map