import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisPropiedadesListaPage } from './mis-propiedades-lista';

@NgModule({
  declarations: [
    MisPropiedadesListaPage,
  ],
  imports: [
    IonicPageModule.forChild(MisPropiedadesListaPage),
  ],
})
export class MisPropiedadesListaPageModule {}
