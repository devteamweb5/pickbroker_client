import { Component , OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrInmuebleProvider} from '../../providers/pr-inmueble/pr-inmueble';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';

@IonicPage()
@Component({
  selector: 'page-mis-propiedades-lista',
  templateUrl: 'mis-propiedades-lista.html',
})
export class MisPropiedadesListaPage {
  inmuebles:any;
  ruta_imagenes:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public pr_inmueble:PrInmuebleProvider,
    public pr_alert_toast:PrAlertToastProvider,
    public pr_rutas:PrRutasProvider) {
    this.ruta_imagenes=this.pr_rutas.get_ruta_imagenes();
  }

  ionViewWillEnter() {
    console.log('ionViewDidLoad PropiedadesListaPage');
    this.get_inmueble();
  }

  atras(){
    this.navCtrl.setRoot('HomePage');
  }

  get_inmueble(){
    let mensaje='Cargando';
    let data=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    console.log(data);
    this.pr_alert_toast.show_loading(mensaje);
    this.pr_inmueble.get_inmueble_id_usuario(data.id).subscribe(result=>{
      this.inmuebles = result;
      console.log(result);
      this.pr_alert_toast.dismis_loading();
    },   
    err => {console.log(err);
    },
    );
    /*
    let data=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    let data_usuario={
      id_usuario:''
    }
    for(let value of data){
      data_usuario.id_usuario=value.id;
    }
    let mensaje='Cargando';
    this.pr_alert_toast.show_loading(mensaje);
     this.pr_inmueble.get_inmueble_id_usuario(data_usuario).subscribe(
        pr_inmueble => {
          this.pr_alert_toast.dismis_loading();
          let resultado=pr_inmueble;
          if(resultado.mensaje=='inmuebles_registrados'){
            let data=resultado.data;    
            this.inmuebles=data;
            console.log(this.inmuebles);
          }else{
            this.inmuebles=null;
          }
        },   
        err => {console.log('el error '+err);
        },
      );
      */
    }
    get_det_inmueble(item){

      if(item.inmueble.status.id == 3){
        this.navCtrl.push('MembresiasListadoPage');
      }else{
        let mensaje='Cargando';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_alert_toast.dismis_loading();
        let data_det_inmuebles={
          det_inmuebles:item.inmueble,
          imagenes:item.imagenes,
          caracteristicas:item.inmueble.caracteristicas ? item.inmueble.caracteristicas.split(',') : [] ,
          servicios:item.inmueble.servicios ? item.inmueble.servicios.split(',') : []
        }
        localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
        this.navCtrl.push('MisPropiedadesDetallePage');
        /*
        this.pr_inmueble.get_imagenes_caracteristicas_servicios_inmueble(data_inmueble).subscribe(
          pr_inmueble => {
            this.pr_alert_toast.dismis_loading();
            let resultado=pr_inmueble;
            if(resultado.status==true){
              let data_det_inmuebles={
                det_inmuebles:data,
                imagenes:resultado.imagenes,
                caracteristicas:resultado.caracteristicas,
                servicios:resultado.servicios
              }
              localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
              this.navCtrl.push('MisPropiedadesDetallePage');
            }
          },
          err => {console.log(err);
        },
        );
        */
      }
    }
    publicar_propiedad(){
      this.navCtrl.push('MisPropiedadesAgregarPage');
    }

  }
