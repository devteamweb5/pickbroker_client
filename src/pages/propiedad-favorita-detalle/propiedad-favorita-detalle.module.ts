import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropiedadFavoritaDetallePage } from './propiedad-favorita-detalle';

@NgModule({
  declarations: [
    PropiedadFavoritaDetallePage,
  ],
  imports: [
    IonicPageModule.forChild(PropiedadFavoritaDetallePage),
  ],
})
export class PropiedadFavoritaDetallePageModule {}
