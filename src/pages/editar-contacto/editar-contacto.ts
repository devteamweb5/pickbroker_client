import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrContactoEmergenciaProvider} from '../../providers/pr-contacto-emergencia/pr-contacto-emergencia';


/**
 * Generated class for the EditarContactoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editar-contacto',
  templateUrl: 'editar-contacto.html',
})
export class EditarContactoPage {
data_cliente:any={
		contacto:'',
		nombre:'',
		telefono:'',
		email:''
	}
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_alert_toast:PrAlertToastProvider, public pr_contacto_emergencia:PrContactoEmergenciaProvider) {
  }

  ionViewDidLoad(){
  	let contacto=JSON.parse(localStorage.getItem('contacto'));
  	console.log(contacto);
  	this.data_cliente.nombre = contacto.nombre;
  	this.data_cliente.telefono = contacto.telefono;
  	this.data_cliente.email = contacto.email;
  	this.data_cliente.contacto = contacto.id;
    console.log('ionViewDidLoad editar contacto');
  }
  atras(){
  	this.navCtrl.pop();
  }
  guardar_contacto(){
  	if(this.data_cliente.nombre=='') {
  		let mensaje='Ingrese un nombre';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);		
  	}else if(!this.data_cliente.telefono){
  		let mensaje='Ingrese un telefono';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}else if(!this.data_cliente.email){
  		let mensaje='Ingrese un email';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}
  	let data_u=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    let mensaje='Guardando';
    this.pr_alert_toast.show_loading(mensaje);
    this.pr_contacto_emergencia.editar(this.data_cliente).subscribe(
      result => {
        this.pr_alert_toast.dismis_loading();
        this.pr_alert_toast.mensaje_toast_pie('Contacto guardado');
        this.navCtrl.pop();
      },
      err => {console.log('el error '+err);
    },
    );
  }

}
