import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListadoCitasPage } from './listado-citas';

@NgModule({
  declarations: [
    ListadoCitasPage,
  ],
  imports: [
    IonicPageModule.forChild(ListadoCitasPage),
  ],
})
export class ListadoCitasPageModule {}
