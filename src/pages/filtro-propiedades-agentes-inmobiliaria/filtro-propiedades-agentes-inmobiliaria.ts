import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FiltroPropiedadesAgentesInmobiliariaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filtro-propiedades-agentes-inmobiliaria',
  templateUrl: 'filtro-propiedades-agentes-inmobiliaria.html',
})
export class FiltroPropiedadesAgentesInmobiliariaPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FiltroPropiedadesAgentesInmobiliariaPage');
  }

}
