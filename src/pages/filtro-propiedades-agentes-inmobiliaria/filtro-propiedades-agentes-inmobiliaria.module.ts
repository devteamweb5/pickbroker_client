import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FiltroPropiedadesAgentesInmobiliariaPage } from './filtro-propiedades-agentes-inmobiliaria';

@NgModule({
  declarations: [
    FiltroPropiedadesAgentesInmobiliariaPage,
  ],
  imports: [
    IonicPageModule.forChild(FiltroPropiedadesAgentesInmobiliariaPage),
  ],
})
export class FiltroPropiedadesAgentesInmobiliariaPageModule {}
