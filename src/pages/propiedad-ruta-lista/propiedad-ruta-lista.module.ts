import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropiedadRutaListaPage } from './propiedad-ruta-lista';

@NgModule({
  declarations: [
    PropiedadRutaListaPage,
  ],
  imports: [
    IonicPageModule.forChild(PropiedadRutaListaPage),
  ],
})
export class PropiedadRutaListaPageModule {}
