import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrContactoEmergenciaProvider} from '../../providers/pr-contacto-emergencia/pr-contacto-emergencia';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import { AlertController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-contacto-emergencia-lista',
  templateUrl: 'contacto-emergencia-lista.html',
})
export class ContactoEmergenciaListaPage {
	contacto_emergencia:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public pr_contacto_emergencia:PrContactoEmergenciaProvider,
    public pr_alert_toast:PrAlertToastProvider,
    public alertCtrl: AlertController,
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactoEmergenciaListaPage');
  }
  ionViewWillEnter(){
    this.get_contactos_emergencia(); 
  }
  get_contactos_emergencia(){
  	let data_u=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    let mensaje='Cargando';
    this.pr_alert_toast.show_loading(mensaje);
    this.pr_contacto_emergencia.get_contactos_emergencia(data_u.id).subscribe(
      result => {
        this.pr_alert_toast.dismis_loading();
        if(result){
          this.contacto_emergencia=result;
        }else{
          this.pr_alert_toast.mensaje_toast_pie('No hay datos');
        }
      },
      err => {console.log(err);
      },
      );
  }

  showConfirm(contacto) {
    const confirm = this.alertCtrl.create({
      title: 'Eliminar contacto?',
      message: 'Seguro que deseas eliminar este contacto ?',
      buttons: [
      {
        text: 'Cancelar',
        handler: () => {
          console.log('Disagree clicked');
        }
      },
      {
        text: 'Eliminar',
        handler: () => {
          this.eliminarContacto(contacto);
          console.log('Agree clicked');
        }
      }
      ]
    });
    confirm.present();
  }


  agregar_contacto(){
  	this.navCtrl.push('ContactoEmergenciaAgregarPage');
  }
  atras(){
  	this.navCtrl.setRoot('HomePage');
  }

  eliminarContacto(contacto){
    let data = {contacto:contacto.id}
    this.pr_alert_toast.show_loading('Eliminando')
    this.pr_contacto_emergencia.del(data).subscribe(result=>{
      this.pr_alert_toast.dismis_loading();
      this.get_contactos_emergencia();
      console.log(result);
    })
  }

  editar(contacto){
    console.log(contacto);
    localStorage.setItem('contacto',JSON.stringify(contacto));
    this.navCtrl.push('EditarContactoPage');
  }

}
