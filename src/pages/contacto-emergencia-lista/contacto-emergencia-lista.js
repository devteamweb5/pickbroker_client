var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrContactoEmergenciaProvider } from '../../providers/pr-contacto-emergencia/pr-contacto-emergencia';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
var ContactoEmergenciaListaPage = /** @class */ (function () {
    function ContactoEmergenciaListaPage(navCtrl, navParams, pr_contacto_emergencia, pr_alert_toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_contacto_emergencia = pr_contacto_emergencia;
        this.pr_alert_toast = pr_alert_toast;
    }
    ContactoEmergenciaListaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactoEmergenciaListaPage');
    };
    ContactoEmergenciaListaPage.prototype.ionViewWillEnter = function () {
        this.get_contactos_emergencia();
    };
    ContactoEmergenciaListaPage.prototype.get_contactos_emergencia = function () {
        var _this = this;
        var data_u = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        var mensaje = 'Cargando';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_contacto_emergencia.get_contactos_emergencia(data_u.id).subscribe(function (result) {
            _this.pr_alert_toast.dismis_loading();
            if (result) {
                _this.contacto_emergencia = result;
            }
            else {
                _this.pr_alert_toast.mensaje_toast_pie('No hay datos');
            }
        }, function (err) {
            console.log(err);
        });
    };
    ContactoEmergenciaListaPage.prototype.agregar_contacto = function () {
        this.navCtrl.push('ContactoEmergenciaAgregarPage');
    };
    ContactoEmergenciaListaPage.prototype.atras = function () {
        this.navCtrl.setRoot('HomePage');
    };
    ContactoEmergenciaListaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-contacto-emergencia-lista',
            templateUrl: 'contacto-emergencia-lista.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrContactoEmergenciaProvider, PrAlertToastProvider])
    ], ContactoEmergenciaListaPage);
    return ContactoEmergenciaListaPage;
}());
export { ContactoEmergenciaListaPage };
//# sourceMappingURL=contacto-emergencia-lista.js.map