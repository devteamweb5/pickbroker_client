import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactoEmergenciaListaPage } from './contacto-emergencia-lista';

@NgModule({
  declarations: [
    ContactoEmergenciaListaPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactoEmergenciaListaPage),
  ],
})
export class ContactoEmergenciaListaPageModule {}
