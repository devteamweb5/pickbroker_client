import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrInmuebleProvider} from '../../providers/pr-inmueble/pr-inmueble';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-mapa-buscar',
  templateUrl: 'mapa-buscar.html',
})
export class MapaBuscarPage {
	inmuebles:any;
  map:any;
  item_sel:any;
  nombre_propiedad:any;
  direccion:any;
  lat_propiedad:any;
  lng_propiedad:any;
  muestra:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public pr_alert_toast:PrAlertToastProvider,
    public pr_inmueble:PrInmuebleProvider,
    public events:Events){
    this.muestra='none';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeMapaPage');
  }
  ionViewWillEnter(){
    setTimeout(()=>{
      this.map=this.loadMap();
    },1000);
    let item=1;
    this.events.publish('user:created', item);
  }
  atras(){
    this.navCtrl.pop();
  }

  loadMap(location= new google.maps.LatLng(31.69036380000001,-106.42454780000003)){
    let mapOptions={
      center: location,
      zoom:16, 
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    }
    let mapEl=document.getElementById('map_canvas');
    let map= new google.maps.Map(mapEl,mapOptions);
    // Wait the MAP_READY before using any methods.
    /*
    navigator.geolocation.getCurrentPosition(
      (position) => {
        let newLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        this.map.setCenter(newLatLng);
        this.get_propiedades_ruta();
      }
      );
      */
      this.get_propiedades_ruta();
      return map;
    }
    get_propiedades_ruta(){
      let data=JSON.parse(localStorage.getItem('resultados'));
      console.log(data);
      let latitude;
      let longitude;
      let lat_long;
      setTimeout(()=>{
        for (var i = 0; i < data.length; ++i) {
          lat_long=data[i].inmueble.latLong.split(',');
          console.log(lat_long);
          latitude=lat_long['0'];
          longitude=lat_long['1'];
          let newLatLng = new google.maps.LatLng(latitude, longitude);
          this.Position_Marker(newLatLng,data[i]);
        }
      },1000)
      
      

    }
    Position_Marker(item,item_2){
      console.log(item);
      console.log('entra en position_marker');
      var icono ="assets/imgs/pin.png";
      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: item,
        draggable: false,
        icon:icono
      });
      google.maps.event.addListener(marker, 'click', () => {
        this.mostrar_propiedad_ruta(item_2);   
      });
    }
    mostrar_propiedad_ruta(item){
      let data=item;
      this.item_sel=data;
      console.log(data);
      this.nombre_propiedad=data.inmueble.nombre;
      this.direccion=data.inmueble.direccion;
      let lat_long=data.inmueble.latLong.split(',');
      this.lat_propiedad=lat_long['0'];
      this.lng_propiedad=lat_long['1'];
      this.muestra='block';
      console.log(this.muestra);
      let mapa=document.getElementById('mapa_2');
      localStorage.setItem('propiedad_seleccionada',JSON.stringify(this.item_sel));
      mapa.classList.add('bounceIn');
    }
    ver_lista(){
      this.navCtrl.pop();
    }
    get_det_inmueble(item){
      let data= JSON.parse(localStorage.getItem('propiedad_seleccionada'));
      console.log(data);
      let mensaje='Cargando';
      let data_det_inmuebles={
        det_inmuebles:data.inmueble,
        imagenes:data.imagenes,
        caracteristicas:data.inmueble.caracteristicas ? data.inmueble.caracteristicas.split(',') : [] ,
      servicios:data.inmueble.servicios ? data.inmueble.servicios.split(',') : []
      }
      localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
      this.navCtrl.setRoot('DetInmueblePage',{
        'prev': 'MapaBuscarPage'
      });
    /*
    this.pr_inmueble.get_imagenes_caracteristicas_servicios_inmueble(data_inmueble).subscribe(
      pr_inmueble => {
        this.pr_alert_toast.dismis_loading();
        let resultado=pr_inmueble;
        if(resultado.status==true){
          let data_det_inmuebles={
            det_inmuebles:data,
            imagenes:resultado.imagenes,
            caracteristicas:resultado.caracteristicas,
            servicios:resultado.servicios
          }
          localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
          this.navCtrl.push('DetInmueblePage');
        }
      },
      err => {console.log('el error '+err);
    },
    );
    */
  }

}
