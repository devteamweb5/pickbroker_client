import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapaBuscarPage } from './mapa-buscar';

@NgModule({
  declarations: [
    MapaBuscarPage,
  ],
  imports: [
    IonicPageModule.forChild(MapaBuscarPage),
  ],
})
export class HomeMapaPageModule {}
