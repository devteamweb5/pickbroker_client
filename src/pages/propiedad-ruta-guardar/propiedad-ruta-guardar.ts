import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController,ToastController,Platform, ModalController } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrPropiedadRutaProvider} from '../../providers/pr-propiedad-ruta/pr-propiedad-ruta';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { FilePath } from '@ionic-native/file-path';
import { ImagePicker } from '@ionic-native/image-picker';
import { Camera ,CameraOptions} from '@ionic-native/camera';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';


declare var cordova;
declare var google;
@IonicPage()
@Component({
  selector: 'page-propiedad-ruta-guardar',
  templateUrl: 'propiedad-ruta-guardar.html',
})
export class PropiedadRutaGuardarPage {
  @ViewChild(Slides) slides: Slides;
	data_propiedad:any={
		nombre:'',
		user:'',
		direccion:'',
		comentarios:'',
		latLong:'',
		fecha:''
	}
	fotos_agregadas:any[]=[];
  placesService:any;
  map:any;
  photo;
  fecha:any;
  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl:ModalController,
    public pr_alert_toast:PrAlertToastProvider,
    public pr_propiedad_ruta:PrPropiedadRutaProvider,
    public actionSheetCtrl: ActionSheetController,
    private file: File,
    private filePath: FilePath,
    public camera: Camera,
    public toastCtrl:ToastController,
    private transfer: FileTransfer,
    private imagePicker: ImagePicker,
    public pr_rutas:PrRutasProvider,
  ){
		var tzoffset = (new Date()).getTimezoneOffset() * 60000;
		let fecha_actual=(new Date(Date.now() - tzoffset)).toISOString().slice(0, -1) + 'Z';
		this.fecha=fecha_actual;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PropiedadRutaGuardarPage');
    this.map=this.loadMap();
  }
  atras(){
  	this.navCtrl.pop();
  }
  buscar_direccion(){
  	let modal = this.modalCtrl.create('AutocompletarDireccionPage');
     modal.onDidDismiss(data =>{
       if(data) {
         this.data_propiedad.direccion = data.description;
         this.getPlaceDetail_i(data.place_id);
       }
      });
     modal.present();
  }
  loadMap(location= new google.maps.LatLng(20.674137, -103.346852)){
    let mapOptions={
        center: location,
        zoom:16, 
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
    }
    let mapEl=document.getElementById('map_canvas');
     let map= new google.maps.Map(mapEl,mapOptions);
     // Wait the MAP_READY before using any methods.
     navigator.geolocation.getCurrentPosition(
      (position) =>{
        let newLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        this.map.setCenter(newLatLng);
        let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: newLatLng,
        draggable: false,
        });
      }
     );
    return map;
  }
  getPlaceDetail_i(place_id:string):void{
    var self = this;
    var request = {
        placeId: place_id
    };
    this.placesService = new google.maps.places.PlacesService(this.map);
    this.placesService.getDetails(request, callback);
    function callback(place, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK){
      console.log('page > getPlaceDetail > place > ', place);
        let lat_i= place.geometry.location.lat();
        let lng_i= place.geometry.location.lng();
        let lat_lng=lat_i+','+lng_i;
        self.data_propiedad.latLong=lat_lng;
        console.log(self.data_propiedad.lat_long);
    }else{
      console.log('page > getPlaceDetail > status > ', status);
    }
    }
  }
  guardar_propiedad(){
    let data=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    this.data_propiedad.user=data.id;
    if(this.data_propiedad.nombre=='') {
      let mensaje='Ingrese un nombre';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.direccion==''){
      let mensaje='Ingrese una Dirección';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.comentario==''){
      let mensaje='Ingrese un comentario';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else{
      let mensaje='Cargando';
      this.pr_alert_toast.show_loading(mensaje);
      //this.subir_imagen_propiedad();
      this.pr_propiedad_ruta.guardar_propiedades_ruta_id_usuario(this.data_propiedad).subscribe(
        result=>{
          this.pr_alert_toast.dismis_loading();
          this.upload(result.id);
        },
        err=>{
          console.log(err);
        }
        );
    }
  }

  pickImage(){
    var options = {};
    this.imagePicker.getPictures(options).then((results) => {
      results.forEach(file=>{
        let filePath =file.replace('file://','http://localhost:8080/_file_');
        this.fotos_agregadas.push({'file':file,'filePath':filePath});
        console.log(this.fotos_agregadas);
      })
    }, (err) => { });
  }

  pickCamera(){
    const options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.NATIVE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    correctOrientation:true
  }

  this.camera.getPicture(options).then((imageData) => {
   // imageData is either a base64 encoded string or a file URI
   // If it's base64 (DATA_URL):
  let filePath =imageData.replace('file://','http://localhost:8080/_file_');
   this.fotos_agregadas.push({'file':imageData,'filePath':filePath});
   console.log(this.fotos_agregadas);
   this.photo =imageData;
   console.log(this.photo);
  }, (err) => {
    console.log(err);
   // Handle error
  });
  }

  selectMethodImage() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Seleccione un metodo',
      buttons: [
        {
          text: 'Desde Galeria',
          handler: () => {
            this.pickImage();
          }
        },{
          text: 'Desde camara',
          handler: () => {
            this.pickCamera();
          }
        }
      ]
    });
    actionSheet.present();
  }

  // full example
  upload(idPropiedad) {
    this.pr_alert_toast.show_loading('subiendo Imagenes');
    console.log(this.fotos_agregadas);
    const fileTransfer: FileTransferObject = this.transfer.create();
    let options: FileUploadOptions = {
       fileKey: 'file',
       fileName: 'name.jpg',
       params: {'propiedad':idPropiedad},
       httpMethod:'post',
       headers: {}
    }
    console.log(options);
    let imagenesSubidas = 0;
    for (var i = 0; i < this.fotos_agregadas.length; ++i) {
      fileTransfer.upload(this.fotos_agregadas[i].file, this.pr_rutas.route+'imagenes/newRuta', options)
     .then((data) => {
       imagenesSubidas++;
       if (imagenesSubidas == this.fotos_agregadas.length) {
         this.pr_alert_toast.dismis_loading();
         this.navCtrl.push('PropiedadGuardadaPage');
       }
       console.log(data);
       // success
     }, (err) => {
       console.log(err);
       if (imagenesSubidas == this.fotos_agregadas.length) {
         this.pr_alert_toast.dismis_loading();
         this.navCtrl.push('PropiedadGuardadaPage');
       }
       // error
     })
    }
  }

   /* la parte de agregar foto*/
 //  /*la parte de guardar imagenes*/
 //  seleccionar_foto(){
 //    let actionSheet = this.actionSheetCtrl.create({
 //      title: 'Seleccione Imagen',
 //      buttons: [
 //        {
 //          text: 'Desde Libreria',
 //          handler: () => {
 //            this.toma_foto(this.camera.PictureSourceType.PHOTOLIBRARY);
 //          }
 //        },
 //        {
 //          text: 'Cancelar',
 //          role: 'cancel'
 //        }
 //      ]
 //    });
 //    actionSheet.present();
 //  }
 //  public toma_foto(sourceType){
 //    // Create options for the Camera Dialog
 //    var options = {
 //      quality: 100,
 //      sourceType: sourceType,
 //      saveToPhotoAlbum: false,
 //      correctOrientation: true
 //    };
 //    // Get the data of an image
 //    this.camera.getPicture(options).then((imagePath) => {
 //      // Special handling for Android library
 //      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY){
 //        this.filePath.resolveNativePath(imagePath)
 //          .then(filePath => {
 //            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
 //            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
 //            this.copiar_archivo_ruta_local(correctPath, currentName, this.createFileName());
 //          });
 //      } else {
 //        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
 //        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
 //        this.copiar_archivo_ruta_local(correctPath, currentName, this.createFileName());
 //      }
 //    }, (err) => {  
 //      this.presentToast('Error al subir la imagen');
 //    });
 //  }
 //  // aqui sube la imagen
 //  copiar_archivo_ruta_local(namePath, currentName, newFileName){
 //    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success =>{
 //      this.fotos_agregadas.push(newFileName);
 //      /*aqui lo sube*/
 //     /* this.subir_imagen();*/
 //    }, error => {
 //      this.presentToast('Error al subir la imagen');
 //    });
 //  }
 //  /************************************************************************************************/
 // subir_imagen_propiedad(){
 //  // Destination URL
 //  var url = "http://pickbroker-panel.bsmx.tech/upload_casas.php"; //la ruta donde esta el archivo upload.php
 //  // File for Upload
 //  let data_imagen;
 //  let la_imagen_subir;
 //  let valores=0;
 //  let longitud=this.fotos_agregadas.length;
 //   for(let value of this.fotos_agregadas){
 //     this.data_propiedad.imagen='img/casas/'+value;
 //     la_imagen_subir=value;
 //    var targetPath = this.pathForImage(la_imagen_subir);
 //    // File name only
 //    var filename = la_imagen_subir;
 //    var options = {
 //      fileKey: "file",
 //      fileName: filename,
 //      chunkedMode: false,
 //      mimeType: "multipart/form-data",
 //      params : {'fileName': filename}
 //    };
 //    console.log(options);
 //    const fileTransfer: TransferObject = this.transfer.create();
 //    // Use the FileTransfer to upload the image
 //    let mensaje='Subiendo imagen '+la_imagen_subir;
 //    fileTransfer.upload(targetPath, url, options).then(data =>{
 //     console.log('entra en el upload');
 //     console.log(this.data_propiedad);
 //     let fecha_explota=this.fecha.split('T');
 //     this.data_propiedad.fecha=fecha_explota['0'];
 //       this.pr_propiedad_ruta.guardar_propiedades_ruta_id_usuario(this.data_propiedad).subscribe(
 //         pr_propiedad_ruta => {
 //           this.pr_alert_toast.dismis_loading();
 //            let resultado=pr_propiedad_ruta;
 //            if(resultado.status==true){
 //              console.log('la imagen guardada '+resultado.data);
 //              this.navCtrl.push('PropiedadRutaGuardadaPage');
 //            }
 //          },
 //          err => {
 //            console.log('el error '+err);
 //          },
 //        );
 //      /*****************************************************/
 //      /*guarda la imagen al server*/
 //    },err =>{
 //      console.log(err);
 //      this.presentToast('Error subiendo imagen');
 //    });
 //    if(valores==longitud){

 //     this.navCtrl.push('PropiedadGuardadaPage');
 //    }else{
 //      valores++;
 //    }
 //  console.log(valores);
 //  console.log(longitud);
 //  }
 // }
 //  /******************************/
 //  // Create a new name for the image
 //  createFileName(){
 //    var d = new Date(),
 //    n = d.getTime(),
 //    newFileName =  n + ".jpg";
 //    return newFileName;
 //  }
  presentToast(text){
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  pathForImage(img){
    if (img === null){
      return '';
    }else{
      return cordova.file.dataDirectory + img;
    }
  }

  deleteImage(index){
    let prev = this.slides.getPreviousIndex();
    // let prev = this.slides.slidePrev();
    this.slides.slideTo(index -1 , 500);
    this.fotos_agregadas.splice(index,1);
    console.log(this.fotos_agregadas);
  }

}
