import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropiedadRutaGuardarPage } from './propiedad-ruta-guardar';
import {IonTagsInputModule} from "ionic-tags-input";

@NgModule({
  declarations: [
    PropiedadRutaGuardarPage,
  ],
  imports: [
    IonicPageModule.forChild(PropiedadRutaGuardarPage),
    IonTagsInputModule
  ],
})
export class PropiedadRutaGuardarPageModule {}
