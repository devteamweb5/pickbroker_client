var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ToastController, Platform, ModalController } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrPropiedadRutaProvider } from '../../providers/pr-propiedad-ruta/pr-propiedad-ruta';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
var PropiedadRutaGuardarPage = /** @class */ (function () {
    function PropiedadRutaGuardarPage(platform, navCtrl, navParams, modalCtrl, pr_alert_toast, pr_propiedad_ruta, actionSheetCtrl, file, transfer, filePath, camera, toastCtrl) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_propiedad_ruta = pr_propiedad_ruta;
        this.actionSheetCtrl = actionSheetCtrl;
        this.file = file;
        this.transfer = transfer;
        this.filePath = filePath;
        this.camera = camera;
        this.toastCtrl = toastCtrl;
        this.data_propiedad = {
            nombre: '',
            user: '',
            direccion: '',
            comentarios: '',
            latLong: '',
            fecha: ''
        };
        this.fotos_agregadas = [];
        var tzoffset = (new Date()).getTimezoneOffset() * 60000;
        var fecha_actual = (new Date(Date.now() - tzoffset)).toISOString().slice(0, -1) + 'Z';
        this.fecha = fecha_actual;
    }
    PropiedadRutaGuardarPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PropiedadRutaGuardarPage');
        this.map = this.loadMap();
    };
    PropiedadRutaGuardarPage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    PropiedadRutaGuardarPage.prototype.buscar_direccion = function () {
        var _this = this;
        var modal = this.modalCtrl.create('AutocompletarDireccionPage');
        modal.onDidDismiss(function (data) {
            if (data) {
                _this.data_propiedad.direccion = data.description;
                _this.getPlaceDetail_i(data.place_id);
            }
        });
        modal.present();
    };
    PropiedadRutaGuardarPage.prototype.loadMap = function (location) {
        var _this = this;
        if (location === void 0) { location = new google.maps.LatLng(20.674137, -103.346852); }
        var mapOptions = {
            center: location,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };
        var mapEl = document.getElementById('map_canvas');
        var map = new google.maps.Map(mapEl, mapOptions);
        // Wait the MAP_READY before using any methods.
        navigator.geolocation.getCurrentPosition(function (position) {
            var newLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            _this.map.setCenter(newLatLng);
            var marker = new google.maps.Marker({
                map: _this.map,
                animation: google.maps.Animation.DROP,
                position: newLatLng,
                draggable: false,
            });
        });
        return map;
    };
    PropiedadRutaGuardarPage.prototype.getPlaceDetail_i = function (place_id) {
        var self = this;
        var request = {
            placeId: place_id
        };
        this.placesService = new google.maps.places.PlacesService(this.map);
        this.placesService.getDetails(request, callback);
        function callback(place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                console.log('page > getPlaceDetail > place > ', place);
                var lat_i = place.geometry.location.lat();
                var lng_i = place.geometry.location.lng();
                var lat_lng = lat_i + ',' + lng_i;
                self.data_propiedad.latLong = lat_lng;
                console.log(self.data_propiedad.lat_long);
            }
            else {
                console.log('page > getPlaceDetail > status > ', status);
            }
        }
    };
    PropiedadRutaGuardarPage.prototype.guardar_propiedad = function () {
        var _this = this;
        var data = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        this.data_propiedad.user = data.id;
        if (this.data_propiedad.nombre == '') {
            var mensaje = 'Ingrese un nombre';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.direccion == '') {
            var mensaje = 'Ingrese una Dirección';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.comentario == '') {
            var mensaje = 'Ingrese un comentario';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else {
            var mensaje = 'Cargando';
            this.pr_alert_toast.show_loading(mensaje);
            //this.subir_imagen_propiedad();
            this.pr_propiedad_ruta.guardar_propiedades_ruta_id_usuario(this.data_propiedad).subscribe(function (result) {
                _this.pr_alert_toast.dismis_loading();
                _this.pr_alert_toast.mensaje_toast_pie('Propiedad guardada');
                _this.navCtrl.pop();
            }, function (err) {
                console.log(err);
            });
        }
    };
    /* la parte de agregar foto*/
    /*la parte de guardar imagenes*/
    PropiedadRutaGuardarPage.prototype.seleccionar_foto = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Seleccione Imagen',
            buttons: [
                {
                    text: 'Desde Libreria',
                    handler: function () {
                        _this.toma_foto(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    PropiedadRutaGuardarPage.prototype.toma_foto = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imagePath) {
            // Special handling for Android library
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copiar_archivo_ruta_local(correctPath, currentName, _this.createFileName());
                });
            }
            else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copiar_archivo_ruta_local(correctPath, currentName, _this.createFileName());
            }
        }, function (err) {
            _this.presentToast('Error al subir la imagen');
        });
    };
    // aqui sube la imagen
    PropiedadRutaGuardarPage.prototype.copiar_archivo_ruta_local = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.fotos_agregadas.push(newFileName);
            /*aqui lo sube*/
            /* this.subir_imagen();*/
        }, function (error) {
            _this.presentToast('Error al subir la imagen');
        });
    };
    /************************************************************************************************/
    PropiedadRutaGuardarPage.prototype.subir_imagen_propiedad = function () {
        var _this = this;
        // Destination URL
        var url = "http://pickbroker-panel.bsmx.tech/upload_casas.php"; //la ruta donde esta el archivo upload.php
        // File for Upload
        var data_imagen;
        var la_imagen_subir;
        var valores = 0;
        var longitud = this.fotos_agregadas.length;
        for (var _i = 0, _a = this.fotos_agregadas; _i < _a.length; _i++) {
            var value = _a[_i];
            this.data_propiedad.imagen = 'img/casas/' + value;
            la_imagen_subir = value;
            var targetPath = this.pathForImage(la_imagen_subir);
            // File name only
            var filename = la_imagen_subir;
            var options = {
                fileKey: "file",
                fileName: filename,
                chunkedMode: false,
                mimeType: "multipart/form-data",
                params: { 'fileName': filename }
            };
            console.log(options);
            var fileTransfer = this.transfer.create();
            // Use the FileTransfer to upload the image
            var mensaje = 'Subiendo imagen ' + la_imagen_subir;
            fileTransfer.upload(targetPath, url, options).then(function (data) {
                console.log('entra en el upload');
                console.log(_this.data_propiedad);
                var fecha_explota = _this.fecha.split('T');
                _this.data_propiedad.fecha = fecha_explota['0'];
                _this.pr_propiedad_ruta.guardar_propiedades_ruta_id_usuario(_this.data_propiedad).subscribe(function (pr_propiedad_ruta) {
                    _this.pr_alert_toast.dismis_loading();
                    var resultado = pr_propiedad_ruta;
                    if (resultado.status == true) {
                        console.log('la imagen guardada ' + resultado.data);
                        _this.navCtrl.push('PropiedadRutaGuardadaPage');
                    }
                }, function (err) {
                    console.log('el error ' + err);
                });
                /*****************************************************/
                /*guarda la imagen al server*/
            }, function (err) {
                console.log(err);
                _this.presentToast('Error subiendo imagen');
            });
            if (valores == longitud) {
                this.navCtrl.push('PropiedadGuardadaPage');
            }
            else {
                valores++;
            }
            console.log(valores);
            console.log(longitud);
        }
    };
    /******************************/
    // Create a new name for the image
    PropiedadRutaGuardarPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    PropiedadRutaGuardarPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    PropiedadRutaGuardarPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    PropiedadRutaGuardarPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-propiedad-ruta-guardar',
            templateUrl: 'propiedad-ruta-guardar.html',
        }),
        __metadata("design:paramtypes", [Platform, NavController, NavParams, ModalController, PrAlertToastProvider, PrPropiedadRutaProvider, ActionSheetController, File, Transfer, FilePath, Camera, ToastController])
    ], PropiedadRutaGuardarPage);
    return PropiedadRutaGuardarPage;
}());
export { PropiedadRutaGuardarPage };
//# sourceMappingURL=propiedad-ruta-guardar.js.map