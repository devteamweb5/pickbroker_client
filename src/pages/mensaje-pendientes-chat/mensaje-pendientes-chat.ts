import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrCitasProvider} from '../../providers/pr-citas/pr-citas';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';

@IonicPage()
@Component({
  selector: 'page-mensaje-pendientes-chat',
  templateUrl: 'mensaje-pendientes-chat.html',
})
export class MensajePendientesChatPage {
	contactos:any;
  user;
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_citas:PrCitasProvider, public pr_alert_toast:PrAlertToastProvider) {
  }
	ionViewWillEnter(){
    this.get_mensajes_pendientes();
  }
  get_mensajes_pendientes(){
		this.user = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
	   let mensaje='Cargando';
	   this.pr_alert_toast.show_loading(mensaje);
		this.pr_citas.get_contactos(this.user.id).subscribe(
	    result=>{
	    this.pr_alert_toast.dismis_loading();
	      this.contactos = result;
        console.log(result);
	    },
	    err => {console.log(err);
	    },
	   );
  }
  ver_chat(item){
    console.log(item);
    this.pr_citas.get_mensajes_pendientes(item.chat).subscribe(result=>{
      console.log(result);
      this.navCtrl.push('ChatClientePage',{
        chat:item.chat,
        contacto:item.contacto.id
      })
    });
  	// let data_u=JSON.parse(localStorage.getItem('data_socio_pickbroker'));
  	// let usuario=data_u.usuario;
  	// let data={
  	// 	id_cita:item.id_cita,
  	// 	id_cliente:item.id_cliente,
  	// 	nombre:'',
  	// 	id_usuario_agente:'',
  	// }
	  // for(let value of usuario.usuario){
			// data.nombre=value.nombre;
			// data.id_usuario_agente=value.id;
	  // }
	  //  console.log(data);
	  //  localStorage.setItem('datos_cita_mensaje',JSON.stringify(data));
	  //  this.navCtrl.push('ChatClientePage');
  }
  atras(){
  	this.navCtrl.setRoot('HomePage');
  }

}
