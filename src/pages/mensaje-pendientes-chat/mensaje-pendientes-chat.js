var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrCitasProvider } from '../../providers/pr-citas/pr-citas';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
var MensajePendientesChatPage = /** @class */ (function () {
    function MensajePendientesChatPage(navCtrl, navParams, pr_citas, pr_alert_toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_citas = pr_citas;
        this.pr_alert_toast = pr_alert_toast;
    }
    MensajePendientesChatPage.prototype.ionViewWillEnter = function () {
        this.get_mensajes_pendientes();
    };
    MensajePendientesChatPage.prototype.get_mensajes_pendientes = function () {
        var _this = this;
        this.user = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        var mensaje = 'Cargando';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_citas.get_contactos(this.user.id).subscribe(function (result) {
            _this.pr_alert_toast.dismis_loading();
            _this.contactos = result;
            console.log(result);
        }, function (err) {
            console.log(err);
        });
    };
    MensajePendientesChatPage.prototype.ver_chat = function (item) {
        var _this = this;
        console.log(item);
        this.pr_citas.get_mensajes_pendientes(item.chat).subscribe(function (result) {
            console.log(result);
            _this.navCtrl.push('ChatClientePage', {
                chat: item.chat,
                contacto: item.contacto.id
            });
        });
        // let data_u=JSON.parse(localStorage.getItem('data_socio_pickbroker'));
        // let usuario=data_u.usuario;
        // let data={
        // 	id_cita:item.id_cita,
        // 	id_cliente:item.id_cliente,
        // 	nombre:'',
        // 	id_usuario_agente:'',
        // }
        // for(let value of usuario.usuario){
        // data.nombre=value.nombre;
        // data.id_usuario_agente=value.id;
        // }
        //  console.log(data);
        //  localStorage.setItem('datos_cita_mensaje',JSON.stringify(data));
        //  this.navCtrl.push('ChatClientePage');
    };
    MensajePendientesChatPage.prototype.atras = function () {
        this.navCtrl.setRoot('HomePage');
    };
    MensajePendientesChatPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-mensaje-pendientes-chat',
            templateUrl: 'mensaje-pendientes-chat.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrCitasProvider, PrAlertToastProvider])
    ], MensajePendientesChatPage);
    return MensajePendientesChatPage;
}());
export { MensajePendientesChatPage };
//# sourceMappingURL=mensaje-pendientes-chat.js.map