import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MensajePendientesChatPage } from './mensaje-pendientes-chat';

@NgModule({
  declarations: [
    MensajePendientesChatPage,
  ],
  imports: [
    IonicPageModule.forChild(MensajePendientesChatPage),
  ],
})
export class MensajePendientesChatPageModule {}
