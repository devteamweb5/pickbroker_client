import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrLoginProvider} from '../../providers/pr-login/pr-login';
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  data_usuario:any={
    username:'',
    password:'',
    //token:''
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_alert_toast:PrAlertToastProvider, public pr_login:PrLoginProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  registro(){
  	this.navCtrl.push('RegisterPage');
  }
  olvido_password(){
    this.navCtrl.push('ForgotPasswordPage');
  }
  login(){
    if(this.data_usuario.username=='') {
      let mensaje='Ingrese un email';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_usuario.password==''){
      let mensaje='Ingrese una contraseña';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else{
       /*
       let token=this.pr_login.get_token();
       if(token==null || token==undefined) {
         token=1;
       }
       this.data_usuario.token=token;
       */
       let mensaje='Cargando';
       this.pr_alert_toast.show_loading(mensaje);
       this.pr_login.login(this.data_usuario).subscribe(
         result => {
           this.pr_alert_toast.dismis_loading();
           console.log(result);
           mensaje='Bienvenido';
           this.pr_alert_toast.mensaje_toast_pie(mensaje);
           this.pr_login.user = result;
           localStorage.setItem('data_cliente_pickbroker',JSON.stringify(result));
           this.navCtrl.setRoot('HomePage');
             /*
             if(resultado.mensaje=='datos_usuario'){
               let data=resultado.data;
               let nivel;
               for(let value of data) {
                nivel=value.id_nivel;
               }
               console.log(nivel);
               if(nivel!=5){
                 let mensaje='Debes ser un cliente para continuar';
                  this.pr_alert_toast.mensaje_toast_pie(mensaje);  
               }else{
                 let mensaje='Bienvenido';
                 this.pr_alert_toast.mensaje_toast_pie(mensaje);
                 localStorage.setItem('data_cliente_pickbroker',JSON.stringify(data));
                 this.navCtrl.setRoot('HomePage');
               }
             }else{
               let mensaje='Login y contraseña no validos';
               this.pr_alert_toast.mensaje_toast_pie(mensaje);
             }
             */
           },
           err => {
             let error = JSON.parse(err._body).error;
             console.log(error);
             this.pr_alert_toast.dismis_loading();
             mensaje= error;
             this.pr_alert_toast.mensaje_toast_pie(mensaje);
           },
           );
     }
   }

 }
