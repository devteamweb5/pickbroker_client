var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrLoginProvider } from '../../providers/pr-login/pr-login';
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, pr_alert_toast, pr_login) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_login = pr_login;
        this.data_usuario = {
            username: '',
            password: '',
        };
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.registro = function () {
        this.navCtrl.push('RegisterPage');
    };
    LoginPage.prototype.olvido_password = function () {
        this.navCtrl.push('ForgotPasswordPage');
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        if (this.data_usuario.username == '') {
            var mensaje = 'Ingrese un email';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_usuario.password == '') {
            var mensaje = 'Ingrese una contraseña';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else {
            /*
            let token=this.pr_login.get_token();
            if(token==null || token==undefined) {
              token=1;
            }
            this.data_usuario.token=token;
            */
            var mensaje_1 = 'Cargando';
            this.pr_alert_toast.show_loading(mensaje_1);
            this.pr_login.login(this.data_usuario).subscribe(function (result) {
                _this.pr_alert_toast.dismis_loading();
                console.log(result);
                mensaje_1 = 'Bienvenido';
                _this.pr_alert_toast.mensaje_toast_pie(mensaje_1);
                localStorage.setItem('data_cliente_pickbroker', JSON.stringify(result));
                _this.navCtrl.setRoot('HomePage');
                /*
                if(resultado.mensaje=='datos_usuario'){
                  let data=resultado.data;
                  let nivel;
                  for(let value of data) {
                   nivel=value.id_nivel;
                  }
                  console.log(nivel);
                  if(nivel!=5){
                    let mensaje='Debes ser un cliente para continuar';
                     this.pr_alert_toast.mensaje_toast_pie(mensaje);
                  }else{
                    let mensaje='Bienvenido';
                    this.pr_alert_toast.mensaje_toast_pie(mensaje);
                    localStorage.setItem('data_cliente_pickbroker',JSON.stringify(data));
                    this.navCtrl.setRoot('HomePage');
                  }
                }else{
                  let mensaje='Login y contraseña no validos';
                  this.pr_alert_toast.mensaje_toast_pie(mensaje);
                }
                */
            }, function (err) {
                var error = JSON.parse(err._body).error;
                console.log(error);
                _this.pr_alert_toast.dismis_loading();
                mensaje_1 = error;
                _this.pr_alert_toast.mensaje_toast_pie(mensaje_1);
            });
        }
    };
    LoginPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-login',
            templateUrl: 'login.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrAlertToastProvider, PrLoginProvider])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.js.map