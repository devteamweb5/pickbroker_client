import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DirectorioServiciosPage } from './directorio-servicios';

@NgModule({
  declarations: [
    DirectorioServiciosPage,
  ],
  imports: [
    IonicPageModule.forChild(DirectorioServiciosPage),
  ],
})
export class DirectorioServiciosPageModule {}
