import { Component , NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController,ToastController, Events, ModalController } from 'ionic-angular';
import { File ,FileEntry } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera ,CameraOptions } from '@ionic-native/camera';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrInmuebleProvider} from '../../providers/pr-inmueble/pr-inmueble';
import {PrServicioProvider} from '../../providers/pr-servicio/pr-servicio';
import {PrTipoInmueblePublicacionProvider} from '../../providers/pr-tipo-inmueble-publicacion/pr-tipo-inmueble-publicacion';
import { ImagePicker } from '@ionic-native/image-picker';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { Http } from '@angular/http';


declare var cordova;
declare var google;

@IonicPage()
@Component({
  selector: 'page-mis-propiedades-agregar',
  templateUrl: 'mis-propiedades-agregar.html',
})
export class MisPropiedadesAgregarPage{
  @ViewChild(Slides) slides: Slides;

	data_propiedad:any={
    id_usuario:'',
    id_status_publicacion:3, /*ojo cuando este la membresia esto respetarlo Pendiente.*/
    id_tipo_publicacion:'',
    id_tipo_inmueble:'',
    titulo:'',
    inmobiliaria:'',
    precio:'',
    direccion:'',
    lat_long:'',
    descripcion:'',
    terreno:'',
    construido:'',
    recamaras:'',
    banios:'',
    cochera:'',
    antiguedad:'',
    fecha_publicacion:'',
    fecha_vencimiento:'',
    caracteristicas:null,
    servicios:null,
  }
  data_direccion:any={
    street:'',
    num_ext:'',
    zip:'',
    colonia:'',
    municipio:'',
    ciudad:'',
    estado:'',
    id_tipo_propiedad:'',
    recamaras:'',
    banos:'',
    medios_banos:'',
    estacionamientos:'',
    area_construida:'',
    superficie_terreno:'',
    amenities:'',
    edad:'',
    latitud:'',
    longitud:'',
    user_id:'',
    administrativeArea:'',
    country:'MX',
  }
  caracteristica_agregar:any;
  servicio_agregar:any;
  nueva_caracteristica:any=0;
  caracteristica:any[]=[];
  servicio=null;
  fotos_agregadas:any[]=[];
  placesService:any;
  map:any;
  servicio_select:any;
  data_tipo_inmueble:any;
  data_tipo_publicacion:any;1
  deshabilitado:any=false;
  photo;
  /*renta*/
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public camera: Camera,
    public platform: Platform,
    private file: File,
    private filePath: FilePath,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl:ToastController,
    public events:Events,
    public pr_alert_toast:PrAlertToastProvider,
    public modalCtrl: ModalController,
    public pr_inmueble:PrInmuebleProvider,
    public pr_servicio:PrServicioProvider,
    public pr_tipo_inmueble_publicacion:PrTipoInmueblePublicacionProvider,
    private imagePicker: ImagePicker,
    private transfer: FileTransfer,
    public pr_rutas:PrRutasProvider,
    private zone: NgZone,
    private http: Http,
    ){}
  atras(){
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MisPropiedadesAgregarPage');
    this.map=this.loadMap();
    this.get_servicio_inmueble();
    this.tipo_publicacion();
  }

  tipo_publicacion(){
    let mensaje='Cargando tipo de publicación';
    this.pr_alert_toast.show_loading(mensaje);
    this.pr_tipo_inmueble_publicacion.get_tipo_publicacion().subscribe(
      pr_tipo_inmueble_publicacion =>{
        this.pr_alert_toast.dismis_loading();
        let resultado=pr_tipo_inmueble_publicacion;
        this.tipo_inmueble();
        if(resultado){
          this.data_tipo_publicacion=resultado;
          console.log(this.data_tipo_publicacion);
        }else{
          let mensaje='No existen tipo de publicaciones';
          this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
      },
      err => {console.log('el error '+err);
    },
    );
  }

  deleteImage(index){
    let prev = this.slides.getPreviousIndex();
    // let prev = this.slides.slidePrev();
    this.slides.slideTo(index -1 , 500);
    this.fotos_agregadas.splice(index,1);
    console.log(this.fotos_agregadas);
  }

  tipo_inmueble(){
    let mensaje='Cargando tipo de inmuebles';
    this.pr_alert_toast.show_loading(mensaje);
    this.pr_tipo_inmueble_publicacion.get_tipo_inmueble().subscribe(
      pr_tipo_inmueble_publicacion => {
        this.pr_alert_toast.dismis_loading();
        let resultado=pr_tipo_inmueble_publicacion;
        if(resultado){
          this.data_tipo_inmueble=resultado;
        }else{
          let mensaje='No existen tipo de inmuebles';
          this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
      },
      err => {console.log('el error '+err);
    },
    );
  }
  agregar_caracteristica(){
  	this.nueva_caracteristica=1;
  }
  guardar_caracteristica(){
  	this.nueva_caracteristica=0;
  	this.caracteristica.push(this.caracteristica_agregar);
  	this.caracteristica_agregar=''
  	console.log(this.caracteristica);
  }
  guardar_servicio(item){
  	let nombre_servicio=item;
  	this.nueva_caracteristica=0;
  	this.servicio =nombre_servicio;
  	this.servicio_agregar=''
  	console.log(this.servicio);
  }

  get_servicio_inmueble(){
    this.pr_servicio.get_servicio().subscribe(
      pr_servicio => {
        console.log(pr_servicio);
        let resultado=pr_servicio;
        if(resultado){
          this.servicio_select=resultado;
        }
      },
      err => {console.log(err);
    },
    );
  }

  pickImage(){
    var options = {};
    this.imagePicker.getPictures(options).then((results) => {
      results.forEach(file=>{
        let filePath =file.replace('file://','http://localhost:8080/_file_');
        this.fotos_agregadas.push({'file':file,'filePath':filePath});
        console.log(this.fotos_agregadas);
      })
    }, (err) => { });
  }

  pickCamera(){
    const options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.NATIVE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    correctOrientation:true
  }

  this.camera.getPicture(options).then((imageData) => {
   // imageData is either a base64 encoded string or a file URI
   // If it's base64 (DATA_URL):
  let filePath =imageData.replace('file://','http://localhost:8080/_file_');
   this.fotos_agregadas.push({'file':imageData,'filePath':filePath});
   console.log(this.fotos_agregadas);
   this.photo =imageData;
   console.log(this.photo);
  }, (err) => {
    console.log(err);
   // Handle error
  });
  }

  selectMethodImage() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Seleccione un metodo',
      buttons: [
        {
          text: 'Desde Galeria',
          handler: () => {
            this.pickImage();
          }
        },{
          text: 'Desde camara',
          handler: () => {
            this.pickCamera();
          }
        }
      ]
    });
    actionSheet.present();
  }

  // full example
  upload(idPropiedad) {
    this.pr_alert_toast.show_loading('subiendo Imagenes');
    console.log(this.fotos_agregadas);
    this.newUpload(idPropiedad);
  }

  newUpload(idPropiedad){
    let imagenesSubidas = 0;
    for (let i = 0; i < this.fotos_agregadas.length; i++) {
      console.log(this.fotos_agregadas[i]);
      this.file.resolveLocalFilesystemUrl(this.fotos_agregadas[i].file).then((entry: FileEntry) => {
      entry.file(file => {
        console.log(file); 
        console.log('readFile');
        const reader = new FileReader();
        reader.onloadend = () => {
          console.log('readingOnloaded');
          const imgBlob = new Blob([ reader.result ], { type: file.type });
          let formData = new FormData();
          formData.append('propiedad',idPropiedad);
          formData.append('foto', imgBlob, 'tempfile');
          // this.fotos_agregadas[0].file = imgBlob
          this.zone.run(() => {
            this.http.post(this.pr_rutas.route+'imagenes/new',formData).subscribe(
            result=>{
            console.log(result);
            imagenesSubidas++;
            if (imagenesSubidas == this.fotos_agregadas.length) {
              this.pr_alert_toast.dismis_loading();
              this.navCtrl.push('PropiedadGuardadaPage');
            }
          },error=>{
            console.log(error)
            imagenesSubidas++;
            if (imagenesSubidas == this.fotos_agregadas.length) {
              this.pr_alert_toast.dismis_loading();
              this.navCtrl.push('PropiedadGuardadaPage');
            }
          });
          });
        };
        reader.readAsArrayBuffer(file);
        });
      });
    }
  }

  buscar_direccion(){
  	let modal = this.modalCtrl.create('AutocompletarDireccionPage');
    modal.onDidDismiss(data =>{
      if(data) {
        this.data_propiedad.direccion = data.description;
        this.getPlaceDetail_i(data.place_id);
      }
    });
    modal.present();
  }
  getPlaceDetail_i(place_id:string):void{
    var self = this;
    var request = {
      placeId: place_id
    };
    this.placesService = new google.maps.places.PlacesService(this.map);
    this.placesService.getDetails(request, callback);
    function callback(place, status){
      if (status == google.maps.places.PlacesServiceStatus.OK){
        console.log('page > getPlaceDetail > place > ', place);
        let lat_i= place.geometry.location.lat();
        let lng_i= place.geometry.location.lng();
        let lat_lng=lat_i+','+lng_i;
        self.data_propiedad.latitud=lat_i;
        self.data_propiedad.longitud=lng_i;
        self.data_propiedad.lat_long=lat_lng;
        console.log(self.data_propiedad.lat_long);
      }else{
        console.log('page > getPlaceDetail > status > ', status);
      }
    }
  }
  loadMap(location= new google.maps.LatLng(20.674137, -103.346852)){
    let mapOptions={
      center: location,
      zoom:16, 
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    }
    let mapEl=document.getElementById('map_canvas');
    let map= new google.maps.Map(mapEl,mapOptions);
    // Wait the MAP_READY before using any methods.
    navigator.geolocation.getCurrentPosition(
      (position) =>{
        let newLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        this.map.setCenter(newLatLng);
        let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: newLatLng,
          draggable: false,
        });
      }
      );
    return map;
  }
  calcular_precio(){
    if(this.data_propiedad.nombre==''){
      let mensaje='Ingrese un nombre de inmobiliaria';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.titulo==''){
      let mensaje='Ingrese un titulo';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.direccion==''){
      let mensaje='Ingrese una direccion';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.descripcion==''){
      let mensaje='Ingrese una descripción';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.terreno==''){
      let mensaje='Ingrese los metros del terreno';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.construido==''){
      let mensaje='Ingrese los metros construidoa';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.recamaras==''){
      let mensaje='Ingrese cantidad de recamaras';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.banios==''){
      let mensaje='Ingrese cantidad de baños';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.cochera==''){
      let mensaje='Ingrese cantidad de cocheras';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else{
      let data=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
      this.data_propiedad.id_usuario=data.id;
      let mensaje='Calculando precio de la propiedad';
      this.pr_alert_toast.show_loading(mensaje);
      this.pr_inmueble.calculo_inmueble_yals(this.data_propiedad).subscribe(
        result => {
          this.pr_alert_toast.dismis_loading();
          this.data_propiedad.precio=Math.round(result);
          this.deshabilitado=true;
          /*
          let resultado=pr_inmueble;
          if(resultado.mensaje=='datos_precio_inmueble'){
            this.deshabilitado=true;
            let data=resultado;
            console.log(data.error);
            if(data.error){
              this.pr_alert_toast.mensaje_toast_pie(data.error);
            }else{
              if(this.data_propiedad.id_tipo_publicacion==1){
                //renta
                this.data_propiedad.precio=Math.round(data.valuacion_renta.valuacion);
                this.deshabilitado=true;
              }else if(this.data_propiedad.id_tipo_publicacion==2){
                //venta
                this.deshabilitado=true;
                this.data_propiedad.precio=Math.round(data.valuacion.valuacion);
              }else if(this.data_propiedad.id_tipo_publicacion==3){
                //renta
                this.deshabilitado=true;
                this.data_propiedad.precio=Math.round(data.valuacion_renta.valuacion);
              }
            }
            console.log(this.data_propiedad.id_tipo_publicacion);
          }
          */
        },
        err => {console.log('el error '+err);
      },
      );
    }
  }
  guardar_inmueble(){
  	if(this.data_propiedad.nombre==''){
  		let mensaje='Ingrese un nombre de inmobiliaria';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}else if(this.data_propiedad.titulo==''){
      let mensaje='Ingrese un titulo';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.precio==''){
      let mensaje='Ingrese un precio';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.direccion==''){
      let mensaje='Ingrese una direccion';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.descripcion==''){
      let mensaje='Ingrese una descripción';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.terreno==''){
      let mensaje='Ingrese los metros del terreno';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.construido==''){
      let mensaje='Ingrese los metros construidoa';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.recamaras==''){
      let mensaje='Ingrese cantidad de recamaras';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.banios==''){
      let mensaje='Ingrese cantidad de baños';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_propiedad.cochera==''){
      let mensaje='Ingrese cantidad de cocheras';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else{
      this.data_propiedad.caracteristicas=this.caracteristica;
      this.data_propiedad.servicios=this.servicio;
      console.log(this.data_propiedad);
      let mensaje='Guardando propiedad';
      this.pr_alert_toast.show_loading(mensaje);
      this.pr_inmueble.guardar_inmueble(this.data_propiedad).subscribe(
        result => {
          console.log(result);
          this.pr_alert_toast.dismis_loading();
          if (this.fotos_agregadas.length) {
            this.upload(result.id);
          }else{
            this.navCtrl.push('PropiedadGuardadaPage')
          }
          /*
          let resultado=pr_inmueble;
          if(resultado.mensaje=='inmuebles_guadados_y_registrados'){
            let id_inmueble=resultado.id_inmueble;
            // aqui guarda las caracteristicas del inmueble
            let data_caracteristica={
              id_inmueble:id_inmueble,
              caracteristica:''
            }
            let data_servicio={
              id_inmueble:id_inmueble,
              id_tipo_servicio:''
            }
            localStorage.setItem('inmueble_guardado',JSON.stringify(data_servicio));
            for(let value of this.caracteristica) {
              data_caracteristica.caracteristica=value;
              this.pr_inmueble.guardar_caracteristica(data_caracteristica).subscribe(
                pr_inmueble => {
                  let resultado=pr_inmueble;
                  if(resultado.mensaje=='caracteristica_guardada'){
                  }
                },
                err => {console.log('el error '+err);
              },
              );
            }
            let longitud_servicio=this.servicio.length;
            let i=0;
            this.pr_alert_toast.dismis_loading();
            for(let value of this.servicio){
              for(let value_2 of this.servicio_select){
                if(value==value_2.descripcion){
                  data_servicio.id_tipo_servicio=value_2.id;
                  this.pr_inmueble.guardar_servicio(data_servicio).subscribe(
                    pr_inmueble =>{
                      let resultado=pr_inmueble;
                      if(resultado.mensaje=='servicio_guardada'){
                        i++;
                        if(longitud_servicio==i){
                          this.subir_imagen_propiedad(id_inmueble); 
                          this.navCtrl.push('PropiedadGuardadaPage');
                        }
                      }
                    },
                    err => {console.log('el error '+err);
                  },
                  );
                }	
              }						  	
            }
          }
          */
        },
        err => {console.log(err);
      },
      );
    }
  }
}
