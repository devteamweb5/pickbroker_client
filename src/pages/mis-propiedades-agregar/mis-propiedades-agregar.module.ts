import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisPropiedadesAgregarPage } from './mis-propiedades-agregar';
import {IonTagsInputModule} from "ionic-tags-input";
@NgModule({
  declarations: [
    MisPropiedadesAgregarPage,
  ],
  imports: [
    IonicPageModule.forChild(MisPropiedadesAgregarPage),
    IonTagsInputModule
  ],
})
export class MisPropiedadesAgregarPageModule {}
