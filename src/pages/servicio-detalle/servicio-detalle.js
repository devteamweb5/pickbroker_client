var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrRutasProvider } from '../../providers/pr-rutas/pr-rutas';
var ServicioDetallePage = /** @class */ (function () {
    function ServicioDetallePage(navCtrl, navParams, pr_rutas) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_rutas = pr_rutas;
        //this.ruta_imagenes=this.pr_rutas.get_ruta_imagenes();
        //console.log(this.ruta_imagenes);
    }
    ServicioDetallePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ServicioDetallePage');
        this.data_det_servicio();
    };
    ServicioDetallePage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    ServicioDetallePage.prototype.data_det_servicio = function () {
        this.servicio = JSON.parse(localStorage.getItem('det_servicio'));
        console.log(this.servicio);
    };
    ServicioDetallePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-servicio-detalle',
            templateUrl: 'servicio-detalle.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrRutasProvider])
    ], ServicioDetallePage);
    return ServicioDetallePage;
}());
export { ServicioDetallePage };
//# sourceMappingURL=servicio-detalle.js.map