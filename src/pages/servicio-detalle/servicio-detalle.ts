import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';

@IonicPage()
@Component({
  selector: 'page-servicio-detalle',
  templateUrl: 'servicio-detalle.html',
})
export class ServicioDetallePage {
  servicio;
  det_servicio:any;
  ruta_imagenes:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_rutas:PrRutasProvider){
  	//this.ruta_imagenes=this.pr_rutas.get_ruta_imagenes();
  	//console.log(this.ruta_imagenes);
  }
  ionViewDidLoad(){
    console.log('ionViewDidLoad ServicioDetallePage');
    this.data_det_servicio();
  }
  atras(){
    this.navCtrl.pop();
  }
  data_det_servicio(){
  	this.servicio=JSON.parse(localStorage.getItem('det_servicio'));
    console.log(this.servicio);
  }

}
