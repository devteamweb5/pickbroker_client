var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController, ToastController, Events } from 'ionic-angular';
import { PrRutasProvider } from '../../providers/pr-rutas/pr-rutas';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrPerfilProvider } from '../../providers/pr-perfil/pr-perfil';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { FileTransfer } from '@ionic-native/file-transfer';
import { DomSanitizer } from '@angular/platform-browser';
import { WebView } from '@ionic-native/ionic-webview/ngx';
var PerfilPage = /** @class */ (function () {
    function PerfilPage(navCtrl, navParams, pr_rutas, pr_alert_toast, pr_perfil, camera, platform, file, filePath, actionSheetCtrl, toastCtrl, events, imagePicker, transfer, DomSanitizer, webview) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_rutas = pr_rutas;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_perfil = pr_perfil;
        this.camera = camera;
        this.platform = platform;
        this.file = file;
        this.filePath = filePath;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.events = events;
        this.imagePicker = imagePicker;
        this.transfer = transfer;
        this.DomSanitizer = DomSanitizer;
        this.webview = webview;
        this.data_usuario = {
            id: '',
            clave: '',
            id_nivel: '',
            id_status_usuario: '',
            id_dispositivo: '',
            nombre: '',
            direccion: '',
            telf: '',
            login: '',
            token: '',
            dni: '',
            token_conekta: '',
            imagen: ''
        };
        this.data_contrasena = {
            id_usuario: '',
            pass: '',
            pass_2: '',
        };
        this.fotos_agregadas = [];
        this.ruta_imagenes = this.pr_rutas.get_ruta_imagenes();
    }
    PerfilPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PerfilPage');
        this.get_perfil();
    };
    PerfilPage.prototype.get_perfil = function () {
        var data = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        console.log(data);
        console.log('blue');
        this.data_usuario.id = data.id;
        this.data_usuario.clave = data.pw;
        this.data_usuario.id_nivel = data.rol.id;
        this.data_usuario.id_status_usuario = data.id_status_usuario;
        this.data_usuario.id_dispositivo = data.id_dispositivo;
        this.data_usuario.nombre = data.name;
        this.data_usuario.direccion = data.direccion;
        this.data_usuario.telf = data.phone;
        this.data_usuario.login = data.login;
        this.data_usuario.clave = data.clave;
        this.data_usuario.token = data.token;
        this.data_usuario.dni = data.dni;
        this.data_usuario.token_conekta = data.token_conekta;
        this.data_usuario.imagen = data.image;
        console.log(this.data_usuario);
    };
    PerfilPage.prototype.actualizar_contrasena = function () {
        var _this = this;
        if (this.old_pw == '') {
            var mensaje = 'Ingresa una nueva contraseña';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.new_pw != this.new_pw2) {
            var mensaje = 'Las contraseñas no coinciden';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else {
            this.data_contrasena.id_usuario = this.data_usuario.id;
            var mensaje = 'Guardando contraseña';
            var datos = {
                'id': this.data_usuario.id,
                'old_pw': this.old_pw,
                'new_pw': this.new_pw
            };
            this.pr_alert_toast.show_loading(mensaje);
            this.pr_perfil.actualizar_contrasena(datos).subscribe(function (result) {
                _this.pr_alert_toast.dismis_loading();
                if (result) {
                    var mensaje_1 = 'Contraseña actualizada';
                    _this.pr_alert_toast.mensaje_toast_pie(mensaje_1);
                }
                else {
                    var mensaje_2 = 'Antigua contraseña no coincide';
                    _this.pr_alert_toast.mensaje_toast_pie(mensaje_2);
                }
            }, function (err) {
                console.log(err);
            });
        }
    };
    PerfilPage.prototype.pickImage = function () {
        var _this = this;
        var options = {
            maximumImagesCount: 1
        };
        this.imagePicker.getPictures(options).then(function (results) {
            results.forEach(function (file) {
                _this.fotos_agregadas = file;
                console.log(_this.fotos_agregadas);
                _this.photo = file.replace('file://', 'http://localhost:8080/_file_');
            });
        }, function (err) { });
    };
    PerfilPage.prototype.pickCamera = function () {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.NATIVE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            _this.fotos_agregadas.push(imageData);
            _this.photo = imageData.replace('file://', 'http://localhost:8080/_file_');
            console.log(_this.photo);
        }, function (err) {
            console.log(err);
            // Handle error
        });
    };
    PerfilPage.prototype.selectMethodImage = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Seleccione un metodo',
            buttons: [
                {
                    text: 'Desde Galeria',
                    handler: function () {
                        _this.pickImage();
                    }
                }, {
                    text: 'Desde camara',
                    handler: function () {
                        _this.pickCamera();
                    }
                }
            ]
        });
        actionSheet.present();
    };
    PerfilPage.prototype.subir = function () {
        if (this.photo) {
            this.upload();
        }
        else {
            this.pr_alert_toast.mensaje_toast_pie('Por favor seleccione una imagen');
        }
    };
    // full example
    PerfilPage.prototype.upload = function () {
        var _this = this;
        this.pr_alert_toast.show_loading('subiendo Imagenes');
        console.log(this.fotos_agregadas);
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: 'file',
            fileName: 'name.jpg',
            params: { 'user': this.data_usuario.id },
            httpMethod: 'post',
            headers: {}
        };
        console.log(options);
        var imagenesSubidas = 0;
        for (var i = 0; i < this.fotos_agregadas.length; ++i) {
            fileTransfer.upload(this.fotos_agregadas[i], this.pr_rutas.route + 'imagenes/newProfile', options)
                .then(function (data) {
                imagenesSubidas++;
                if (imagenesSubidas == _this.fotos_agregadas.length) {
                    _this.pr_alert_toast.dismis_loading();
                    var mensaje = 'Imagen guardada';
                    _this.pr_alert_toast.mensaje_toast_pie(mensaje);
                    localStorage.removeItem('data_cliente_pickbroker');
                    _this.navCtrl.setRoot('login');
                }
                console.log(data);
                // success
            }, function (err) {
                console.log(err);
                if (imagenesSubidas == _this.fotos_agregadas.length) {
                    _this.pr_alert_toast.dismis_loading();
                    var mensaje = 'Imagen guardada';
                    _this.pr_alert_toast.mensaje_toast_pie(mensaje);
                    localStorage.removeItem('data_cliente_pickbroker');
                    _this.navCtrl.setRoot('login');
                }
                // error
            });
        }
    };
    // /*la parte de guardar imagenes*/
    // seleccionar_foto(){
    //   let actionSheet = this.actionSheetCtrl.create({
    //     title: 'Seleccione Imagen',
    //     buttons: [
    //     {
    //       text: 'Desde Libreria',
    //       handler: () => {
    //         this.toma_foto(this.camera.PictureSourceType.PHOTOLIBRARY);
    //       }
    //     },
    //     {
    //       text: 'Cancelar',
    //       role: 'cancel'
    //     }
    //     ]
    //   });
    //   actionSheet.present();
    // }
    // public toma_foto(sourceType){
    //   // Create options for the Camera Dialog
    //   var options = {
    //     quality: 100,
    //     sourceType: sourceType,
    //     saveToPhotoAlbum: false,
    //     correctOrientation: true
    //   };
    //   // Get the data of an image
    //   this.camera.getPicture(options).then((imagePath) => {
    //     // Special handling for Android library
    //     if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY){
    //       this.filePath.resolveNativePath(imagePath)
    //       .then(filePath => {
    //         let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
    //         let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
    //         this.copiar_archivo_ruta_local(correctPath, currentName, this.createFileName());
    //       });
    //     } else {
    //       var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
    //       var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
    //       this.copiar_archivo_ruta_local(correctPath, currentName, this.createFileName());
    //     }
    //   }, (err) => {  
    //     this.presentToast('Error al subir la imagen');
    //   });
    // }
    // // aqui sube la imagen
    // copiar_archivo_ruta_local(namePath, currentName, newFileName){
    //   this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success =>{
    //     this.fotos_agregadas.push(newFileName);
    //     /*aqui lo sube*/
    //     this.subir_imagen_propiedad();
    //   }, error => {
    //     this.presentToast('Error al subir la imagen');
    //   });
    // }
    // /************************************************************************************************/
    // subir_imagen_propiedad(){
    //   // Destination URL
    //   var url = "http://pickbroker-panel.bsmx.tech/upload_casas.php"; //la ruta donde esta el archivo upload.php
    //   // File for Upload
    //   let data_imagen;
    //   let la_imagen_subir;
    //   let valores=0;
    //   let longitud=this.fotos_agregadas.length;
    //   for(let value of this.fotos_agregadas){
    //     this.data_propiedad='img/casas/'+value;
    //     la_imagen_subir=value;
    //     var targetPath = this.pathForImage(la_imagen_subir);
    //     // File name only
    //     var filename = la_imagen_subir;
    //     var options = {
    //       fileKey: "file",
    //       fileName: filename,
    //       chunkedMode: false,
    //       mimeType: "multipart/form-data",
    //       params : {'fileName': filename}
    //     };
    //     console.log(options);
    //     const fileTransfer: TransferObject = this.transfer.create();
    //     // Use the FileTransfer to upload the image
    //     let mensaje='Subiendo imagen '+la_imagen_subir;
    //     fileTransfer.upload(targetPath, url, options).then(data =>{
    //       console.log('entra en el upload');
    //       console.log(this.data_propiedad);
    //       let data_u=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    //       let data_guardar={
    //         id_usuario:'',
    //         imagen:this.data_propiedad,
    //       }
    //       for(let value of data_u) {
    //         data_guardar.id_usuario=value.id;
    //       }
    //       this.pr_perfil.guardar_imagen_perfil(data_guardar).subscribe(
    //         pr_perfil =>{
    //           let resultado=pr_perfil;
    //           if(resultado.status==true){
    //             console.log('la imagen guardada '+resultado.data);
    //             let mensaje='Imagen guardada, inicie sesión';
    //             this.pr_alert_toast.mensaje_toast_pie(mensaje);
    //             localStorage.removeItem('data_socio_pickbroker');
    //             this.navCtrl.setRoot('LoginPage');
    //           }
    //         },
    //         err => {
    //           console.log('el error '+err);
    //         },
    //         );
    //       /*****************************************************/
    //       /*guarda la imagen al server*/
    //     },err =>{
    //       console.log(err);
    //       this.presentToast('Error subiendo imagen');
    //     });
    //     if(valores==longitud){
    //       this.navCtrl.push('PropiedadGuardadaPage');
    //     }else{
    //       valores++;
    //     }
    //     console.log(valores);
    //     console.log(longitud);
    //   }
    // }
    /******************************/
    // Create a new name for the image
    // createFileName(){
    //   var d = new Date(),
    //   n = d.getTime(),
    //   newFileName =  n + ".jpg";
    //   return newFileName;
    // }
    // presentToast(text){
    //   let toast = this.toastCtrl.create({
    //     message: text,
    //     duration: 3000,
    //     position: 'top'
    //   });
    //   toast.present();
    // }
    // pathForImage(img){
    //   if (img === null){
    //     return '';
    //   }else{
    //     return cordova.file.dataDirectory + img;
    //   }
    // }
    PerfilPage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    PerfilPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-perfil',
            templateUrl: 'perfil.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            PrRutasProvider,
            PrAlertToastProvider,
            PrPerfilProvider,
            Camera,
            Platform,
            File,
            FilePath,
            ActionSheetController,
            ToastController,
            Events,
            ImagePicker,
            FileTransfer,
            DomSanitizer,
            WebView])
    ], PerfilPage);
    return PerfilPage;
}());
export { PerfilPage };
//# sourceMappingURL=perfil.js.map