import { Component , NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController,ToastController, Events, ModalController } from 'ionic-angular';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrPerfilProvider} from '../../providers/pr-perfil/pr-perfil';
import { File , FileEntry } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera , CameraOptions } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { DomSanitizer } from '@angular/platform-browser';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Http } from '@angular/http';

declare var cordova;
declare var google;
@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {
  photo;
  old_pw;
  new_pw;
  new_pw2;
	data_usuario:any={
		id:'',
		clave:'',
		id_nivel:'',
		id_status_usuario:'',
		id_dispositivo:'',
		nombre:'',
		direccion:'',
		telf:'',
		login:'',
		token:'',
		dni:'',
		token_conekta:'',
		imagen:''
	};
	ruta_imagenes:any;
	data_contrasena:any={
		id_usuario:'',
		pass:'',
		pass_2:'',
	}
  newFoto;
  user;
	data_propiedad:any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public pr_rutas:PrRutasProvider,
    public pr_alert_toast:PrAlertToastProvider,
    public pr_perfil:PrPerfilProvider,
    public camera: Camera,
    public platform: Platform,
    private file: File,
    private filePath: FilePath,
    public actionSheetCtrl: ActionSheetController,
    public toastCtrl:ToastController,
    public events:Events,
    private imagePicker: ImagePicker,
    private transfer: FileTransfer,
    private DomSanitizer: DomSanitizer,
    private webview: WebView,
    private zone: NgZone,
    private http: Http,
   ) 
  {
  	this.ruta_imagenes=this.pr_rutas.get_ruta_imagenes();
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad PerfilPage');
    this.get_perfil();
  }
  get_perfil(){
    let data=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    this.user = data;
    console.log(data);
    console.log(this.user.id);
    console.log('blue');
      this.data_usuario.id=data.id;
      this.data_usuario.clave=data.pw;
      this.data_usuario.rol=data.rol.role;
      this.data_usuario.id_nivel=data.rol.id;
      this.data_usuario.id_status_usuario=data.id_status_usuario;
      this.data_usuario.id_dispositivo=data.id_dispositivo;
      this.data_usuario.nombre=data.name;
      this.data_usuario.apellido=data.lastName;
      this.data_usuario.direccion=data.direccion;
      this.data_usuario.telf=data.phone;
      this.data_usuario.login=data.login;
      this.data_usuario.clave=data.clave;
      this.data_usuario.token=data.token;
      this.data_usuario.dni=data.dni;
      this.data_usuario.token_conekta=data.token_conekta;
      this.data_usuario.imagen=data.image;
    console.log(this.data_usuario);
  }
  actualizar_contrasena(){
  	if(this.old_pw=='') {
  		let mensaje='Ingresa una nueva contraseña';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}else if(this.new_pw != this.new_pw2){
  		let mensaje='Las contraseñas no coinciden';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}else{
  		this.data_contrasena.id_usuario=this.data_usuario.id;
  		let mensaje='Guardando contraseña';
      let datos = {
        'id': this.data_usuario.id,
        'old_pw':this.old_pw,
        'new_pw':this.new_pw
      }
  		this.pr_alert_toast.show_loading(mensaje);
      this.pr_perfil.actualizar_contrasena(datos).subscribe(
        result=>{
          this.pr_alert_toast.dismis_loading();
          if(result){
            let mensaje='Contraseña actualizada';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
          }else{
            let mensaje='Antigua contraseña no coincide';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
          }
        },
        err => {console.log(err);
      },
      );
    }
  }
    pickImage(){
    var options = {
      maximumImagesCount:1
    };
    this.imagePicker.getPictures(options).then((results) => {
      results.forEach(file=>{
        this.newFoto = file;
        console.log(this.newFoto);
        this.photo = file.replace('file://','http://localhost:8080/_file_');
      })
    }, (err) => { });
  }

  pickCamera(){
    const options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.NATIVE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }

  this.camera.getPicture(options).then((imageData) => {
   // imageData is either a base64 encoded string or a file URI
   // If it's base64 (DATA_URL):
   this.newFoto = imageData;
   this.photo =imageData.replace('file://','http://localhost:8080/_file_');
   console.log(this.photo);
  }, (err) => {
    console.log(err);
   // Handle error
  });
  }

  selectMethodImage() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Seleccione un metodo',
      buttons: [
        {
          text: 'Desde Galeria',
          handler: () => {
            this.pickImage();
          }
        },{
          text: 'Desde camara',
          handler: () => {
            this.pickCamera();
          }
        }
      ]
    });
    actionSheet.present();
  }

  subir(){
    if (this.photo) {
      this.upload();
    }else{
      this.pr_alert_toast.mensaje_toast_pie('Por favor seleccione una imagen');
    }
  }

  // full example
  upload() {
    this.pr_alert_toast.show_loading('subiendo Imagenes');
    console.log(this.newFoto);
    this.newUpload()
  }

  newUpload(){
    let imagenesSubidas = 0;
      console.log(this.newFoto);
      this.file.resolveLocalFilesystemUrl(this.newFoto).then((entry: FileEntry) => {
      entry.file(file => {
        console.log(file); 
        console.log('readFile');
        const reader = new FileReader();
        reader.onloadend = () => {
          console.log('readingOnloaded');
          const imgBlob = new Blob([ reader.result ], { type: file.type });
          let formData = new FormData();
          formData.append('user',this.user.id);
          formData.append('foto', imgBlob, 'tempfile');
          this.zone.run(() => {
            this.http.post(this.pr_rutas.route+'imagenes/newProfile',formData).subscribe(
            (result:any)=>{
            console.log(result);
            this.user.image = result._body.replace('"','').replace('"','');
            localStorage.setItem('data_cliente_pickbroker',JSON.stringify(this.user));
            this.pr_alert_toast.dismis_loading();
            this.navCtrl.setRoot('HomePage');
          },error=>{
            console.log(error)
            this.pr_alert_toast.dismis_loading();
            this.pr_alert_toast.mensaje_toast_pie('Error al subir la imagen');
          });
          });
        };
        reader.readAsArrayBuffer(file);
        });
      });
  }

  // /*la parte de guardar imagenes*/
  // seleccionar_foto(){
  //   let actionSheet = this.actionSheetCtrl.create({
  //     title: 'Seleccione Imagen',
  //     buttons: [
  //     {
  //       text: 'Desde Libreria',
  //       handler: () => {
  //         this.toma_foto(this.camera.PictureSourceType.PHOTOLIBRARY);
  //       }
  //     },
  //     {
  //       text: 'Cancelar',
  //       role: 'cancel'
  //     }
  //     ]
  //   });
  //   actionSheet.present();
  // }
  // public toma_foto(sourceType){
  //   // Create options for the Camera Dialog
  //   var options = {
  //     quality: 100,
  //     sourceType: sourceType,
  //     saveToPhotoAlbum: false,
  //     correctOrientation: true
  //   };
  //   // Get the data of an image
  //   this.camera.getPicture(options).then((imagePath) => {
  //     // Special handling for Android library
  //     if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY){
  //       this.filePath.resolveNativePath(imagePath)
  //       .then(filePath => {
  //         let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
  //         let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
  //         this.copiar_archivo_ruta_local(correctPath, currentName, this.createFileName());
  //       });
  //     } else {
  //       var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
  //       var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
  //       this.copiar_archivo_ruta_local(correctPath, currentName, this.createFileName());
  //     }
  //   }, (err) => {  
  //     this.presentToast('Error al subir la imagen');
  //   });
  // }
  // // aqui sube la imagen
  // copiar_archivo_ruta_local(namePath, currentName, newFileName){
  //   this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success =>{
  //     this.newFoto.push(newFileName);
  //     /*aqui lo sube*/
  //     this.subir_imagen_propiedad();
  //   }, error => {
  //     this.presentToast('Error al subir la imagen');
  //   });
  // }
  // /************************************************************************************************/
  // subir_imagen_propiedad(){
  //   // Destination URL
  //   var url = "http://pickbroker-panel.bsmx.tech/upload_casas.php"; //la ruta donde esta el archivo upload.php
  //   // File for Upload
  //   let data_imagen;
  //   let la_imagen_subir;
  //   let valores=0;
  //   let longitud=this.newFoto.length;
  //   for(let value of this.newFoto){
  //     this.data_propiedad='img/casas/'+value;
  //     la_imagen_subir=value;
  //     var targetPath = this.pathForImage(la_imagen_subir);
  //     // File name only
  //     var filename = la_imagen_subir;
  //     var options = {
  //       fileKey: "file",
  //       fileName: filename,
  //       chunkedMode: false,
  //       mimeType: "multipart/form-data",
  //       params : {'fileName': filename}
  //     };
  //     console.log(options);
  //     const fileTransfer: TransferObject = this.transfer.create();
  //     // Use the FileTransfer to upload the image
  //     let mensaje='Subiendo imagen '+la_imagen_subir;
  //     fileTransfer.upload(targetPath, url, options).then(data =>{
  //       console.log('entra en el upload');
  //       console.log(this.data_propiedad);
  //       let data_u=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
  //       let data_guardar={
  //         id_usuario:'',
  //         imagen:this.data_propiedad,
  //       }
  //       for(let value of data_u) {
  //         data_guardar.id_usuario=value.id;
  //       }
  //       this.pr_perfil.guardar_imagen_perfil(data_guardar).subscribe(
  //         pr_perfil =>{
  //           let resultado=pr_perfil;
  //           if(resultado.status==true){
  //             console.log('la imagen guardada '+resultado.data);
  //             let mensaje='Imagen guardada, inicie sesión';
  //             this.pr_alert_toast.mensaje_toast_pie(mensaje);
  //             localStorage.removeItem('data_socio_pickbroker');
  //             this.navCtrl.setRoot('LoginPage');
  //           }
  //         },
  //         err => {
  //           console.log('el error '+err);
  //         },
  //         );
  //       /*****************************************************/
  //       /*guarda la imagen al server*/
  //     },err =>{
  //       console.log(err);
  //       this.presentToast('Error subiendo imagen');
  //     });
  //     if(valores==longitud){

  //       this.navCtrl.push('PropiedadGuardadaPage');
  //     }else{
  //       valores++;
  //     }
  //     console.log(valores);
  //     console.log(longitud);
  //   }
  // }
  /******************************/
  // Create a new name for the image
  // createFileName(){
  //   var d = new Date(),
  //   n = d.getTime(),
  //   newFileName =  n + ".jpg";
  //   return newFileName;
  // }
  // presentToast(text){
  //   let toast = this.toastCtrl.create({
  //     message: text,
  //     duration: 3000,
  //     position: 'top'
  //   });
  //   toast.present();
  // }
  // pathForImage(img){
  //   if (img === null){
  //     return '';
  //   }else{
  //     return cordova.file.dataDirectory + img;
  //   }
  // }

  atras(){
    this.navCtrl.pop();
  }


}
