import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrLoginProvider} from '../../providers/pr-login/pr-login';
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
	data_registro:any={
		id_nivel:5,
		id_tipo_membresia:1,
		id_dispositivo:'',
		nombre:'',
		apellido:'',
		login:'',
		pass_1:'',
		pass_2:'',
		token:'',
    nombre_contacto:'',
    telf_contacto:'',
    email_contacto:'',
    token_open_pay:''
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_alert_toast:PrAlertToastProvider, public pr_login:PrLoginProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  guardar(){
    if(this.data_registro.nombre==''){
      let mensaje='Ingrese un nombre';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_registro.apellidos==''){
      let mensaje='Ingrese un apellidos';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_registro.login==''){
      let mensaje='Ingrese un email';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_registro.pass_1==''){
      let mensaje='Ingrese una contraseña';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_registro.pass_1!=this.data_registro.pass_2){
      let mensaje='Las contraseñas no coinciden';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_registro.nombre_contacto==''){
      let mensaje='Ingrese un nombre de contacto';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_registro.email_contacto==''){
      let mensaje='Ingrese un email de contacto';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else{
      let token=this.pr_login.get_token();
      if(token==null ||token==undefined) {
        this.data_registro.token=1;
      }else{
        this.data_registro.token=token;
      }
      let serial=this.pr_login.get_serial();
      if(serial==null || serial==undefined){
        this.data_registro.id_dispositivo=1234;
      }else{
        this.data_registro.id_dispositivo=this.pr_login.get_serial();
      }
      console.log(this.data_registro);
      this.pr_alert_toast.show_loading('Cargando');
      this.pr_login.registro(this.data_registro).subscribe(
        result => {
          let data_usuario ={
            username:this.data_registro.login,
            password:this.data_registro.pass_1,
          }
        this.pr_login.login(data_usuario).subscribe(user=>{
          this.pr_login.user = user;
          localStorage.setItem('data_cliente_pickbroker',JSON.stringify(user));
          let mensaje='Registro guardado, bienvenido';
          this.pr_alert_toast.dismis_loading();
          this.pr_alert_toast.mensaje_toast_pie(mensaje);
          this.navCtrl.setRoot('HomePage');
        })
        
        /*
        this.pr_alert_toast.dismis_loading();
	      let resultado=pr_login;
        console.log(resultado);
	      if(resultado.mensaje=='usuario_existe'){
          let mensaje='Usuario ya existe';
	      	this.pr_alert_toast.mensaje_toast_pie(mensaje);
	      }else if(resultado.mensaje=='este_dispositivo_ya_esta_registrado'){
          let mensaje='Este dispositivo ya está registrado con otro usuario';
          this.pr_alert_toast.mensaje_toast_pie(mensaje);
	      }else if(resultado.mensaje=='usuario_guardado'){
          let data=resultado.data;
          localStorage.setItem('data_cliente_pickbroker',JSON.stringify(data));
          let mensaje='Registro guardado, bienvenido';
          this.pr_alert_toast.mensaje_toast_pie(mensaje);
          this.navCtrl.setRoot('HomePage');
        }else{
          let mensaje='Ocurrio un problema, intente nuevamente';
	      	this.pr_alert_toast.mensaje_toast_pie(mensaje);
	      }
        */
      },
      err => {
        console.log(err);
        this.pr_alert_toast.dismis_loading();
        this.pr_alert_toast.mensaje_toast_pie(JSON.parse(err._body).message);
    },
    );
  }
  }
  crear_token_cliente_open_pay(){
    if(this.data_registro.nombre==''){
      let mensaje='Ingrese un nombre';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_registro.apellidos==''){
      let mensaje='Ingrese un apellidos';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_registro.login==''){
      let mensaje='Ingrese un email';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_registro.pass_1==''){
      let mensaje='Ingrese una contraseña';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_registro.pass_1!=this.data_registro.pass_2){
      let mensaje='Las contraseñas no coinciden';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_registro.nombre_contacto==''){
      let mensaje='Ingrese un nombre de contacto';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_registro.email_contacto==''){
      let mensaje='Ingrese un email';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else{
      let token=this.pr_login.get_token();
      if(token==null ||token==undefined) {
        this.data_registro.token=1;
      }else{
        this.data_registro.token=token;
      }
      let serial=this.pr_login.get_serial();
      if(serial==null || serial==undefined){
        this.data_registro.id_dispositivo=1234;
      }else{
        this.data_registro.id_dispositivo=this.pr_login.get_serial();
      }
      let datos_cliente={
        nombre_cliente:this.data_registro.nombre+' '+this.data_registro.apellidos,
        email_cliente:this.data_registro.login,
        requires_account:false,
      }
      console.log(this.data_registro);
      let mensaje='Cargando';
      this.pr_alert_toast.show_loading(mensaje);
      this.pr_login.crear_cliente_open_pay(datos_cliente).subscribe(
        pr_login =>{
          let resultado=pr_login;
          if(resultado.status==true){
            let el_resultado=JSON.parse(resultado.data);
            let el_id=el_resultado.id;
            this.data_registro.token_open_pay=el_id;
            this.guardar();
          }
        },
        err => {
          console.log(err);
          this.pr_alert_toast.dismis_loading();
          this.pr_alert_toast.mensaje_toast_pie(err.message);
        },
        );
    }
  }

  cerrar(){
    this.navCtrl.pop();
  }

}
