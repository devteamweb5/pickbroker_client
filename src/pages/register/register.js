var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrLoginProvider } from '../../providers/pr-login/pr-login';
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, navParams, pr_alert_toast, pr_login) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_login = pr_login;
        this.data_registro = {
            id_nivel: 5,
            id_tipo_membresia: 1,
            id_dispositivo: '',
            nombre: '',
            apellido: '',
            login: '',
            pass_1: '',
            pass_2: '',
            token: '',
            nombre_contacto: '',
            telf_contacto: '',
            email_contacto: '',
            token_open_pay: ''
        };
    }
    RegisterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.guardar = function () {
        var _this = this;
        var token = this.pr_login.get_token();
        if (token == null || token == undefined) {
            this.data_registro.token = 1;
        }
        else {
            this.data_registro.token = token;
        }
        var serial = this.pr_login.get_serial();
        if (serial == null || serial == undefined) {
            this.data_registro.id_dispositivo = 1234;
        }
        else {
            this.data_registro.id_dispositivo = this.pr_login.get_serial();
        }
        console.log(this.data_registro);
        this.pr_alert_toast.show_loading('Cargando');
        this.pr_login.registro(this.data_registro).subscribe(function (result) {
            var data_usuario = {
                username: _this.data_registro.login,
                password: _this.data_registro.pass_1,
            };
            _this.pr_login.login(data_usuario).subscribe(function (user) {
                localStorage.setItem('data_cliente_pickbroker', JSON.stringify(user));
                var mensaje = 'Registro guardado, bienvenido';
                _this.pr_alert_toast.dismis_loading();
                _this.pr_alert_toast.mensaje_toast_pie(mensaje);
                _this.navCtrl.setRoot('HomePage');
            });
            /*
            this.pr_alert_toast.dismis_loading();
              let resultado=pr_login;
            console.log(resultado);
              if(resultado.mensaje=='usuario_existe'){
                let mensaje='Usuario ya existe';
                this.pr_alert_toast.mensaje_toast_pie(mensaje);
              }else if(resultado.mensaje=='este_dispositivo_ya_esta_registrado'){
                let mensaje='Este dispositivo ya está registrado con otro usuario';
              this.pr_alert_toast.mensaje_toast_pie(mensaje);
              }else if(resultado.mensaje=='usuario_guardado'){
              let data=resultado.data;
               localStorage.setItem('data_cliente_pickbroker',JSON.stringify(data));
               let mensaje='Registro guardado, bienvenido';
               this.pr_alert_toast.mensaje_toast_pie(mensaje);
               this.navCtrl.setRoot('HomePage');
            }else{
                let mensaje='Ocurrio un problema, intente nuevamente';
                this.pr_alert_toast.mensaje_toast_pie(mensaje);
              }
            */
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    RegisterPage.prototype.crear_token_cliente_open_pay = function () {
        var _this = this;
        if (this.data_registro.nombre == '') {
            var mensaje = 'Ingrese un nombre';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_registro.apellidos == '') {
            var mensaje = 'Ingrese un apellidos';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_registro.login == '') {
            var mensaje = 'Ingrese un email';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_registro.pass_1 == '') {
            var mensaje = 'Ingrese una contraseña';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_registro.pass_1 != this.data_registro.pass_2) {
            var mensaje = 'Las contraseñas no coinciden';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_registro.nombre_contacto == '') {
            var mensaje = 'Ingrese un nombre de contacto';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_registro.email_contacto == '') {
            var mensaje = 'Ingrese un email';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else {
            var token = this.pr_login.get_token();
            if (token == null || token == undefined) {
                this.data_registro.token = 1;
            }
            else {
                this.data_registro.token = token;
            }
            var serial = this.pr_login.get_serial();
            if (serial == null || serial == undefined) {
                this.data_registro.id_dispositivo = 1234;
            }
            else {
                this.data_registro.id_dispositivo = this.pr_login.get_serial();
            }
            var datos_cliente = {
                nombre_cliente: this.data_registro.nombre + ' ' + this.data_registro.apellidos,
                email_cliente: this.data_registro.login,
                requires_account: false,
            };
            console.log(this.data_registro);
            var mensaje = 'Cargando';
            this.pr_alert_toast.show_loading(mensaje);
            this.pr_login.crear_cliente_open_pay(datos_cliente).subscribe(function (pr_login) {
                var resultado = pr_login;
                if (resultado.status == true) {
                    var el_resultado = JSON.parse(resultado.data);
                    var el_id = el_resultado.id;
                    _this.data_registro.token_open_pay = el_id;
                    _this.guardar();
                }
            }, function (err) {
                console.log(err);
            });
        }
    };
    RegisterPage.prototype.cerrar = function () {
        this.navCtrl.pop();
    };
    RegisterPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-register',
            templateUrl: 'register.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrAlertToastProvider, PrLoginProvider])
    ], RegisterPage);
    return RegisterPage;
}());
export { RegisterPage };
//# sourceMappingURL=register.js.map