import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LlamadasHistorialPage } from './llamadas-historial';

@NgModule({
  declarations: [
    LlamadasHistorialPage,
  ],
  imports: [
    IonicPageModule.forChild(LlamadasHistorialPage),
  ],
})
export class LlamadasHistorialPageModule {}
