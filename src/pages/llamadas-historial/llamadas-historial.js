var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrLlamadasProvider } from '../../providers/pr-llamadas/pr-llamadas';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrRutasProvider } from '../../providers/pr-rutas/pr-rutas';
import { CallNumber } from '@ionic-native/call-number';
var LlamadasHistorialPage = /** @class */ (function () {
    function LlamadasHistorialPage(navCtrl, navParams, pr_llamadas, pr_alert_toast, pr_rutas, callNumber) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_llamadas = pr_llamadas;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_rutas = pr_rutas;
        this.callNumber = callNumber;
        this.ruta_imagenes = this.pr_rutas.get_ruta_imagenes();
    }
    LlamadasHistorialPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LlamadasHistorialPage');
        this.get_historial_llamadas_id_usuario();
    };
    LlamadasHistorialPage.prototype.llamar = function (item) {
        var _this = this;
        this.callNumber.callNumber(item.telf, true)
            .then(function (res) {
            console.log('Launched dialer!', res);
        })
            .catch(function (err) { return console.log('Error launching dialer', err); });
        var data_u = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        var llamada = {
            user: data_u.id,
            called: item.user.id,
        };
        console.log(llamada);
        console.log(item);
        this.pr_llamadas.guardar_llamadas(llamada).subscribe(function (pr_llamadas) {
            var resultado = pr_llamadas;
            if (resultado.status == true) {
                _this.get_historial_llamadas_id_usuario();
            }
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    LlamadasHistorialPage.prototype.get_historial_llamadas_id_usuario = function () {
        var _this = this;
        var data = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        this.pr_llamadas.get_historial_llamadas_id_usuario(data.id).subscribe(function (result) {
            if (result) {
                _this.llamadas = result;
                console.log(_this.llamadas);
            }
            else {
                var mensaje = 'No hay datos';
                _this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    LlamadasHistorialPage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    LlamadasHistorialPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-llamadas-historial',
            templateUrl: 'llamadas-historial.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            PrLlamadasProvider,
            PrAlertToastProvider,
            PrRutasProvider,
            CallNumber])
    ], LlamadasHistorialPage);
    return LlamadasHistorialPage;
}());
export { LlamadasHistorialPage };
//# sourceMappingURL=llamadas-historial.js.map