import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrLlamadasProvider} from '../../providers/pr-llamadas/pr-llamadas';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';
import { CallNumber } from '@ionic-native/call-number';

@IonicPage()
@Component({
  selector: 'page-llamadas-historial',
  templateUrl: 'llamadas-historial.html',
})
export class LlamadasHistorialPage{
	llamadas:any;
	ruta_imagenes:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public pr_llamadas:PrLlamadasProvider,
    public pr_alert_toast:PrAlertToastProvider,
    public pr_rutas:PrRutasProvider,
    public callNumber:CallNumber) {
  	this.ruta_imagenes=this.pr_rutas.get_ruta_imagenes();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LlamadasHistorialPage');
    this.get_historial_llamadas_id_usuario();
  }
  llamar(item){
  	this.callNumber.callNumber(item.telf, true)
  	.then(res =>{
  		console.log('Launched dialer!', res)
  	})
  	.catch(err => console.log('Error launching dialer', err));
  	let data_u=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
  	let llamada={
      user:data_u.id,
      called:item.user.id,
    }
    console.log(llamada);
    console.log(item);
    this.pr_llamadas.guardar_llamadas(llamada).subscribe(
      pr_llamadas => {
        let resultado=pr_llamadas;
        if(resultado.status==true){
          this.get_historial_llamadas_id_usuario();
        }
      },
      err => {console.log('el error '+err);
    },
    );
  }
  get_historial_llamadas_id_usuario(){
  	let data=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    this.pr_llamadas.get_historial_llamadas_id_usuario(data.id).subscribe(
      result => {
        if (result) {
          this.llamadas=result;
          console.log(this.llamadas)
        }else{
          let mensaje='No hay datos';
          this.pr_alert_toast.mensaje_toast_pie(mensaje)
        }
      },
      err => {console.log('el error '+err);
    },
    );
  }
  atras(){
    this.navCtrl.pop();
  }


}
