var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrRutasProvider } from '../../providers/pr-rutas/pr-rutas';
import { PrInmuebleProvider } from '../../providers/pr-inmueble/pr-inmueble';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
var PropiedadFavoritaPage = /** @class */ (function () {
    function PropiedadFavoritaPage(navCtrl, navParams, pr_rutas, pr_inmueble, pr_alert_toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_rutas = pr_rutas;
        this.pr_inmueble = pr_inmueble;
        this.pr_alert_toast = pr_alert_toast;
        this.ruta_imagenes = this.pr_rutas.get_ruta_imagenes();
    }
    PropiedadFavoritaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PropiedadFavoritaPage');
        this.propiedades_favoritas();
    };
    PropiedadFavoritaPage.prototype.propiedades_favoritas = function () {
        var _this = this;
        this.user = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        this.pr_inmueble.get_favoritos(this.user.id).subscribe(function (result) {
            console.log(result);
            _this.inmuebles = result;
        });
    };
    PropiedadFavoritaPage.prototype.atras = function () {
        this.navCtrl.setRoot('HomePage');
    };
    PropiedadFavoritaPage.prototype.get_det_inmueble = function (item) {
        var data = item;
        var data_inmueble = {
            id_inmueble: data.id_inmueble
        };
        console.log(data);
        var mensaje = 'Cargando';
        var data_det_inmuebles = {
            det_inmuebles: data,
            imagenes: [],
            caracteristicas: [],
            servicios: []
        };
        this.pr_alert_toast.show_loading(mensaje);
        localStorage.setItem('det_inmueble', JSON.stringify(data_det_inmuebles));
        this.navCtrl.push('DetInmueblePage');
        /*
         this.pr_inmueble.get_imagenes_caracteristicas_servicios_inmueble(data_inmueble).subscribe(
            pr_inmueble =>{
              this.pr_alert_toast.dismis_loading();
              let resultado=pr_inmueble;
              if(resultado.status==true){
                let data_det_inmuebles={
                  det_inmuebles:data,
                  imagenes:resultado.imagenes,
                  caracteristicas:resultado.caracteristicas,
                  servicios:resultado.servicios
                }
               localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
               this.navCtrl.push('DetInmueblePage');
              }
            },
            err => {console.log('el error '+err);
            },
          );
          */
    };
    PropiedadFavoritaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-propiedad-favorita',
            templateUrl: 'propiedad-favorita.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            PrRutasProvider,
            PrInmuebleProvider,
            PrAlertToastProvider])
    ], PropiedadFavoritaPage);
    return PropiedadFavoritaPage;
}());
export { PropiedadFavoritaPage };
//# sourceMappingURL=propiedad-favorita.js.map