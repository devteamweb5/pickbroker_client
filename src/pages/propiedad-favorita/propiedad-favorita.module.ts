import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropiedadFavoritaPage } from './propiedad-favorita';

@NgModule({
  declarations: [
    PropiedadFavoritaPage,
  ],
  imports: [
    IonicPageModule.forChild(PropiedadFavoritaPage),
  ],
})
export class PropiedadFavoritaPageModule {}
