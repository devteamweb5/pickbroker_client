import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';
import {PrInmuebleProvider} from '../../providers/pr-inmueble/pr-inmueble';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
@IonicPage()
@Component({
  selector: 'page-propiedad-favorita',
  templateUrl: 'propiedad-favorita.html',
})
export class PropiedadFavoritaPage {
	var_propiedades_favoritas:any;
	ruta_imagenes:any;
  user;
  inmuebles;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public pr_rutas:PrRutasProvider,
    public pr_inmueble:PrInmuebleProvider,
    public pr_alert_toast:PrAlertToastProvider,
)   {
  	this.ruta_imagenes=this.pr_rutas.get_ruta_imagenes();
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad PropiedadFavoritaPage');
    this.propiedades_favoritas();
  }
  propiedades_favoritas(){
    this.user = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
  	this.pr_inmueble.get_favoritos(this.user.id).subscribe(result=>{
      console.log(result);
      this.inmuebles = result;
    });
  }
  atras(){
    this.navCtrl.setRoot('HomePage');
  }
  get_det_inmueble(item){
    console.log(item);
    let mensaje='Cargando';
    let data_det_inmuebles={
      det_inmuebles:item.inmueble,
      imagenes:item.imagenes,
      caracteristicas:item.inmueble.caracteristicas ? item.inmueble.caracteristicas.split(',') : [] ,
      servicios:item.inmueble.servicios ? item.inmueble.servicios.split(',') : []
    }
    this.pr_alert_toast.show_loading(mensaje);
    localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
    this.navCtrl.push('DetInmueblePage');
    /*
     this.pr_inmueble.get_imagenes_caracteristicas_servicios_inmueble(data_inmueble).subscribe(
        pr_inmueble =>{
          this.pr_alert_toast.dismis_loading();
          let resultado=pr_inmueble;
          if(resultado.status==true){
            let data_det_inmuebles={
              det_inmuebles:data,
              imagenes:resultado.imagenes,
              caracteristicas:resultado.caracteristicas,
              servicios:resultado.servicios
            }
           localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
           this.navCtrl.push('DetInmueblePage');
          }
        },
        err => {console.log('el error '+err);
        },
      );
      */
    }

  }
