import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrTerminosPoliticasProvider} from '../../providers/pr-terminos-politicas/pr-terminos-politicas';
@IonicPage()
@Component({
  selector: 'page-politicas-privacidad',
  templateUrl: 'politicas-privacidad.html',
})
export class PoliticasPrivacidadPage {
	terminos_condiciones:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_alert_toast:PrAlertToastProvider, public pr_terminos_condiciones:PrTerminosPoliticasProvider) {
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad PoliticasPrivacidadPage');
    //this.get_terminos_condiciones();
  }
  get_terminos_condiciones(){
  	let mensaje='Cargando';
  	this.pr_alert_toast.show_loading(mensaje);
	  this.pr_terminos_condiciones.get_terminos_condiciones('').subscribe(
	    pr_terminos_condiciones =>{
	    	this.pr_alert_toast.dismis_loading();
	      let resultado=pr_terminos_condiciones;
	      if(resultado.mensaje=='datos_registro'){
					this.terminos_condiciones=resultado.data;
					console.log(this.terminos_condiciones);
	      }else{
	      	let mensaje='No hay datos';
					this.pr_alert_toast.mensaje_toast_pie(mensaje);
	      }
	    },
	    err => {console.log('el error '+err);
	    },
	  );
  }

    atras(){
    this.navCtrl.pop();
  }

}
