var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrTerminosPoliticasProvider } from '../../providers/pr-terminos-politicas/pr-terminos-politicas';
var PoliticasPrivacidadPage = /** @class */ (function () {
    function PoliticasPrivacidadPage(navCtrl, navParams, pr_alert_toast, pr_terminos_condiciones) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_terminos_condiciones = pr_terminos_condiciones;
    }
    PoliticasPrivacidadPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PoliticasPrivacidadPage');
        //this.get_terminos_condiciones();
    };
    PoliticasPrivacidadPage.prototype.get_terminos_condiciones = function () {
        var _this = this;
        var mensaje = 'Cargando';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_terminos_condiciones.get_terminos_condiciones('').subscribe(function (pr_terminos_condiciones) {
            _this.pr_alert_toast.dismis_loading();
            var resultado = pr_terminos_condiciones;
            if (resultado.mensaje == 'datos_registro') {
                _this.terminos_condiciones = resultado.data;
                console.log(_this.terminos_condiciones);
            }
            else {
                var mensaje_1 = 'No hay datos';
                _this.pr_alert_toast.mensaje_toast_pie(mensaje_1);
            }
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    PoliticasPrivacidadPage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    PoliticasPrivacidadPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-politicas-privacidad',
            templateUrl: 'politicas-privacidad.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrAlertToastProvider, PrTerminosPoliticasProvider])
    ], PoliticasPrivacidadPage);
    return PoliticasPrivacidadPage;
}());
export { PoliticasPrivacidadPage };
//# sourceMappingURL=politicas-privacidad.js.map