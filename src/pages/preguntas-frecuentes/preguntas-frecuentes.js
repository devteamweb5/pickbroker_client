var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrPreguntasFrecuentesProvider } from '../../providers/pr-preguntas-frecuentes/pr-preguntas-frecuentes';
var PreguntasFrecuentesPage = /** @class */ (function () {
    function PreguntasFrecuentesPage(navCtrl, navParams, pr_alert_toast, pr_preguntas_frecuentes) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_preguntas_frecuentes = pr_preguntas_frecuentes;
    }
    PreguntasFrecuentesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PreguntasFrecuentesPage');
        this.get_preguntas_frecuentes();
    };
    PreguntasFrecuentesPage.prototype.get_preguntas_frecuentes = function () {
        var _this = this;
        var mensaje = 'Cargando';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_preguntas_frecuentes.get_preguntas_frecuentes().subscribe(function (result) {
            _this.pr_alert_toast.dismis_loading();
            if (result) {
                _this.preguntas_frecuentes = result;
            }
            else {
                var mensaje_1 = 'No hay datos';
                _this.pr_alert_toast.mensaje_toast_pie(mensaje_1);
            }
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    PreguntasFrecuentesPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-preguntas-frecuentes',
            templateUrl: 'preguntas-frecuentes.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrAlertToastProvider, PrPreguntasFrecuentesProvider])
    ], PreguntasFrecuentesPage);
    return PreguntasFrecuentesPage;
}());
export { PreguntasFrecuentesPage };
//# sourceMappingURL=preguntas-frecuentes.js.map