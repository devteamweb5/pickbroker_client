import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrPreguntasFrecuentesProvider} from '../../providers/pr-preguntas-frecuentes/pr-preguntas-frecuentes';
@IonicPage()
@Component({
  selector: 'page-preguntas-frecuentes',
  templateUrl: 'preguntas-frecuentes.html',
})
export class PreguntasFrecuentesPage {
	preguntas_frecuentes:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_alert_toast:PrAlertToastProvider, public pr_preguntas_frecuentes:PrPreguntasFrecuentesProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PreguntasFrecuentesPage');
    this.get_preguntas_frecuentes();
  }
  get_preguntas_frecuentes(){
  	let mensaje='Cargando';
  	this.pr_alert_toast.show_loading(mensaje);
  	this.pr_preguntas_frecuentes.get_preguntas_frecuentes().subscribe(
	    result => {
			this.pr_alert_toast.dismis_loading();
	      if(result){
				this.preguntas_frecuentes=result;
	      }else{
	      	let mensaje='No hay datos';
  				this.pr_alert_toast.mensaje_toast_pie(mensaje);
	      }
	    },
	    err => {console.log('el error '+err);
	    },
	  );
  }

}
