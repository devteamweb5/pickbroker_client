import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import {PrLlamadasProvider} from '../../providers/pr-llamadas/pr-llamadas';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrCitasProvider} from '../../providers/pr-citas/pr-citas';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas'

@IonicPage()
@Component({
  selector: 'page-inmueble-contactart-dueno',
  templateUrl: 'inmueble-contactart-dueno.html',
})
export class InmuebleContactartDuenoPage {
	det_inmueble:any={
		id_usuario_agente:'',
		nombre_empresa_asociada:'',
		direccion:'',
		nombre_usuario:'',
		telf_usuario:'',
    id_usuario:''
	};
  data_inmueble:any={
    nombre:'',
    mensaje:'',
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public callNumber:CallNumber,
    public pr_llamada:PrLlamadasProvider,
    public pr_alert_toast:PrAlertToastProvider,
    public pr_citas:PrCitasProvider,
    public pr_rutas:PrRutasProvider,
    ) {
  }
  atras(){
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InmuebleContactartDuenoPage');
    this.dueno();
  }
  dueno(){
  	let data=JSON.parse(localStorage.getItem('det_inmueble'));
    console.log(data);
  	this.det_inmueble.nombre_empresa_asociada= 'Blue Entreprise';
  	this.det_inmueble.direccion=data.det_inmuebles.direccion;
  	this.det_inmueble.nombre_usuario=data.det_inmuebles.user.name;
  	this.det_inmueble.id_usuario_agente= 637182;
  	this.det_inmueble.telf_usuario=data.det_inmuebles.user.phone;
    this.det_inmueble.id_usuario = data.det_inmuebles.user.id;
    this.det_inmueble.imagen = data.det_inmuebles.user.image;
  	console.log(this.det_inmueble);
  }
  enviar_mensaje_push(){
    if(this.data_inmueble.nombre=='') {
      let mensaje='Ingrese un nombre';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else if(this.data_inmueble.mensaje==''){
      let mensaje='Ingrese un mensaje';
      this.pr_alert_toast.mensaje_toast_pie(mensaje);
    }else{
      let user=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
      console.log(user);
      let data={
        envia:user.id,
        recibe: this.det_inmueble.id_usuario,
        //nombre:this.data_inmueble.nombre,
        mensaje:this.data_inmueble.mensaje
      }
      let mensaje='Cargando';
      this.pr_alert_toast.show_loading(mensaje);
      this.pr_citas.enviar_mensaje_socio(data).subscribe(
        result =>{
          this.pr_alert_toast.dismis_loading();
          let mensaje='Mensaje enviado';
          this.pr_alert_toast.mensaje_toast_pie(mensaje);
          this.navCtrl.pop();
          /*
          if(resultado.status==true){
            let mensaje='Mensaje enviado';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
            this.navCtrl.pop();
          }else{
            let mensaje='Mensaje NO enviado';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
          }
          */
        },
        err => {console.log('el error '+err);
      },
      );

    }
  }
  llamar(){
  	console.log(this.det_inmueble.telf_usuario);
  	this.callNumber.callNumber(this.det_inmueble.telf_usuario, true)
  	.then(res =>{
      let datos_usuario=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
      let data={
        user:datos_usuario.id,
        called:this.det_inmueble.id_usuario
      }	
      console.log(data);
      console.log(this.det_inmueble);
      this.pr_llamada.guardar_llamadas(data).subscribe(
        result =>{
          console.log(result);
        },
        err => {console.log('el error '+err);
      },
      );
      console.log('Launched dialer!', res)
    })
  	.catch(err => console.log('Error launching dialer', err));
  }
  agendar_cita_cliente_agente(){
  	this.navCtrl.push('CitaAgendarPage');
  }

}
