var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { PrLlamadasProvider } from '../../providers/pr-llamadas/pr-llamadas';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrCitasProvider } from '../../providers/pr-citas/pr-citas';
var InmuebleContactartDuenoPage = /** @class */ (function () {
    function InmuebleContactartDuenoPage(navCtrl, navParams, callNumber, pr_llamada, pr_alert_toast, pr_citas) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.callNumber = callNumber;
        this.pr_llamada = pr_llamada;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_citas = pr_citas;
        this.det_inmueble = {
            id_usuario_agente: '',
            nombre_empresa_asociada: '',
            direccion: '',
            nombre_usuario: '',
            telf_usuario: '',
            id_usuario: ''
        };
        this.data_inmueble = {
            nombre: '',
            mensaje: '',
        };
    }
    InmuebleContactartDuenoPage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    InmuebleContactartDuenoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InmuebleContactartDuenoPage');
        this.dueno();
    };
    InmuebleContactartDuenoPage.prototype.dueno = function () {
        var data = JSON.parse(localStorage.getItem('det_inmueble'));
        console.log(data);
        this.det_inmueble.nombre_empresa_asociada = 'Blue Entreprise';
        this.det_inmueble.direccion = data.det_inmuebles.direccion;
        this.det_inmueble.nombre_usuario = data.det_inmuebles.user.name;
        this.det_inmueble.id_usuario_agente = 637182;
        this.det_inmueble.telf_usuario = data.det_inmuebles.user.phone;
        this.det_inmueble.id_usuario = data.det_inmuebles.user.id;
        console.log(this.det_inmueble);
    };
    InmuebleContactartDuenoPage.prototype.enviar_mensaje_push = function () {
        var _this = this;
        if (this.data_inmueble.nombre == '') {
            var mensaje = 'Ingrese un nombre';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_inmueble.mensaje == '') {
            var mensaje = 'Ingrese un mensaje';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else {
            var user = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
            console.log(user);
            var data = {
                envia: user.id,
                recibe: this.det_inmueble.id_usuario,
                //nombre:this.data_inmueble.nombre,
                mensaje: this.data_inmueble.mensaje
            };
            var mensaje = 'Cargando';
            this.pr_alert_toast.show_loading(mensaje);
            this.pr_citas.enviar_mensaje_socio(data).subscribe(function (result) {
                _this.pr_alert_toast.dismis_loading();
                var mensaje = 'Mensaje enviado';
                _this.pr_alert_toast.mensaje_toast_pie(mensaje);
                _this.navCtrl.pop();
                /*
                if(resultado.status==true){
                  let mensaje='Mensaje enviado';
                  this.pr_alert_toast.mensaje_toast_pie(mensaje);
                  this.navCtrl.pop();
                }else{
                  let mensaje='Mensaje NO enviado';
                  this.pr_alert_toast.mensaje_toast_pie(mensaje);
                }
                */
            }, function (err) {
                console.log('el error ' + err);
            });
        }
    };
    InmuebleContactartDuenoPage.prototype.llamar = function () {
        var _this = this;
        console.log(this.det_inmueble.telf_usuario);
        this.callNumber.callNumber(this.det_inmueble.telf_usuario, true)
            .then(function (res) {
            var datos_usuario = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
            var data = {
                user: datos_usuario.id,
                called: _this.det_inmueble.id_usuario
            };
            console.log(data);
            console.log(_this.det_inmueble);
            _this.pr_llamada.guardar_llamadas(data).subscribe(function (result) {
                console.log(result);
            }, function (err) {
                console.log('el error ' + err);
            });
            console.log('Launched dialer!', res);
        })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    InmuebleContactartDuenoPage.prototype.agendar_cita_cliente_agente = function () {
        this.navCtrl.push('CitaAgendarPage');
    };
    InmuebleContactartDuenoPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-inmueble-contactart-dueno',
            templateUrl: 'inmueble-contactart-dueno.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            CallNumber,
            PrLlamadasProvider,
            PrAlertToastProvider,
            PrCitasProvider])
    ], InmuebleContactartDuenoPage);
    return InmuebleContactartDuenoPage;
}());
export { InmuebleContactartDuenoPage };
//# sourceMappingURL=inmueble-contactart-dueno.js.map