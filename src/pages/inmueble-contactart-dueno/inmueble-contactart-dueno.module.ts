import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InmuebleContactartDuenoPage } from './inmueble-contactart-dueno';

@NgModule({
  declarations: [
    InmuebleContactartDuenoPage,
  ],
  imports: [
    IonicPageModule.forChild(InmuebleContactartDuenoPage),
  ],
})
export class InmuebleContactartDuenoPageModule {}
