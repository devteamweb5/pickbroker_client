import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrMembresiaProvider} from '../../providers/pr-membresia/pr-membresia';

@IonicPage()
@Component({
  selector: 'page-membresias-listado',
  templateUrl: 'membresias-listado.html',
})
export class MembresiasListadoPage{
	listado_publicacion:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_membresia:PrMembresiaProvider){
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad MembresiasListadoPage');
    this.get_precio_publicacion_propiedades();
  }
  get_precio_publicacion_propiedades(){
  	 this.pr_membresia.get_precio_publicacion_propiedades('').subscribe(
  	  pr_membresia =>{
	      let resultado=pr_membresia;
	      if(resultado.status==true){
					this.listado_publicacion=resultado.data;
					console.log(this.listado_publicacion);
	      }
	    },
	    err => {console.log('el error '+err);
	    },
  	  );
  }
	get_membresia_seleccionada(item){
  	let datos=item;
  	 localStorage.setItem('membresia_seleccionada',JSON.stringify(datos));
  	this.navCtrl.push('MembresiaComprarPage');
  }

}
