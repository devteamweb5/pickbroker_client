import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MembresiasListadoPage } from './membresias-listado';

@NgModule({
  declarations: [
    MembresiasListadoPage,
  ],
  imports: [
    IonicPageModule.forChild(MembresiasListadoPage),
  ],
})
export class MembresiasListadoPageModule {}
