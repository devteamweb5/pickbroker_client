var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrMembresiaProvider } from '../../providers/pr-membresia/pr-membresia';
var MembresiasListadoPage = /** @class */ (function () {
    function MembresiasListadoPage(navCtrl, navParams, pr_membresia) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_membresia = pr_membresia;
    }
    MembresiasListadoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MembresiasListadoPage');
        this.get_precio_publicacion_propiedades();
    };
    MembresiasListadoPage.prototype.get_precio_publicacion_propiedades = function () {
        var _this = this;
        this.pr_membresia.get_precio_publicacion_propiedades('').subscribe(function (pr_membresia) {
            var resultado = pr_membresia;
            if (resultado.status == true) {
                _this.listado_publicacion = resultado.data;
                console.log(_this.listado_publicacion);
            }
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    MembresiasListadoPage.prototype.get_membresia_seleccionada = function (item) {
        var datos = item;
        localStorage.setItem('membresia_seleccionada', JSON.stringify(datos));
        this.navCtrl.push('MembresiaComprarPage');
    };
    MembresiasListadoPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-membresias-listado',
            templateUrl: 'membresias-listado.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrMembresiaProvider])
    ], MembresiasListadoPage);
    return MembresiasListadoPage;
}());
export { MembresiasListadoPage };
//# sourceMappingURL=membresias-listado.js.map