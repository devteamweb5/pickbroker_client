import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrTarjetasProvider} from '../../providers/pr-tarjetas/pr-tarjetas';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrInmuebleProvider} from '../../providers/pr-inmueble/pr-inmueble';
declare var OpenPay;



@IonicPage()
@Component({
  selector: 'page-membresia-comprar',
  templateUrl: 'membresia-comprar.html',
})
export class MembresiaComprarPage {
	membresia_seleccionada:any[]=[];
	tarjetas:any=[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_tarjetas:PrTarjetasProvider, public pr_alert_toast:PrAlertToastProvider, public pr_inmueble:PrInmuebleProvider) {
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad MembresiaComprarPage');
    this.get_membresia_sleccionada();
  }
  ionViewWillEnter(){
    this.get_tarjetas_storage();
  }
  get_membresia_sleccionada(){
  	this.membresia_seleccionada=JSON.parse(localStorage.getItem('membresia_seleccionada'));
  	console.log(this.membresia_seleccionada);
  }
  get_tarjetas_storage(){
  	this.tarjetas=JSON.parse(localStorage.getItem('tarjetas_picbroker'));
  	console.log(this.tarjetas);
  }
  nueva_tarjeta(){
  	this.navCtrl.push('TarjetaAgregarPage');
  }
  seleccionar_tarjeta(item){
    OpenPay.setSandboxMode(true);
    OpenPay.setId('mtfxe0rwm5ssuaiijz5d');
    OpenPay.setApiKey('pk_0fee13dd1b3744d3a5568a65ce6d4aed');
    let usuario_pick=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    let orden=Math.random() * 100000;
    let datos_tarjeta={
     customer_id:'',
     method:"Card",
     amount:this.membresia_seleccionada['precio'],
     currency:"MXN",
     description:this.membresia_seleccionada['descripcion'],
     order_id:'picbr-'+Math.round(orden),
     name: '',
     last_name: '' ,
     phone_number:'',
     email:'',
     source_id:item.source_id,
     device_session_id:'',
    }
    var deviceDataId = OpenPay.deviceData.setup(datos_tarjeta);
    datos_tarjeta.device_session_id=deviceDataId;
    console.log(deviceDataId);
    console.log(item);
    for(let value of usuario_pick){
     let nombre_apellido=value.nombre.split(' ');
     datos_tarjeta.name=nombre_apellido['0'];
     datos_tarjeta.last_name=nombre_apellido['1'];
     datos_tarjeta.phone_number='523312'+orden;
     datos_tarjeta.email=value.login;
     datos_tarjeta.customer_id=value.token_open_pay;
    }
    console.log(datos_tarjeta);
    let mensaje='Cargando';
    this.pr_alert_toast.show_loading(mensaje);
     this.pr_tarjetas.hacer_cargo_tarjeta(datos_tarjeta).subscribe(
        pr_tarjetas => {
         
          let resultado=pr_tarjetas;
          if(resultado.status==true){
            let datos=JSON.parse(resultado.data);
            if(datos.error_code) {
              let mensaje='Ocurrió un error al hacer el pago, intente nuevamente';
              this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }else{
             this.cambiar_status_inmueble();
            }
          }
        },
        err => {console.log('el error '+err);
        },
      );

  }
  cambiar_status_inmueble(){
    let datos_inmueble=JSON.parse(localStorage.getItem('inmueble_guardado'));
    console.log(datos_inmueble);
    let data_inmueble={
       id_inmueble:21,/*datos_inmueble.id_inmueble,*/
       id_status_publicacion:4,
       dias:this.membresia_seleccionada['dias']
    }
     this.pr_inmueble.cambiar_status_inmueble_pagado(data_inmueble).subscribe(
        pr_inmueble => {
           this.pr_alert_toast.dismis_loading();
          let resultado=pr_inmueble;
          if(resultado.status==true){
            let mensaje='Inmueble pagado';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
            this.navCtrl.setRoot('HomePage');
          }else{
            let mensaje='No se cambió el status del inmueble, Comuniquese con Administrador';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
          }
        },
        err => {console.log('el error '+err);
        },
      );
  }
}
