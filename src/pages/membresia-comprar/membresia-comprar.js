var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrTarjetasProvider } from '../../providers/pr-tarjetas/pr-tarjetas';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrInmuebleProvider } from '../../providers/pr-inmueble/pr-inmueble';
var MembresiaComprarPage = /** @class */ (function () {
    function MembresiaComprarPage(navCtrl, navParams, pr_tarjetas, pr_alert_toast, pr_inmueble) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_tarjetas = pr_tarjetas;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_inmueble = pr_inmueble;
        this.membresia_seleccionada = [];
        this.tarjetas = [];
    }
    MembresiaComprarPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MembresiaComprarPage');
        this.get_membresia_sleccionada();
    };
    MembresiaComprarPage.prototype.ionViewWillEnter = function () {
        this.get_tarjetas_storage();
    };
    MembresiaComprarPage.prototype.get_membresia_sleccionada = function () {
        this.membresia_seleccionada = JSON.parse(localStorage.getItem('membresia_seleccionada'));
        console.log(this.membresia_seleccionada);
    };
    MembresiaComprarPage.prototype.get_tarjetas_storage = function () {
        this.tarjetas = JSON.parse(localStorage.getItem('tarjetas_picbroker'));
        console.log(this.tarjetas);
    };
    MembresiaComprarPage.prototype.nueva_tarjeta = function () {
        this.navCtrl.push('TarjetaAgregarPage');
    };
    MembresiaComprarPage.prototype.seleccionar_tarjeta = function (item) {
        var _this = this;
        OpenPay.setSandboxMode(true);
        OpenPay.setId('mtfxe0rwm5ssuaiijz5d');
        OpenPay.setApiKey('pk_0fee13dd1b3744d3a5568a65ce6d4aed');
        var usuario_pick = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        var orden = Math.random() * 100000;
        var datos_tarjeta = {
            customer_id: '',
            method: "Card",
            amount: this.membresia_seleccionada['precio'],
            currency: "MXN",
            description: this.membresia_seleccionada['descripcion'],
            order_id: 'picbr-' + Math.round(orden),
            name: '',
            last_name: '',
            phone_number: '',
            email: '',
            source_id: item.source_id,
            device_session_id: '',
        };
        var deviceDataId = OpenPay.deviceData.setup(datos_tarjeta);
        datos_tarjeta.device_session_id = deviceDataId;
        console.log(deviceDataId);
        console.log(item);
        for (var _i = 0, usuario_pick_1 = usuario_pick; _i < usuario_pick_1.length; _i++) {
            var value = usuario_pick_1[_i];
            var nombre_apellido = value.nombre.split(' ');
            datos_tarjeta.name = nombre_apellido['0'];
            datos_tarjeta.last_name = nombre_apellido['1'];
            datos_tarjeta.phone_number = '523312' + orden;
            datos_tarjeta.email = value.login;
            datos_tarjeta.customer_id = value.token_open_pay;
        }
        console.log(datos_tarjeta);
        var mensaje = 'Cargando';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_tarjetas.hacer_cargo_tarjeta(datos_tarjeta).subscribe(function (pr_tarjetas) {
            var resultado = pr_tarjetas;
            if (resultado.status == true) {
                var datos = JSON.parse(resultado.data);
                if (datos.error_code) {
                    var mensaje_1 = 'Ocurrió un error al hacer el pago, intente nuevamente';
                    _this.pr_alert_toast.mensaje_toast_pie(mensaje_1);
                }
                else {
                    _this.cambiar_status_inmueble();
                }
            }
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    MembresiaComprarPage.prototype.cambiar_status_inmueble = function () {
        var _this = this;
        var datos_inmueble = JSON.parse(localStorage.getItem('inmueble_guardado'));
        console.log(datos_inmueble);
        var data_inmueble = {
            id_inmueble: 21,
            id_status_publicacion: 4,
            dias: this.membresia_seleccionada['dias']
        };
        this.pr_inmueble.cambiar_status_inmueble_pagado(data_inmueble).subscribe(function (pr_inmueble) {
            _this.pr_alert_toast.dismis_loading();
            var resultado = pr_inmueble;
            if (resultado.status == true) {
                var mensaje = 'Inmueble pagado';
                _this.pr_alert_toast.mensaje_toast_pie(mensaje);
                _this.navCtrl.setRoot('HomePage');
            }
            else {
                var mensaje = 'No se cambió el status del inmueble, Comuniquese con Administrador';
                _this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    MembresiaComprarPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-membresia-comprar',
            templateUrl: 'membresia-comprar.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrTarjetasProvider, PrAlertToastProvider, PrInmuebleProvider])
    ], MembresiaComprarPage);
    return MembresiaComprarPage;
}());
export { MembresiaComprarPage };
//# sourceMappingURL=membresia-comprar.js.map