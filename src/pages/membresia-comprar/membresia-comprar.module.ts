import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MembresiaComprarPage } from './membresia-comprar';

@NgModule({
  declarations: [
    MembresiaComprarPage,
  ],
  imports: [
    IonicPageModule.forChild(MembresiaComprarPage),
  ],
})
export class MembresiaComprarPageModule {}
