import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisPropiedadesDetallePage } from './mis-propiedades-detalle';

@NgModule({
  declarations: [
    MisPropiedadesDetallePage,
  ],
  imports: [
    IonicPageModule.forChild(MisPropiedadesDetallePage),
  ],
})
export class MisPropiedadesDetallePageModule {}
