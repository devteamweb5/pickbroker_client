var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Slides } from 'ionic-angular';
import { PrInmuebleProvider } from '../../providers/pr-inmueble/pr-inmueble';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrRutasProvider } from '../../providers/pr-rutas/pr-rutas';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { PhotoViewer } from '@ionic-native/photo-viewer';
var MisPropiedadesDetallePage = /** @class */ (function () {
    function MisPropiedadesDetallePage(navCtrl, navParams, alertCtrl, pr_inmueble, pr_alert_toast, pr_rutas, iab, photoViewer) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.pr_inmueble = pr_inmueble;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_rutas = pr_rutas;
        this.iab = iab;
        this.photoViewer = photoViewer;
        this.det_inmueble = {
            id_inmueble: '',
            titulo: '',
            imagen: '',
            precio: '',
            direccion: '',
            lat_long: '',
            descripcion: '',
            terreno: '',
            construido: '',
            recamaras: '',
            banios: '',
            cochera: '',
            antiguedad: '',
            fecha_publicacion: '',
            fecha_vencimiento: '',
            status_publicacion: '',
        };
        this.ruta_imagenes = this.pr_rutas.get_ruta_imagenes();
    }
    MisPropiedadesDetallePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PropiedadesDetallePage');
        this.get_det_inmueble();
    };
    MisPropiedadesDetallePage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    MisPropiedadesDetallePage.prototype.mostrar_zoom = function (item) {
        this.photoViewer.show(this.ruta_imagenes + item);
    };
    MisPropiedadesDetallePage.prototype.get_det_inmueble = function () {
        var data = JSON.parse(localStorage.getItem('det_inmueble'));
        this.inmueble = JSON.parse(localStorage.getItem('det_inmueble'));
        console.log(this.inmueble.det_inmuebles);
        console.log(data);
        this.det_inmueble.id_inmueble = data.det_inmuebles.id;
        this.det_inmueble.id_status_publicacion = data.det_inmuebles.status.id;
        this.det_inmueble.imagen = data.det_inmuebles.imagen;
        this.det_inmueble.titulo = data.det_inmuebles.titulo;
        this.det_inmueble.precio = data.det_inmuebles.precio;
        this.det_inmueble.direccion = data.det_inmuebles.direccion;
        this.det_inmueble.lat_long = data.det_inmuebles.lat_long;
        this.det_inmueble.descripcion = data.det_inmuebles.descripcion;
        this.det_inmueble.terreno = data.det_inmuebles.terreno;
        this.det_inmueble.construido = data.det_inmuebles.construido;
        this.det_inmueble.recamaras = data.det_inmuebles.recamaras;
        this.det_inmueble.banios = data.det_inmuebles.banios;
        this.det_inmueble.cochera = data.det_inmuebles.cochera;
        this.det_inmueble.antiguedad = data.det_inmuebles.antiguedad;
        this.det_inmueble.fecha_publicacion = data.det_inmuebles.fecha_publicacion;
        this.det_inmueble.fecha_vencimiento = data.det_inmuebles.fecha_vencimiento;
        this.imagenes = data.imagenes;
        this.caracteristica = data.caracteristicas;
        this.servicio = data.servicios;
    };
    MisPropiedadesDetallePage.prototype.editar_inmueble = function () {
        this.navCtrl.push('MisPropiedadesEditarPage');
    };
    MisPropiedadesDetallePage.prototype.cambiar_status_inmueble = function () {
        var _this = this;
        var mensaje;
        console.log(this.det_inmueble.id_status_publicacion);
        if (this.det_inmueble.id_status_publicacion == 3 || this.det_inmueble.id_status_publicacion == 1) {
            mensaje = 'Inactivo';
        }
        else {
            mensaje = 'Activo';
        }
        var alert = this.alertCtrl.create({
            title: 'Advertencia',
            subTitle: 'Cambiará el status a ' + mensaje + ', ¿desea continuar?',
            buttons: [
                {
                    text: 'Si',
                    handler: function () {
                        _this.cambiar_status();
                    }
                },
                {
                    text: 'No',
                    handler: function () {
                    }
                }
            ]
        });
        alert.present();
    };
    MisPropiedadesDetallePage.prototype.cambiar_status = function () {
        var _this = this;
        var mensaje = 'Cambiando status de inmueble';
        this.pr_alert_toast.show_loading(mensaje);
        var data = { "inmueble": this.det_inmueble.id_inmueble };
        this.pr_inmueble.cambiar_status_inmueble(data).subscribe(function (result) {
            _this.pr_alert_toast.dismis_loading();
            var mensaje = 'Inmueble actualizado correctamente';
            _this.pr_alert_toast.mensaje_toast_pie(mensaje);
            _this.navCtrl.pop();
        }, function (err) {
            console.log(err);
        });
    };
    MisPropiedadesDetallePage.prototype.editar = function () {
        this.navCtrl.push('MisPropiedadesEditarPage', {
            'id': this.inmueble.det_inmuebles.id
        });
    };
    __decorate([
        ViewChild(Slides),
        __metadata("design:type", Slides)
    ], MisPropiedadesDetallePage.prototype, "slides", void 0);
    MisPropiedadesDetallePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-mis-propiedades-detalle',
            templateUrl: 'mis-propiedades-detalle.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, AlertController, PrInmuebleProvider, PrAlertToastProvider, PrRutasProvider, InAppBrowser, PhotoViewer])
    ], MisPropiedadesDetallePage);
    return MisPropiedadesDetallePage;
}());
export { MisPropiedadesDetallePage };
//# sourceMappingURL=mis-propiedades-detalle.js.map