import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Slides } from 'ionic-angular';
import {PrInmuebleProvider} from '../../providers/pr-inmueble/pr-inmueble';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { PhotoViewer } from '@ionic-native/photo-viewer';
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-mis-propiedades-detalle',
  templateUrl: 'mis-propiedades-detalle.html',
})
export class MisPropiedadesDetallePage {
  @ViewChild(Slides) slides: Slides;
	imagenes:any;
  inmueble;
	det_inmueble:any={
		id_inmueble:'',
		titulo:'',
    imagen:'',
		precio:'',
		direccion:'',
		lat_long:'',
		descripcion:'',
		terreno:'',
		construido:'',
		recamaras:'',
		banios:'',
		cochera:'',
		antiguedad:'',
		fecha_publicacion:'',
		fecha_vencimiento:'',
    status_publicacion:'',
	};
  map:any;
  lat:any;
  long:any;
	caracteristica:any;
	servicio:any;
  ruta_imagenes:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl:AlertController,public pr_inmueble: PrInmuebleProvider, public pr_alert_toast:PrAlertToastProvider, public pr_rutas:PrRutasProvider, public iab:InAppBrowser, private photoViewer: PhotoViewer) {
    this.ruta_imagenes=this.pr_rutas.get_ruta_imagenes();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PropiedadesDetallePage');
    this.get_det_inmueble();
  }
  atras(){
    this.navCtrl.pop();
  }
  mostrar_zoom(item){
    this.photoViewer.show(this.ruta_imagenes+item);
  }
  get_det_inmueble(){
    let data=JSON.parse(localStorage.getItem('det_inmueble'));
  	this.inmueble = JSON.parse(localStorage.getItem('det_inmueble'));
    console.log(this.inmueble.det_inmuebles);
  	console.log(data);
    this.det_inmueble.id_inmueble=data.det_inmuebles.id;
    this.det_inmueble.id_status_publicacion=data.det_inmuebles.status.id;
		this.det_inmueble.imagen=data.det_inmuebles.imagen;
		this.det_inmueble.titulo=data.det_inmuebles.titulo;
		this.det_inmueble.precio=data.det_inmuebles.precio;
		this.det_inmueble.direccion=data.det_inmuebles.direccion;
		this.det_inmueble.lat_long=data.det_inmuebles.latLong;
    let lat_explota= data.det_inmuebles.latLong.split(',');
    this.lat=lat_explota['0'];
    this.long=lat_explota['1'];
		this.det_inmueble.descripcion=data.det_inmuebles.descripcion;
		this.det_inmueble.terreno=data.det_inmuebles.terreno;
		this.det_inmueble.construido=data.det_inmuebles.construido;
		this.det_inmueble.recamaras=data.det_inmuebles.recamaras;
		this.det_inmueble.banios=data.det_inmuebles.banios;
		this.det_inmueble.cochera=data.det_inmuebles.cochera;
		this.det_inmueble.antiguedad=data.det_inmuebles.antiguedad;
		this.det_inmueble.fecha_publicacion=data.det_inmuebles.fecha_publicacion;
		this.det_inmueble.fecha_vencimiento=data.det_inmuebles.fecha_vencimiento;
		this.imagenes=data.imagenes;
		this.caracteristica=data.caracteristicas;
		this.servicio=data.servicios;
     setTimeout(()=>{
      this.map=this.loadMap();
    },1000);
  }
  editar_inmueble(){
  	this.navCtrl.push('MisPropiedadesEditarPage');
  }
  loadMap(location= new google.maps.LatLng(20.708799, -103.410071)){
    console.log('mapa cargado');
    let mapOptions={
      center: location,
      zoom:16, 
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    }
    let mapEl=document.getElementById('map_canvas2');
    let map= new google.maps.Map(mapEl,mapOptions);
    // Wait the MAP_READY before using any methods.
    navigator.geolocation.getCurrentPosition(
      (position) => {
        let newLatLng = new google.maps.LatLng(this.lat,this.long);
        map.setCenter(newLatLng);
        let lat=position.coords.latitude;
        let long=position.coords.longitude;
        var icono ="assets/imgs/pin.png";
        console.log(icono);

        let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: newLatLng,
          draggable: false,
          icon:icono
        });
      }
      );
    return map;
  }
  cambiar_status_inmueble(){
    let mensaje;
    console.log(this.det_inmueble.id_status_publicacion);
    if(this.det_inmueble.id_status_publicacion==3 || this.det_inmueble.id_status_publicacion==1){
      mensaje='Inactivo'
    }else{
      mensaje='Activo'
    }
     let alert = this.alertCtrl.create({
        title: 'Advertencia',
        subTitle: 'Cambiará el status a '+mensaje+', ¿desea continuar?',
        buttons: 
        [
        { 
          text: 'Si',
          handler: () =>{
            this.cambiar_status();
          }
        },
        {
          text: 'No',
          handler: () =>{
            }
        }
        ]
        });
        alert.present();
  }
  cambiar_status(){
  	let mensaje='Cambiando status de inmueble';
  	this.pr_alert_toast.show_loading(mensaje);
    let data = {"inmueble":this.det_inmueble.id_inmueble};
  	 this.pr_inmueble.cambiar_status_inmueble(data).subscribe(
  	    result => {
  	    	this.pr_alert_toast.dismis_loading();
  	      	let mensaje='Inmueble actualizado correctamente';
  	      	this.pr_alert_toast.mensaje_toast_pie(mensaje);
  					this.navCtrl.pop();
  	    },
  	    err => {console.log(err);
  	    },
  	  );
  }

  editar(){
    this.navCtrl.push('MisPropiedadesEditarPage',{
      'id': this.inmueble.det_inmuebles.id
    });
  }

}
