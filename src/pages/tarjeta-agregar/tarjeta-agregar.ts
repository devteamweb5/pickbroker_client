import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrTarjetasProvider} from '../../providers/pr-tarjetas/pr-tarjetas';

@IonicPage()
@Component({
  selector: 'page-tarjeta-agregar',
  templateUrl: 'tarjeta-agregar.html',
})
export class TarjetaAgregarPage {
	data_tarjeta={
		nombre:'',
		num_tajeta:'',
		anio:'',
		ccv:'',
	}
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_alert_toast:PrAlertToastProvider, public pr_tarjetas:PrTarjetasProvider) {
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad TarjetaAgregarPage');
  }
  guardar_tarjeta(){
  	if(this.data_tarjeta.nombre=='') {
  		let mensaje='Ingrese un nombre';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}else if(this.data_tarjeta.num_tajeta==''){
  		let mensaje='Ingrese un numero de tarjeta';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}else if(this.data_tarjeta.anio==''){
  		let mensaje='Ingrese un Año de vencimiento';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}else if(this.data_tarjeta.ccv==''){
  		let mensaje='Ingrese un CCV';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}else{
	  	let anio_tarjeta=this.data_tarjeta.anio.split('-');
  		let tarjetas_1={
					customer_id:'',
					card_number:this.data_tarjeta.num_tajeta,
					holder_name:this.data_tarjeta.nombre,
					expiration_year:anio_tarjeta['0'].substr(2,2),
					expiration_month:anio_tarjeta['1'],
					cvv2:this.data_tarjeta.ccv
  		}
			let usuario_pick = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
  		for(let value of usuario_pick){
  			tarjetas_1.customer_id=value.token_open_pay
  		}
	  	let tarjetas=JSON.parse(localStorage.getItem('tarjetas_picbroker'));
	  	let tarjetas_agregar:any[]=[];
	  	/*aqui genera el token de la tarjeta desde el Servidor*/
	  	let mensaje='Guardando tarjeta';
	  	this.pr_alert_toast.show_loading(mensaje);
	  	 this.pr_tarjetas.agregar_tarjetas(tarjetas_1).subscribe(
	  	    pr_tarjetas => {
	  	    	this.pr_alert_toast.dismis_loading();
	  	      let resultado=pr_tarjetas;
	  	      if(resultado.status==true){
	  					let resultado_tarjeta=JSON.parse(resultado.data);
	  	      	if(resultado_tarjeta.error_code){
	  	      		let mensaje='Ocurrió un error al cargar la tarjeta';
	  	      		this.pr_alert_toast.mensaje_toast_pie(mensaje);
	  	      	}else{
		  					/* para guardarlo en el Storage*/
						  	if(tarjetas==null ||tarjetas==undefined){
						  		tarjetas_agregar.push({
						  			source_id:resultado_tarjeta.id,
						  			customer_id:usuario_pick.token_open_pay,
										card_number:this.data_tarjeta.num_tajeta,
										holder_name:this.data_tarjeta.nombre,
										expiration_year:anio_tarjeta['1'],
										expiration_month:anio_tarjeta['0'],
										cvv2:this.data_tarjeta.ccv
						  		});
						  	}else{
						  		 for(let value of tarjetas){
						  			tarjetas_agregar.push({
						  			source_id:resultado_tarjeta.id,
						  			customer_id:usuario_pick.token_open_pay,
										card_number:value.card_number,
										holder_name:value.holder_name,
										expiration_year:value.expiration_year,
										expiration_month:value.expiration_month,
										cvv2:value.cvv2
						  			});
						  		 }
						  		 tarjetas_agregar.push({
						  		 	source_id:resultado_tarjeta.id,
						  		 	customer_id:usuario_pick.token_open_pay,
										card_number:this.data_tarjeta.num_tajeta,
										holder_name:this.data_tarjeta.nombre,
										expiration_year:anio_tarjeta['1'],
										expiration_month:anio_tarjeta['0'],
										cvv2:this.data_tarjeta.ccv
						  		});
						  	}
						  	let mensaje='Tarjeta guardada';
						  	this.pr_alert_toast.mensaje_toast_pie(mensaje);
						  	localStorage.setItem('tarjetas_picbroker',JSON.stringify(tarjetas_agregar));
						  	this.navCtrl.pop();
		  				/*******************************/
	  	      	}
	  	      }
	  	    },
	  	    err => {console.log('el error '+err);
	  	    },
	  	  );

	  	
  	}
  }


}
