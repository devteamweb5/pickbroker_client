import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TarjetaAgregarPage } from './tarjeta-agregar';

@NgModule({
  declarations: [
    TarjetaAgregarPage,
  ],
  imports: [
    IonicPageModule.forChild(TarjetaAgregarPage),
  ],
})
export class TarjetaAgregarPageModule {}
