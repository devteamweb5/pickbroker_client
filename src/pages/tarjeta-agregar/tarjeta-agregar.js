var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrTarjetasProvider } from '../../providers/pr-tarjetas/pr-tarjetas';
var TarjetaAgregarPage = /** @class */ (function () {
    function TarjetaAgregarPage(navCtrl, navParams, pr_alert_toast, pr_tarjetas) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_tarjetas = pr_tarjetas;
        this.data_tarjeta = {
            nombre: '',
            num_tajeta: '',
            anio: '',
            ccv: '',
        };
    }
    TarjetaAgregarPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TarjetaAgregarPage');
    };
    TarjetaAgregarPage.prototype.guardar_tarjeta = function () {
        var _this = this;
        if (this.data_tarjeta.nombre == '') {
            var mensaje = 'Ingrese un nombre';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_tarjeta.num_tajeta == '') {
            var mensaje = 'Ingrese un numero de tarjeta';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_tarjeta.anio == '') {
            var mensaje = 'Ingrese un Año de vencimiento';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_tarjeta.ccv == '') {
            var mensaje = 'Ingrese un CCV';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else {
            var anio_tarjeta_1 = this.data_tarjeta.anio.split('-');
            var tarjetas_1 = {
                customer_id: '',
                card_number: this.data_tarjeta.num_tajeta,
                holder_name: this.data_tarjeta.nombre,
                expiration_year: anio_tarjeta_1['0'].substr(2, 2),
                expiration_month: anio_tarjeta_1['1'],
                cvv2: this.data_tarjeta.ccv
            };
            var usuario_pick_2 = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
            for (var _i = 0, usuario_pick_1 = usuario_pick_2; _i < usuario_pick_1.length; _i++) {
                var value = usuario_pick_1[_i];
                tarjetas_1.customer_id = value.token_open_pay;
            }
            var tarjetas_2 = JSON.parse(localStorage.getItem('tarjetas_picbroker'));
            var tarjetas_agregar_1 = [];
            /*aqui genera el token de la tarjeta desde el Servidor*/
            var mensaje = 'Guardando tarjeta';
            this.pr_alert_toast.show_loading(mensaje);
            this.pr_tarjetas.agregar_tarjetas(tarjetas_1).subscribe(function (pr_tarjetas) {
                _this.pr_alert_toast.dismis_loading();
                var resultado = pr_tarjetas;
                if (resultado.status == true) {
                    var resultado_tarjeta = JSON.parse(resultado.data);
                    if (resultado_tarjeta.error_code) {
                        var mensaje_1 = 'Ocurrió un error al cargar la tarjeta';
                        _this.pr_alert_toast.mensaje_toast_pie(mensaje_1);
                    }
                    else {
                        /* para guardarlo en el Storage*/
                        if (tarjetas_2 == null || tarjetas_2 == undefined) {
                            tarjetas_agregar_1.push({
                                source_id: resultado_tarjeta.id,
                                customer_id: usuario_pick_2.token_open_pay,
                                card_number: _this.data_tarjeta.num_tajeta,
                                holder_name: _this.data_tarjeta.nombre,
                                expiration_year: anio_tarjeta_1['1'],
                                expiration_month: anio_tarjeta_1['0'],
                                cvv2: _this.data_tarjeta.ccv
                            });
                        }
                        else {
                            for (var _i = 0, tarjetas_3 = tarjetas_2; _i < tarjetas_3.length; _i++) {
                                var value = tarjetas_3[_i];
                                tarjetas_agregar_1.push({
                                    source_id: resultado_tarjeta.id,
                                    customer_id: usuario_pick_2.token_open_pay,
                                    card_number: value.card_number,
                                    holder_name: value.holder_name,
                                    expiration_year: value.expiration_year,
                                    expiration_month: value.expiration_month,
                                    cvv2: value.cvv2
                                });
                            }
                            tarjetas_agregar_1.push({
                                source_id: resultado_tarjeta.id,
                                customer_id: usuario_pick_2.token_open_pay,
                                card_number: _this.data_tarjeta.num_tajeta,
                                holder_name: _this.data_tarjeta.nombre,
                                expiration_year: anio_tarjeta_1['1'],
                                expiration_month: anio_tarjeta_1['0'],
                                cvv2: _this.data_tarjeta.ccv
                            });
                        }
                        var mensaje_2 = 'Tarjeta guardada';
                        _this.pr_alert_toast.mensaje_toast_pie(mensaje_2);
                        localStorage.setItem('tarjetas_picbroker', JSON.stringify(tarjetas_agregar_1));
                        _this.navCtrl.pop();
                        /*******************************/
                    }
                }
            }, function (err) {
                console.log('el error ' + err);
            });
        }
    };
    TarjetaAgregarPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-tarjeta-agregar',
            templateUrl: 'tarjeta-agregar.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrAlertToastProvider, PrTarjetasProvider])
    ], TarjetaAgregarPage);
    return TarjetaAgregarPage;
}());
export { TarjetaAgregarPage };
//# sourceMappingURL=tarjeta-agregar.js.map