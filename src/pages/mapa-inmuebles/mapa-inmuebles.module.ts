import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MapaInmueblesPage } from './mapa-inmuebles';

@NgModule({
  declarations: [
    MapaInmueblesPage,
  ],
  imports: [
    IonicPageModule.forChild(MapaInmueblesPage),
  ],
})
export class MapaInmueblesPageModule {}
