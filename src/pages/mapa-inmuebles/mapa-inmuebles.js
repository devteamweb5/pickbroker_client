var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
var MapaInmueblesPage = /** @class */ (function () {
    function MapaInmueblesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    MapaInmueblesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MapaInmueblesPage');
        this.map = this.loadMap();
    };
    MapaInmueblesPage.prototype.loadMap = function (location) {
        var _this = this;
        if (location === void 0) { location = new google.maps.LatLng(43.0741904, -89.3809802); }
        var mapOptions = {
            center: location,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };
        var mapEl = document.getElementById('map_canvas');
        var map = new google.maps.Map(mapEl, mapOptions);
        // Wait the MAP_READY before using any methods.
        navigator.geolocation.getCurrentPosition(function (position) {
            var newLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            _this.map.setCenter(newLatLng);
            _this.lat = position.coords.latitude;
            _this.long = position.coords.longitude;
            /*let marker = new google.maps.Marker({
                map: this.map,
            animation: google.maps.Animation.DROP,
                position: newLatLng,
                draggable: false,
                });*/
            /*this.get_barberias();
            this.pr_barber_shop.set_lat(this.lat);
            this.pr_barber_shop.set_long(this.long);*/
        });
        return map;
    };
    MapaInmueblesPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-mapa-inmuebles',
            templateUrl: 'mapa-inmuebles.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], MapaInmueblesPage);
    return MapaInmueblesPage;
}());
export { MapaInmueblesPage };
//# sourceMappingURL=mapa-inmuebles.js.map