import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {GoogleMapsLatLng} from 'ionic-native';
declare var google: any;


@IonicPage()
@Component({
  selector: 'page-mapa-inmuebles',
  templateUrl: 'mapa-inmuebles.html',
})
export class MapaInmueblesPage {
	public map; 
	lat:any;
  long:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MapaInmueblesPage');
    this.map=this.loadMap();
  }
  loadMap(location= new google.maps.LatLng(43.0741904,-89.3809802)){

  let mapOptions={
  		center: location,
  		zoom:16, 
  		mapTypeId: google.maps.MapTypeId.ROADMAP,
  		disableDefaultUI: true
  }
  let mapEl=document.getElementById('map_canvas');
 	let map= new google.maps.Map(mapEl,mapOptions);

  // Wait the MAP_READY before using any methods.
   navigator.geolocation.getCurrentPosition(
      (position) => {
      	let newLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        this.map.setCenter(newLatLng);
        this.lat=position.coords.latitude;
        this.long=position.coords.longitude;
        /*let marker = new google.maps.Marker({
    		map: this.map,
        animation: google.maps.Animation.DROP,
    		position: newLatLng,
    		draggable: false,
  			});*/
        /*this.get_barberias();
        this.pr_barber_shop.set_lat(this.lat);
        this.pr_barber_shop.set_long(this.long);*/
  	}
    );
    return map;
}

}
