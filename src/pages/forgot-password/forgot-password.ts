import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrLoginProvider} from '../../providers/pr-login/pr-login';
@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {
	data_usuario:any={
		login:''
	}
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_alert_toast:PrAlertToastProvider, public pr_login:PrLoginProvider) {
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }
  recuperar_password(){
  	if(this.data_usuario.login=='') {
  		let mensaje='Ingrese un email';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}else{
  		let mensaje='Cargando';
  		this.pr_alert_toast.show_loading(mensaje);
		  this.pr_login.recuperar_password(this.data_usuario).subscribe(
		    pr_login => {
		    	this.pr_alert_toast.dismis_loading();
		      let resultado=pr_login;
		      if(resultado.mensaje=='usuario_actualizado'){
					let mensaje='Revise su bandeja de email';
					this.pr_alert_toast.mensaje_toast_pie(mensaje);
					this.navCtrl.pop();
		      }else{
		      	let mensaje='Email no registrado en nuestra base de datos';
						this.pr_alert_toast.mensaje_toast_pie(mensaje);
		      }
		    },
		    err => {console.log('el error '+err);
		    },
		  );
  	}
  }

}
