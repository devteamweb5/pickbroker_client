var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrLoginProvider } from '../../providers/pr-login/pr-login';
var ForgotPasswordPage = /** @class */ (function () {
    function ForgotPasswordPage(navCtrl, navParams, pr_alert_toast, pr_login) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_login = pr_login;
        this.data_usuario = {
            login: ''
        };
    }
    ForgotPasswordPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ForgotPasswordPage');
    };
    ForgotPasswordPage.prototype.recuperar_password = function () {
        var _this = this;
        if (this.data_usuario.login == '') {
            var mensaje = 'Ingrese un email';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else {
            var mensaje = 'Cargando';
            this.pr_alert_toast.show_loading(mensaje);
            this.pr_login.recuperar_password(this.data_usuario).subscribe(function (pr_login) {
                _this.pr_alert_toast.dismis_loading();
                var resultado = pr_login;
                if (resultado.mensaje == 'usuario_actualizado') {
                    var mensaje_1 = 'Revise su bandeja de email';
                    _this.pr_alert_toast.mensaje_toast_pie(mensaje_1);
                    _this.navCtrl.pop();
                }
                else {
                    var mensaje_2 = 'Email no registrado en nuestra base de datos';
                    _this.pr_alert_toast.mensaje_toast_pie(mensaje_2);
                }
            }, function (err) {
                console.log('el error ' + err);
            });
        }
    };
    ForgotPasswordPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-forgot-password',
            templateUrl: 'forgot-password.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrAlertToastProvider, PrLoginProvider])
    ], ForgotPasswordPage);
    return ForgotPasswordPage;
}());
export { ForgotPasswordPage };
//# sourceMappingURL=forgot-password.js.map