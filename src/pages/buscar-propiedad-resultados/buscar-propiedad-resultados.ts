import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrInmuebleProvider} from '../../providers/pr-inmueble/pr-inmueble';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';


@IonicPage()
@Component({
  selector: 'page-buscar-propiedad-resultados',
  templateUrl: 'buscar-propiedad-resultados.html',
})
export class BuscarPropiedadResultadosPage {
	inmuebles_glob:any;
	ruta_imagenes:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_inmueble:PrInmuebleProvider, public pr_alert_toast:PrAlertToastProvider, public pr_rutas:PrRutasProvider) {
  	this.ruta_imagenes=this.pr_rutas.get_ruta_imagenes();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuscarPropiedadResultadosPage');
    this.get_inmuebles_resultados();
  }
  atras(){
    this.navCtrl.pop();
  }
  get_inmuebles_resultados(){
  	let inmuebles=JSON.parse(localStorage.getItem('resultados'));
  	this.inmuebles_glob=inmuebles;
  	console.log(this.inmuebles_glob);
  }
  get_det_inmueble(item){
    let mensaje='Cargando';
    this.pr_alert_toast.show_loading(mensaje);
    let data_det_inmuebles={
      det_inmuebles:item.inmueble,
      imagenes:item.imagenes,
      caracteristicas:item.inmueble.caracteristicas ? item.inmueble.caracteristicas.split(',') : [] ,
      servicios:item.inmueble.servicios ? item.inmueble.servicios.split(',') : []
    }
    localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
    this.navCtrl.push('DetInmueblePage');
    /*
     this.pr_inmueble.get_imagenes_caracteristicas_servicios_inmueble(data_inmueble).subscribe(
        pr_inmueble => {
          let resultado=pr_inmueble;
          if(resultado.status==true){
            let data_det_inmuebles={
              det_inmuebles:data,
              imagenes:resultado.imagenes,
              caracteristicas:resultado.caracteristicas,
              servicios:resultado.servicios
            }
           localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
           this.navCtrl.push('PropiedadesDetallePage');
          }
        },
        err => {console.log('el error '+err);
        },
      );
    */
  }

  filtro(){
    this.navCtrl.push('BuscarPropiedadPage')
  }
  ver_mapa(){
    this.navCtrl.push('MapaBuscarPage');
  }

}
