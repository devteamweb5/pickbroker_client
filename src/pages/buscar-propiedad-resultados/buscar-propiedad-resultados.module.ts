import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuscarPropiedadResultadosPage } from './buscar-propiedad-resultados';

@NgModule({
  declarations: [
    BuscarPropiedadResultadosPage,
  ],
  imports: [
    IonicPageModule.forChild(BuscarPropiedadResultadosPage),
  ],
})
export class BuscarPropiedadResultadosPageModule {}
