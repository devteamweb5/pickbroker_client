var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrInmuebleProvider } from '../../providers/pr-inmueble/pr-inmueble';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrRutasProvider } from '../../providers/pr-rutas/pr-rutas';
var BuscarPropiedadResultadosPage = /** @class */ (function () {
    function BuscarPropiedadResultadosPage(navCtrl, navParams, pr_inmueble, pr_alert_toast, pr_rutas) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_inmueble = pr_inmueble;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_rutas = pr_rutas;
        this.ruta_imagenes = this.pr_rutas.get_ruta_imagenes();
    }
    BuscarPropiedadResultadosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BuscarPropiedadResultadosPage');
        this.get_inmuebles_resultados();
    };
    BuscarPropiedadResultadosPage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    BuscarPropiedadResultadosPage.prototype.get_inmuebles_resultados = function () {
        var inmuebles = JSON.parse(localStorage.getItem('resultados'));
        this.inmuebles_glob = inmuebles;
        console.log(this.inmuebles_glob);
    };
    BuscarPropiedadResultadosPage.prototype.get_det_inmueble = function (item) {
        var mensaje = 'Cargando';
        this.pr_alert_toast.show_loading(mensaje);
        var data_det_inmuebles = {
            det_inmuebles: item,
            imagenes: [],
            caracteristicas: [],
            servicios: []
        };
        localStorage.setItem('det_inmueble', JSON.stringify(data_det_inmuebles));
        this.navCtrl.push('DetInmueblePage');
        /*
         this.pr_inmueble.get_imagenes_caracteristicas_servicios_inmueble(data_inmueble).subscribe(
            pr_inmueble => {
              let resultado=pr_inmueble;
              if(resultado.status==true){
                let data_det_inmuebles={
                  det_inmuebles:data,
                  imagenes:resultado.imagenes,
                  caracteristicas:resultado.caracteristicas,
                  servicios:resultado.servicios
                }
               localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
               this.navCtrl.push('PropiedadesDetallePage');
              }
            },
            err => {console.log('el error '+err);
            },
          );
        */
    };
    BuscarPropiedadResultadosPage.prototype.filtro = function () {
        this.navCtrl.push('BuscarPropiedadPage');
    };
    BuscarPropiedadResultadosPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-buscar-propiedad-resultados',
            templateUrl: 'buscar-propiedad-resultados.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrInmuebleProvider, PrAlertToastProvider, PrRutasProvider])
    ], BuscarPropiedadResultadosPage);
    return BuscarPropiedadResultadosPage;
}());
export { BuscarPropiedadResultadosPage };
//# sourceMappingURL=buscar-propiedad-resultados.js.map