import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeMapaPage } from './home-mapa';

@NgModule({
  declarations: [
    HomeMapaPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeMapaPage),
  ],
})
export class HomeMapaPageModule {}
