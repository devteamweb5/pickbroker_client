import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrInmuebleProvider} from '../../providers/pr-inmueble/pr-inmueble';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-home-mapa',
  templateUrl: 'home-mapa.html',
})
export class HomeMapaPage {
	inmuebles:any;
  map:any;
  item_sel:any;
  nombre_propiedad:any;
  direccion:any;
  lat_propiedad:any;
  lng_propiedad:any;
  muestra:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public pr_alert_toast:PrAlertToastProvider,
    public pr_inmueble:PrInmuebleProvider,
    public events:Events){
    this.muestra='none';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeMapaPage');
  }
  ionViewWillEnter(){
    setTimeout(()=>{
      this.map=this.loadMap();
    },1000);
    let item=1;
    this.events.publish('user:created', item);
  }
  atras(){
    this.navCtrl.pop();
  }

  loadMap(location= new google.maps.LatLng(31.69036380000001,-106.42454780000003)){
    let mapOptions={
      center: location,
      zoom:16, 
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    }
    let mapEl=document.getElementById('map_canvas');
    let map= new google.maps.Map(mapEl,mapOptions);
    // Wait the MAP_READY before using any methods.
    navigator.geolocation.getCurrentPosition(
      (position) => {
        let newLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        this.map.setCenter(newLatLng);
        this.get_propiedades_ruta();
      }
      );
      return map;
    }
    get_propiedades_ruta(){
      let data_u=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));

      let data={
        id_usuario:'',
      }
      for(let value of data_u) {
        data.id_usuario=value.id;
      }
      let mensaje='Cargando';
      this.pr_alert_toast.show_loading(mensaje);
      this.pr_inmueble.get_inmuebles_todos().subscribe(
        pr_barber_shop => {
          this.pr_alert_toast.dismis_loading();
          let resultado=pr_barber_shop;
          if(resultado){
            let data=resultado;
            console.log(data);
            let latitude;
            let longitude;
            let lat_long;
            for(let value of data) {
              lat_long=value.inmueble.latLong.split(',');
              latitude=lat_long['0'];
              longitude=lat_long['1'];
              let newLatLng = new google.maps.LatLng(latitude, longitude);
              this.Position_Marker(newLatLng,value);
            }
          }
        },
        err => {console.log(err);
        },
        );

    }
    Position_Marker(item,item_2){
      let lat_lng=item;
      let datos=item_2;
      console.log('entra en position_marker');
      var icono ="assets/imgs/pin.png";
      let marker = new google.maps.Marker({
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: lat_lng,
        draggable: false,
        icon:icono
      });
      google.maps.event.addListener(marker, 'click', () => {
        this.mostrar_propiedad_ruta(datos);   
      });
    }
    mostrar_propiedad_ruta(item){
      let data=item;
      this.item_sel=data;
      console.log(data);
      this.nombre_propiedad=data.inmueble.nombre;
      this.direccion=data.inmueble.direccion;
      let lat_long=data.inmueble.latLong.split(',');
      this.lat_propiedad=lat_long['0'];
      this.lng_propiedad=lat_long['1'];
      this.muestra='block';
      console.log(this.muestra);
      let mapa=document.getElementById('mapa_2');
      localStorage.setItem('propiedad_seleccionada',JSON.stringify(this.item_sel));
      mapa.classList.add('bounceIn');
    }
    ver_lista(){
      this.navCtrl.setRoot('HomePage');
    }
    get_det_inmueble(item){
      console.log(item);
      let data= JSON.parse(localStorage.getItem('propiedad_seleccionada'));
      console.log(data);
      let mensaje='Cargando';
      let data_det_inmuebles={
        det_inmuebles:data.inmueble,
        imagenes:data.imagenes,
        caracteristicas:data.inmueble.caracteristicas ? data.inmueble.caracteristicas.split(',') : [] ,
      servicios:data.inmueble.servicios ? data.inmueble.servicios.split(',') : []
      }
      localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
      this.navCtrl.setRoot('DetInmueblePage',{
        'prev': 'HomeMapaPage'
      });
    /*
    this.pr_inmueble.get_imagenes_caracteristicas_servicios_inmueble(data_inmueble).subscribe(
      pr_inmueble => {
        this.pr_alert_toast.dismis_loading();
        let resultado=pr_inmueble;
        if(resultado.status==true){
          let data_det_inmuebles={
            det_inmuebles:data,
            imagenes:resultado.imagenes,
            caracteristicas:resultado.caracteristicas,
            servicios:resultado.servicios
          }
          localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
          this.navCtrl.push('DetInmueblePage');
        }
      },
      err => {console.log('el error '+err);
    },
    );
    */
  }

}
