var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrInmuebleProvider } from '../../providers/pr-inmueble/pr-inmueble';
var HomeMapaPage = /** @class */ (function () {
    function HomeMapaPage(navCtrl, navParams, pr_alert_toast, pr_inmueble, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_inmueble = pr_inmueble;
        this.events = events;
        this.muestra = 'none';
    }
    HomeMapaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HomeMapaPage');
    };
    HomeMapaPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        setTimeout(function () {
            _this.map = _this.loadMap();
        }, 1000);
        var item = 1;
        this.events.publish('user:created', item);
    };
    HomeMapaPage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    HomeMapaPage.prototype.loadMap = function (location) {
        if (location === void 0) { location = new google.maps.LatLng(31.69036380000001, -106.42454780000003); }
        var mapOptions = {
            center: location,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };
        var mapEl = document.getElementById('map_canvas');
        var map = new google.maps.Map(mapEl, mapOptions);
        // Wait the MAP_READY before using any methods.
        /*
        navigator.geolocation.getCurrentPosition(
          (position) => {
            let newLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            this.map.setCenter(newLatLng);
            this.get_propiedades_ruta();
          }
          );
          */
        this.get_propiedades_ruta();
        return map;
    };
    HomeMapaPage.prototype.get_propiedades_ruta = function () {
        var _this = this;
        var data_u = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        var data = {
            id_usuario: '',
        };
        for (var _i = 0, data_u_1 = data_u; _i < data_u_1.length; _i++) {
            var value = data_u_1[_i];
            data.id_usuario = value.id;
        }
        var mensaje = 'Cargando';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_inmueble.get_inmuebles_todos().subscribe(function (pr_barber_shop) {
            _this.pr_alert_toast.dismis_loading();
            var resultado = pr_barber_shop;
            if (resultado) {
                var data_2 = resultado;
                console.log(data_2);
                var latitude = void 0;
                var longitude = void 0;
                var lat_long = void 0;
                for (var _i = 0, data_1 = data_2; _i < data_1.length; _i++) {
                    var value = data_1[_i];
                    lat_long = value.inmueble.latLong.split(',');
                    latitude = lat_long['0'];
                    longitude = lat_long['1'];
                    var newLatLng = new google.maps.LatLng(latitude, longitude);
                    _this.Position_Marker(newLatLng, value);
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    HomeMapaPage.prototype.Position_Marker = function (item, item_2) {
        var _this = this;
        var lat_lng = item;
        var datos = item_2;
        console.log('entra en position_marker');
        var icono = "assets/imgs/pin.png";
        var marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: lat_lng,
            draggable: false,
            icon: icono
        });
        google.maps.event.addListener(marker, 'click', function () {
            _this.mostrar_propiedad_ruta(datos);
        });
    };
    HomeMapaPage.prototype.mostrar_propiedad_ruta = function (item) {
        var data = item;
        this.item_sel = data;
        console.log(data);
        this.nombre_propiedad = data.inmueble.nombre;
        this.direccion = data.inmueble.direccion;
        var lat_long = data.inmueble.latLong.split(',');
        this.lat_propiedad = lat_long['0'];
        this.lng_propiedad = lat_long['1'];
        this.muestra = 'block';
        console.log(this.muestra);
        var mapa = document.getElementById('mapa_2');
        localStorage.setItem('propiedad_seleccionada', JSON.stringify(this.item_sel));
        mapa.classList.add('bounceIn');
    };
    HomeMapaPage.prototype.ver_lista = function () {
        this.navCtrl.setRoot('HomePage');
    };
    HomeMapaPage.prototype.get_det_inmueble = function (item) {
        console.log(item);
        var data = JSON.parse(localStorage.getItem('propiedad_seleccionada'));
        console.log(data);
        var mensaje = 'Cargando';
        var data_det_inmuebles = {
            det_inmuebles: data.inmueble,
            imagenes: data.imagenes,
            caracteristicas: data.caracteristicas ? data.caracteristicas.split(',') : [],
            servicios: data.servicios ? data.servicios.split(',') : []
        };
        localStorage.setItem('det_inmueble', JSON.stringify(data_det_inmuebles));
        this.navCtrl.push('DetInmueblePage');
        /*
        this.pr_inmueble.get_imagenes_caracteristicas_servicios_inmueble(data_inmueble).subscribe(
          pr_inmueble => {
            this.pr_alert_toast.dismis_loading();
            let resultado=pr_inmueble;
            if(resultado.status==true){
              let data_det_inmuebles={
                det_inmuebles:data,
                imagenes:resultado.imagenes,
                caracteristicas:resultado.caracteristicas,
                servicios:resultado.servicios
              }
              localStorage.setItem('det_inmueble',JSON.stringify(data_det_inmuebles));
              this.navCtrl.push('DetInmueblePage');
            }
          },
          err => {console.log('el error '+err);
        },
        );
        */
    };
    HomeMapaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-home-mapa',
            templateUrl: 'home-mapa.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            PrAlertToastProvider,
            PrInmuebleProvider,
            Events])
    ], HomeMapaPage);
    return HomeMapaPage;
}());
export { HomeMapaPage };
//# sourceMappingURL=home-mapa.js.map