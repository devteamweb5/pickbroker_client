import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FiltrarInmueblesPage } from './filtrar-inmuebles';

@NgModule({
  declarations: [
    FiltrarInmueblesPage,
  ],
  imports: [
    IonicPageModule.forChild(FiltrarInmueblesPage),
  ],
})
export class FiltrarInmueblesPageModule {}
