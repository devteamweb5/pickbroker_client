var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { PrContactoEmergenciaProvider } from '../../providers/pr-contacto-emergencia/pr-contacto-emergencia';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrLlamadasProvider } from '../../providers/pr-llamadas/pr-llamadas';
import { PrCitasProvider } from '../../providers/pr-citas/pr-citas';
var CitaDetallePage = /** @class */ (function () {
    function CitaDetallePage(navCtrl, navParams, callNumber, launchNavigator, pr_contacto_emergencia, pr_alert_toast, pr_llamadas, pr_citas) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.callNumber = callNumber;
        this.launchNavigator = launchNavigator;
        this.pr_contacto_emergencia = pr_contacto_emergencia;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_llamadas = pr_llamadas;
        this.pr_citas = pr_citas;
        this.det_cita = {
            dia_cita_programada: '',
            direccion_cita_programada: '',
            hora_cita_programada: '',
            id_cita_programada: '',
            id_inmueble_propiedad_deseada: '',
            id_usuario: '',
            lat_long_cita_programada: '',
            nombre_usuario: '',
            telf: '',
        };
    }
    CitaDetallePage.prototype.ionViewDidLoad = function () {
        this.user = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        console.log('ionViewDidLoad CitaDetallePage');
        this.get_cita_detalle();
        this.ubicacion_usuario();
    };
    CitaDetallePage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    CitaDetallePage.prototype.get_cita_detalle = function () {
        this.cita = JSON.parse(localStorage.getItem('data_cita'));
        console.log(this.cita);
        /*
            this.det_cita.dia_cita_programada=data.dia_cita_programada;
            this.det_cita.direccion_cita_programada=data.direccion_cita_programada;
            this.det_cita.hora_cita_programada=data.hora_cita_programada;
            this.det_cita.id_cita_programada=data.id_cita_programada;
            this.det_cita.id_inmueble_propiedad_deseada=data.id_inmueble_propiedad_deseada;
        this.det_cita.id_usuario=data.id_usuario;
            this.det_cita.id_cliente=data.id_cliente;
            this.det_cita.lat_long_cita_programada=data.lat_long_cita_programada;
            this.det_cita.nombre_usuario=data.nombre_usuario;
            this.det_cita.telf=data.telf_usuario;
        */
    };
    CitaDetallePage.prototype.llamar = function () {
        this.callNumber.callNumber(this.cita.socio.phone, true)
            .then(function (res) {
            console.log('Launched dialer!', res);
        })
            .catch(function (err) { return console.log('Error launching dialer', err); });
        var data_u = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        var llamada = {
            user: data_u.id,
            called: this.cita.socio.id,
        };
        console.log(llamada);
        this.pr_llamadas.guardar_llamadas(llamada).subscribe(function (result) {
            console.log('llamada realizada');
        }, function (err) {
            console.log(err);
        });
    };
    CitaDetallePage.prototype.trazar_ruta = function () {
        var inicio = this.lat + ',' + this.long;
        var fin = this.cita.latLong;
        console.log(inicio);
        console.log(fin);
        var options = {
            start: inicio
        };
        this.launchNavigator.navigate(fin, options)
            .then(function (success) { return console.log('Launched navigator'); }, function (error) { return console.log('Error launching navigator' + error); });
    };
    CitaDetallePage.prototype.ubicacion_usuario = function () {
        var _this = this;
        var options = { timeout: 120000, enableHighAccuracy: true };
        navigator.geolocation.getCurrentPosition(function (position) {
            _this.lat = position.coords.latitude;
            _this.long = position.coords.longitude;
            console.log(_this.lat);
            console.log(_this.long);
        }, function (error) {
            console.log(error);
        }, options);
    };
    CitaDetallePage.prototype.enviar_mensaje = function () {
        var _this = this;
        var datos = { envia: this.user.id, recibe: this.cita.socio.id };
        this.pr_citas.get_chat(datos).subscribe(function (result) {
            var contacto;
            if (result.user1.id == _this.user.id) {
                contacto = result.user1.id;
            }
            else {
                contacto = result.user2.id;
            }
            _this.navCtrl.push('ChatClientePage', {
                chat: result.id,
                contacto: contacto
            });
        });
        // this.pr_citas.get_mensajes_pendientes(item.chat).subscribe(result=>{
        //    console.log(result);
        //    this.navCtrl.push('ChatClientePage',{
        //      chat:item.chat,
        //      contacto:item.contacto.id
        //    })
        //  });
    };
    CitaDetallePage.prototype.enviar_email_contacto_emergencia = function () {
        var _this = this;
        var data_u = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        var usuario = data_u.usuario;
        var data = {
            id_usuario: '',
            nombre: '',
        };
        for (var _i = 0, data_u_1 = data_u; _i < data_u_1.length; _i++) {
            var value = data_u_1[_i];
            data.id_usuario = value.id;
            data.nombre = value.nombre;
        }
        this.pr_contacto_emergencia.enviar_email_contacto_emergencia(data).subscribe(function (pr_contacto_emergencia) {
            var resultado = pr_contacto_emergencia;
            if (resultado.status == true) {
                var mensaje = 'Email enviado';
                _this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }
            else {
                var mensaje = 'No se envió el email';
                _this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    CitaDetallePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-cita-detalle',
            templateUrl: 'cita-detalle.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            CallNumber,
            LaunchNavigator,
            PrContactoEmergenciaProvider,
            PrAlertToastProvider,
            PrLlamadasProvider,
            PrCitasProvider])
    ], CitaDetallePage);
    return CitaDetallePage;
}());
export { CitaDetallePage };
//# sourceMappingURL=cita-detalle.js.map