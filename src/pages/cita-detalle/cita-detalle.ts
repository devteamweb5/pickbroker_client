import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import {PrContactoEmergenciaProvider} from '../../providers/pr-contacto-emergencia/pr-contacto-emergencia';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrLlamadasProvider} from '../../providers/pr-llamadas/pr-llamadas';
import {PrCitasProvider} from '../../providers/pr-citas/pr-citas';
import { PrRutasProvider} from '../../providers/pr-rutas/pr-rutas'
import { Http } from '@angular/http';


@IonicPage()
@Component({
  selector: 'page-cita-detalle',
  templateUrl: 'cita-detalle.html',
})
export class CitaDetallePage {
  cita;
	det_cita:any={
		dia_cita_programada:'',
		direccion_cita_programada:'',
		hora_cita_programada:'',
		id_cita_programada:'',
		id_inmueble_propiedad_deseada:'',
		id_usuario:'',
		lat_long_cita_programada:'',
		nombre_usuario:'',
		telf:'',
	};
	lat:any;
	long:any;
  user;
  constructor(
    public navCtrl: NavController,
   public navParams: NavParams,
   public callNumber:CallNumber,
   private launchNavigator :LaunchNavigator,
  public pr_contacto_emergencia:PrContactoEmergenciaProvider,
   public pr_alert_toast:PrAlertToastProvider,
    public pr_llamadas:PrLlamadasProvider,
    public pr_citas:PrCitasProvider,
    public pr_rutas:PrRutasProvider,
    public http:Http,
    ) {
  }

  ionViewDidLoad() {
    this.user =JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    console.log('ionViewDidLoad CitaDetallePage');
    this.get_cita_detalle();
    this.ubicacion_usuario();
  }
  atras(){
  	this.navCtrl.pop();
  }

  cancelarCita(){
    this.pr_alert_toast.show_loading('Cancelando cita');
    this.http.get(this.pr_rutas.route+`citas/${this.cita.id}/cancelar`).subscribe(result=>{
      console.log(result);
      this.pr_alert_toast.dismis_loading();
      this.navCtrl.pop();
    });
  }
  get_cita_detalle(){
		this.cita=JSON.parse(localStorage.getItem('data_cita'));
    console.log(this.cita);
    /*
		this.det_cita.dia_cita_programada=data.dia_cita_programada;
		this.det_cita.direccion_cita_programada=data.direccion_cita_programada;
		this.det_cita.hora_cita_programada=data.hora_cita_programada;
		this.det_cita.id_cita_programada=data.id_cita_programada;
		this.det_cita.id_inmueble_propiedad_deseada=data.id_inmueble_propiedad_deseada;
    this.det_cita.id_usuario=data.id_usuario;
		this.det_cita.id_cliente=data.id_cliente;
		this.det_cita.lat_long_cita_programada=data.lat_long_cita_programada;
		this.det_cita.nombre_usuario=data.nombre_usuario;
		this.det_cita.telf=data.telf_usuario;
    */
  }
  llamar(){
  	this.callNumber.callNumber(this.cita.socio.phone, true)
  	.then(res =>{
  		console.log('Launched dialer!', res)
  	})
  	.catch(err => console.log('Error launching dialer', err));
    let data_u=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
      let llamada={
      user:data_u.id,
      called:this.cita.socio.id,
    }
    console.log(llamada);
     this.pr_llamadas.guardar_llamadas(llamada).subscribe(
        result => {
          console.log('llamada realizada')
        },
        err => {console.log(err);
        },
      );
  }
  trazar_ruta(){
    let inicio=this.lat+','+this.long;
    let fin= this.cita.latLong;
    console.log(inicio);
    console.log(fin);
    let options: LaunchNavigatorOptions={
      start: inicio
    };
    this.launchNavigator.navigate(fin, options)
    .then(
    success => console.log('Launched navigator'),
    error => console.log('Error launching navigator'+ error)
    );
  }
  ubicacion_usuario(){
    let options = {timeout: 120000, enableHighAccuracy: true};
    navigator.geolocation.getCurrentPosition(
      (position) => {
        this.lat=position.coords.latitude;
        this.long=position.coords.longitude;
       console.log(this.lat);
       console.log(this.long);
      },
      (error)=>{
        console.log(error);
      },
      options
    );
  }
  enviar_mensaje(){
    let datos  = {envia:this.user.id ,recibe:this.cita.socio.id}
    this.pr_citas.get_chat(datos).subscribe(result=>{
        let contacto;
      if (result.user1.id == this.user.id) {
        contacto = result.user1.id;
      }else{
        contacto = result.user2.id;
      }
      this.navCtrl.push('ChatClientePage',{
        chat:result.id,
        contacto:contacto
      })
    })
  	// this.pr_citas.get_mensajes_pendientes(item.chat).subscribe(result=>{
   //    console.log(result);
   //    this.navCtrl.push('ChatClientePage',{
   //      chat:item.chat,
   //      contacto:item.contacto.id
   //    })
   //  });
  }


  enviar_email_contacto_emergencia(){
    this.pr_alert_toast.mensaje_toast_pie('Mensaje enviado');
    // let data_u=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    // let usuario=data_u.usuario;
    //  let data={
    //    id_usuario:'',
    //    nombre:'',
    //  }
    //  for(let value of data_u){
    //   data.id_usuario=value.id;
    //   data.nombre=value.nombre;
    // }
    //  this.pr_contacto_emergencia.enviar_email_contacto_emergencia(data).subscribe(
    //     pr_contacto_emergencia => {
    //       let resultado=pr_contacto_emergencia;
    //       if(resultado.status==true){
    //         let mensaje='Email enviado';
    //         this.pr_alert_toast.mensaje_toast_pie(mensaje);
    //       }else{
    //         let mensaje='No se envió el email';
    //         this.pr_alert_toast.mensaje_toast_pie(mensaje);
    //       }
    //     },
    //     err => {console.log('el error '+err);
    //     },
    //   );
  }
}
