import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MembresiaAdquiridaPage } from './membresia-adquirida';

@NgModule({
  declarations: [
    MembresiaAdquiridaPage,
  ],
  imports: [
    IonicPageModule.forChild(MembresiaAdquiridaPage),
  ],
})
export class MembresiaAdquiridaPageModule {}
