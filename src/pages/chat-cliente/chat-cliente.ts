import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrCitasProvider} from '../../providers/pr-citas/pr-citas';
import * as firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-chat-cliente',
  templateUrl: 'chat-cliente.html',
})
export class ChatClientePage {
	mensajes:any[]=[];
	id_usuario:any;
	id_usuario_agente:any;
	nombre_usuario:any;
	mensaje;
  user;
  chatRefresh;
  id_cita:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public pr_alert_toast:PrAlertToastProvider,
    public pr_citas:PrCitasProvider
   ) {
  }

  ionViewWillEnter(){
    console.log('ionViewDidLoad ChatClientePage');
    this.obtenerMensajes();
    this.user=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    console.log(this.user);
    this.chatRefresh = setInterval(int=>{
      this.obtenerMensajes();
    },1000)
   // this.get_mensajes();
   // this.actualizar_mensaje_pendiente();
  }

  atras(){
  	this.navCtrl.pop();
  }

  obtenerMensajes(){
    this.pr_citas.get_mensajes_pendientes(this.navParams.get('chat')).subscribe(result=>{
      this.mensajes = result;
    }) 
  }

  enviar_mensaje(){
    let data ={
      envia:this.user.id,
      recibe:this.navParams.get('contacto'),
      mensaje : this.mensaje}
    this.pr_citas.enviar_mensaje_socio(data).subscribe(result=>{
      this.obtenerMensajes();
    });
    this.mensaje = '';
  }

  ionViewDidLeave(){
    clearInterval(this.chatRefresh);
  }

  // /*filtro los mensajes directos*/
  // get_mensajes(){
  // this.mensajes=[];
  // let data_u=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
  //  for(let value of data_u){
  //   this.nombre_usuario=value.nombre;
  // 	this.id_usuario=value.id;
  //  }
  // console.log(this.nombre_usuario);
  // let data_cita=JSON.parse(localStorage.getItem('datos_cita_mensaje'));
  // console.log(data_cita);
  // this.id_cita=data_cita.id_cita;

  // this.id_usuario_agente=data_cita.id_usuario_agente;
  // console.log(this.id_usuario_agente);
  //  var starCountRef = firebase.database().ref('mensaje_chat');
  //  let query=starCountRef
  //  /* orderByChild la columna que necesito trabajar*/
  //  /* equalTo el valor que le está enviando para que lo filtre*/
  //  .orderByChild('id_cita')
  //  .equalTo(this.id_cita);
  //   query.on('value', (snap)=>{
  //     console.log(snap);
  //     this.mensajes=[];
  //      var data=snap.val();
  //      for(var key in data){
  //       this.mensajes.push(data[key]);
  //      }
  //      console.log(this.mensajes);
  //   });
  // }
  // /*****************************/
  // enviar_mensaje(){
  // 	if(this.data_mensaje==''){
  // 		let mensaje='Ingrese un mensaje';
  // 		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  // 	}else{
  // 		let data_cita=JSON.parse(localStorage.getItem('datos_cita_mensaje'));
  //     let id_cita=data_cita.id_cita;
  //     let data_envia_mensaje={
  //       id_cita:id_cita,
  //       id_usuario_envia:this.id_usuario,
  //       id_usuario_recibe:this.id_usuario_agente,
  //       nombre:this.nombre_usuario,
  //       mensaje:this.data_mensaje
  //     };
	 //    var messagesRef= firebase.database().ref().child("mensaje_chat");
	 //    messagesRef.push({
	 //      id_cita:id_cita,
  //       id_usuario_envia:this.id_usuario,
  //       id_usuario_recibe:this.id_usuario_agente,
  //       nombre:this.nombre_usuario,
  //       mensaje:this.data_mensaje
	 //    });
	 //    this.data_mensaje='';
  //      this.pr_citas.enviar_mensaje(data_envia_mensaje).subscribe(
  //         pr_citas => {
  //           let resultado=pr_citas;
  //           if(resultado.status==true){
  //               let mensaje=resultado.mensaje;
  //               console.log(mensaje);
  //           }
  //         },
  //         err => {console.log('el error '+err);
  //         },
  //       );
  // 	};
  // }
  // actualizar_mensaje_pendiente(){
  //   let data_u=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
  //   let data_cita=JSON.parse(localStorage.getItem('datos_cita_mensaje'));
  //   console.log(data_u);
  //    let usuario;
  //    for(let value of data_u){
  //     this.nombre_usuario=value.nombre;
  //     this.id_usuario=value.id;
  //    }
  //    let datos_envio={
  //      id_usuario_recibe:this.id_usuario,
  //      id_cita:data_cita.id_cita,
  //    }
  //     this.pr_citas.actualizar_mensaje_pendiente(datos_envio).subscribe(
  //     pr_citas=>{
  //        let resultado=pr_citas;
  //        if(resultado.status==true){
  //          console.log('actualizó las citas');
  //        }
  //      },
  //      err => {console.log('el error '+err);
  //      },
  //     );
  // }

}
