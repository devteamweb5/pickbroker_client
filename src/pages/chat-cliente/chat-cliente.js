var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrCitasProvider } from '../../providers/pr-citas/pr-citas';
var ChatClientePage = /** @class */ (function () {
    function ChatClientePage(navCtrl, navParams, pr_alert_toast, pr_citas) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_citas = pr_citas;
        this.mensajes = [];
    }
    ChatClientePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        console.log('ionViewDidLoad ChatClientePage');
        this.obtenerMensajes();
        this.user = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        console.log(this.user);
        this.chatRefresh = setInterval(function (int) {
            _this.obtenerMensajes();
        }, 5000);
        // this.get_mensajes();
        // this.actualizar_mensaje_pendiente();
    };
    ChatClientePage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    ChatClientePage.prototype.obtenerMensajes = function () {
        var _this = this;
        this.pr_citas.get_mensajes_pendientes(this.navParams.get('chat')).subscribe(function (result) {
            _this.mensajes = result;
            console.log(result);
        });
    };
    ChatClientePage.prototype.enviar_mensaje = function () {
        var _this = this;
        var data = {
            envia: this.user.id,
            recibe: this.navParams.get('contacto'),
            mensaje: this.mensaje
        };
        this.pr_citas.enviar_mensaje_socio(data).subscribe(function (result) {
            _this.obtenerMensajes();
        });
        this.mensaje = '';
    };
    ChatClientePage.prototype.ionViewDidLeave = function () {
        clearInterval(this.chatRefresh);
    };
    ChatClientePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-chat-cliente',
            templateUrl: 'chat-cliente.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            PrAlertToastProvider,
            PrCitasProvider])
    ], ChatClientePage);
    return ChatClientePage;
}());
export { ChatClientePage };
//# sourceMappingURL=chat-cliente.js.map