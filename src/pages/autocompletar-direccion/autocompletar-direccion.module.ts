import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AutocompletarDireccionPage } from './autocompletar-direccion';

@NgModule({
  declarations: [
    AutocompletarDireccionPage,
  ],
  imports: [
    IonicPageModule.forChild(AutocompletarDireccionPage),
  ],
})
export class AutocompletarDireccionPageModule {}
