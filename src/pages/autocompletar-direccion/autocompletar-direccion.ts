import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
declare var google:any;


@IonicPage()
@Component({
  selector: 'page-autocompletar-direccion',
  templateUrl: 'autocompletar-direccion.html',
})
export class AutocompletarDireccionPage {
	 autocompleteItems;
	 autocomplete;
	 backPressed:any=false;
	 loader:any;
	 service = new google.maps.places.AutocompleteService();
   ubicaciones_frecuentes_todas:any[]=[];
  constructor(public navCtrl: NavController,private platform: Platform, public navParams: NavParams,public viewCtrl: ViewController, private zone: NgZone, ) {
  	this.autocompleteItems = [];
  	this.autocomplete = {
		query: ''
		}
    platform.registerBackButtonAction(() =>{
     if (this.navCtrl.canGoBack()) {
      this.dismiss();
      return;
    	}
     if(!this.backPressed) {
      this.backPressed = true
      this.dismiss();
      setTimeout(() => this.backPressed = false, 2000)
      return;
     }else{
      this.platform.exitApp();
     }
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AutocompletarDireccionPage');
  }
   dismiss(){
    this.viewCtrl.dismiss();
   }
   chooseItem(item: any) {
    this.viewCtrl.dismiss(item);
   }
   updateSearch(){
      console.log('modal > updateSearch');
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        let self = this;
        let config = { 
          /*  types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'*/
            input:this.autocomplete.query,  /*si quiero buscar por lugar exacto  input: 'lugar'+this.autocomplete.query,*/
            componentRestrictions: { country: 'MX' } 
        }
    this.service.getPlacePredictions(config, function (predictions, status){
        console.log('modal > getPlacePredictions > status > ', status);
        self.autocompleteItems = [];
         self.zone.run(function () {
        if(predictions){
          predictions.forEach(function (prediction){
            self.autocompleteItems.push(prediction);
          });
        }
      });
    });
  }

}
