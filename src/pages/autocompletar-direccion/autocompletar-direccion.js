var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ViewController } from 'ionic-angular';
var AutocompletarDireccionPage = /** @class */ (function () {
    function AutocompletarDireccionPage(navCtrl, platform, navParams, viewCtrl, zone) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.zone = zone;
        this.backPressed = false;
        this.service = new google.maps.places.AutocompleteService();
        this.ubicaciones_frecuentes_todas = [];
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
        platform.registerBackButtonAction(function () {
            if (_this.navCtrl.canGoBack()) {
                _this.dismiss();
                return;
            }
            if (!_this.backPressed) {
                _this.backPressed = true;
                _this.dismiss();
                setTimeout(function () { return _this.backPressed = false; }, 2000);
                return;
            }
            else {
                _this.platform.exitApp();
            }
        });
    }
    AutocompletarDireccionPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AutocompletarDireccionPage');
    };
    AutocompletarDireccionPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AutocompletarDireccionPage.prototype.chooseItem = function (item) {
        this.viewCtrl.dismiss(item);
    };
    AutocompletarDireccionPage.prototype.updateSearch = function () {
        console.log('modal > updateSearch');
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        var self = this;
        var config = {
            /*  types:  ['geocode'], // other types available in the API: 'establishment', 'regions', and 'cities'*/
            input: this.autocomplete.query,
            componentRestrictions: { country: 'MX' }
        };
        this.service.getPlacePredictions(config, function (predictions, status) {
            console.log('modal > getPlacePredictions > status > ', status);
            self.autocompleteItems = [];
            self.zone.run(function () {
                if (predictions) {
                    predictions.forEach(function (prediction) {
                        self.autocompleteItems.push(prediction);
                    });
                }
            });
        });
    };
    AutocompletarDireccionPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-autocompletar-direccion',
            templateUrl: 'autocompletar-direccion.html',
        }),
        __metadata("design:paramtypes", [NavController, Platform, NavParams, ViewController, NgZone])
    ], AutocompletarDireccionPage);
    return AutocompletarDireccionPage;
}());
export { AutocompletarDireccionPage };
//# sourceMappingURL=autocompletar-direccion.js.map