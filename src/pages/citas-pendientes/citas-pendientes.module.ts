import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CitasPendientesPage } from './citas-pendientes';

@NgModule({
  declarations: [
    CitasPendientesPage,
  ],
  imports: [
    IonicPageModule.forChild(CitasPendientesPage),
  ],
})
export class CitasPendientesPageModule {}
