import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrCitasProvider} from '../../providers/pr-citas/pr-citas';

@IonicPage()
@Component({
  selector: 'page-cita-lista',
  templateUrl: 'cita-lista.html',
})
export class CitaListaPage {
	citas:any;
	fecha:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_alert_toast:PrAlertToastProvider,public pr_citas:PrCitasProvider){
  	var tzoffset = (new Date()).getTimezoneOffset() * 60000;
    let fecha_actual=(new Date(Date.now() - tzoffset)).toISOString().slice(0, -1) + 'Z';
    this.fecha=fecha_actual;
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad CitaListaPage');
    this.get_citas();
  }
  atras(){
    this.navCtrl.setRoot('HomePage');
  }
  get_citas(){
  	let data_u=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
  	let mensaje='Cargando';
  	this.pr_alert_toast.show_loading(mensaje);
  	 this.pr_citas.get_citas_id_cliente(data_u.id).subscribe(
  	    result =>{
  	    	this.pr_alert_toast.dismis_loading();
          if (result) {
            this.citas=result;
            console.log(result);
          }else{
            let mensaje='No tienes citas agendadas';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
          }
  	    },
  	    err => {
  	    	this.pr_alert_toast.dismis_loading();
  	    	console.log('el error '+err);
  	    },
  	  );
  }
  ver_cita(item){
  	let data=item;
  	 localStorage.setItem('data_cita',JSON.stringify(data));
  	 this.navCtrl.push('CitaDetallePage');
  }

}
