import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CitaListaPage } from './cita-lista';

@NgModule({
  declarations: [
    CitaListaPage,
  ],
  imports: [
    IonicPageModule.forChild(CitaListaPage),
  ],
})
export class CitaListaPageModule {}
