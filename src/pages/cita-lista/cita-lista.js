var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrCitasProvider } from '../../providers/pr-citas/pr-citas';
var CitaListaPage = /** @class */ (function () {
    function CitaListaPage(navCtrl, navParams, pr_alert_toast, pr_citas) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_citas = pr_citas;
        var tzoffset = (new Date()).getTimezoneOffset() * 60000;
        var fecha_actual = (new Date(Date.now() - tzoffset)).toISOString().slice(0, -1) + 'Z';
        this.fecha = fecha_actual;
    }
    CitaListaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CitaListaPage');
        this.get_citas();
    };
    CitaListaPage.prototype.atras = function () {
        this.navCtrl.setRoot('HomePage');
    };
    CitaListaPage.prototype.get_citas = function () {
        var _this = this;
        var data_u = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        var mensaje = 'Cargando';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_citas.get_citas_id_cliente(data_u.id).subscribe(function (result) {
            _this.pr_alert_toast.dismis_loading();
            if (result) {
                _this.citas = result;
                console.log(result);
            }
            else {
                var mensaje_1 = 'No tienes citas agendadas';
                _this.pr_alert_toast.mensaje_toast_pie(mensaje_1);
            }
        }, function (err) {
            _this.pr_alert_toast.dismis_loading();
            console.log('el error ' + err);
        });
    };
    CitaListaPage.prototype.ver_cita = function (item) {
        var data = item;
        localStorage.setItem('data_cita', JSON.stringify(data));
        this.navCtrl.push('CitaDetallePage');
    };
    CitaListaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-cita-lista',
            templateUrl: 'cita-lista.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrAlertToastProvider, PrCitasProvider])
    ], CitaListaPage);
    return CitaListaPage;
}());
export { CitaListaPage };
//# sourceMappingURL=cita-lista.js.map