import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetInmueblePage } from './det-inmueble';

@NgModule({
  declarations: [
    DetInmueblePage,
  ],
  imports: [
    IonicPageModule.forChild(DetInmueblePage),
  ],
})
export class DetInmueblePageModule {}
