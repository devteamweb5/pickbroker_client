var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import { PrRutasProvider } from '../../providers/pr-rutas/pr-rutas';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SocialSharing } from '@ionic-native/social-sharing';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrInmuebleProvider } from '../../providers/pr-inmueble/pr-inmueble';
var DetInmueblePage = /** @class */ (function () {
    function DetInmueblePage(navCtrl, navParams, pr_rutas, photoViewer, iab, pr_alert_toast, social_sharing, pr_inmueble) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_rutas = pr_rutas;
        this.photoViewer = photoViewer;
        this.iab = iab;
        this.pr_alert_toast = pr_alert_toast;
        this.social_sharing = social_sharing;
        this.pr_inmueble = pr_inmueble;
        this.imagenes = ['blue'];
        this.det_inmueble = {
            titulo: '',
            id: '',
            imagen: '',
            precio: '',
            direccion: '',
            lat_long: '',
            descripcion: '',
            terreno: '',
            construido: '',
            recamaras: '',
            banios: '',
            cochera: '',
            antiguedad: '',
            fecha_publicacion: '',
            fecha_vencimiento: '',
        };
        this.ruta_corazon = '../assets/imgs/corazon_inactivo.png';
        //this.ruta_imagenes=this.pr_rutas.get_ruta_imagenes();
    }
    DetInmueblePage.prototype.ionViewDidLoad = function () {
        this.pr_alert_toast.dismis_loading();
        console.log('ionViewDidLoad PropiedadesDetallePage');
        this.get_det_inmueble();
        this.ver_like_inmueble();
    };
    DetInmueblePage.prototype.mostrar_zoom = function (item) {
        this.photoViewer.show(this.ruta_imagenes + item);
    };
    DetInmueblePage.prototype.compartir = function (item) {
        var _this = this;
        console.log(item);
        var user = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        var url = 'http://picbroker-landing.bsmx.tech/inmueble/' + item.id + '/' + user.id + '';
        var url_2 = url.toString();
        var mensaje = null;
        var mensaje_2 = "Espere por favor";
        this.pr_alert_toast.show_loading(mensaje_2);
        this.social_sharing.share(mensaje, mensaje, mensaje, url).then(function () {
            _this.pr_alert_toast.dismis_loading();
            console.log('envio el mensaje');
        }).catch(function () { });
    };
    DetInmueblePage.prototype.abrir_navegador = function (item) {
        var data_u = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        var id_usuario;
        for (var _i = 0, data_u_1 = data_u; _i < data_u_1.length; _i++) {
            var value = data_u_1[_i];
            id_usuario = value.id;
        }
        var browser = this.iab.create('http://picbroker-landing.bsmx.tech/inmueble/' + item + '/' + id_usuario);
        browser.show();
    };
    DetInmueblePage.prototype.ver_like_inmueble = function () {
        var _this = this;
        this.user = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        this.pr_inmueble.get_favoritos(this.user.id).subscribe(function (result) {
            console.log(result);
            _this.favoritos = result;
            for (var i = 0; i < _this.favoritos.length; ++i) {
                if (_this.det_inmueble.id == _this.favoritos[i].inmueble.id) {
                    _this.ruta_corazon = '../assets/imgs/corazon_activo.png';
                }
            }
        });
        // let favoritos=JSON.parse(localStorage.getItem('favoritos_inmueble'));
        // if(favoritos!=undefined || favoritos!=null ){
        //   if(favoritos.length==0){
        //     this.ruta_corazon='../assets/imgs/corazon_inactivo.png';
        //   }
        //   let i=0;
        //   for(let value of favoritos){
        //     if(this.det_inmueble.id_inmueble==value.id_inmueble) {
        //       this.ruta_corazon='../assets/imgs/corazon_activo.png';
        //       i=1;
        //     }else{
        //     	if(i==0){
        //         this.ruta_corazon='../assets/imgs/corazon_inactivo.png';
        //       }
        //     }
        //   }
        // }else{
        //   this.ruta_corazon='../assets/imgs/corazon_inactivo.png';
        // }
    };
    DetInmueblePage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    DetInmueblePage.prototype.get_det_inmueble = function () {
        var _this = this;
        var data = JSON.parse(localStorage.getItem('det_inmueble'));
        console.log(data);
        this.det_inmueble.imagenes = data.imagenes;
        this.det_inmueble.imagen = data.det_inmuebles.imagen;
        this.det_inmueble.titulo = data.det_inmuebles.titulo;
        this.det_inmueble.id = data.det_inmuebles.id;
        this.det_inmueble.precio = data.det_inmuebles.precio;
        this.det_inmueble.direccion = data.det_inmuebles.direccion;
        this.det_inmueble.lat_long = data.det_inmuebles.latLong;
        var lat_explota = data.det_inmuebles.latLong.split(',');
        this.lat = lat_explota['0'];
        this.long = lat_explota['1'];
        this.det_inmueble.descripcion = data.det_inmuebles.descripcion;
        this.det_inmueble.terreno = data.det_inmuebles.terreno;
        this.det_inmueble.construido = data.det_inmuebles.construido;
        this.det_inmueble.recamaras = data.det_inmuebles.recamaras;
        this.det_inmueble.banios = data.det_inmuebles.banios;
        this.det_inmueble.cochera = data.det_inmuebles.cochera;
        this.det_inmueble.antiguedad = data.det_inmuebles.antiguedad;
        this.det_inmueble.fecha_publicacion = data.det_inmuebles.fecha_publicacion;
        this.det_inmueble.fecha_vencimiento = data.det_inmuebles.fecha_vencimiento;
        //this.imagenes=data.imagenes;
        //console.log(this.imagenes);
        this.caracteristica = data.caracteristicas;
        this.servicio = data.servicios;
        this.lat = ;
        this.long;
        setTimeout(function () {
            _this.map = _this.loadMap();
        }, 1000);
        console.log(this.loadMap());
    };
    DetInmueblePage.prototype.loadMap = function (location) {
        var _this = this;
        if (location === void 0) { location = new google.maps.LatLng(20.708799, -103.410071); }
        console.log('mapa cargado');
        var mapOptions = {
            center: location,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };
        var mapEl = document.getElementById('map_canvas');
        var map = new google.maps.Map(mapEl, mapOptions);
        // Wait the MAP_READY before using any methods.
        navigator.geolocation.getCurrentPosition(function (position) {
            var newLatLng = new google.maps.LatLng(_this.lat, _this.long);
            _this.map.setCenter(newLatLng);
            var lat = position.coords.latitude;
            var long = position.coords.longitude;
            var icono = "assets/imgs/pin.png";
            console.log(icono);
            var marker = new google.maps.Marker({
                map: _this.map,
                animation: google.maps.Animation.DROP,
                position: newLatLng,
                draggable: false,
                icon: icono
            });
        });
        return map;
    };
    DetInmueblePage.prototype.contactar = function () {
        var data = JSON.parse(localStorage.getItem('det_inmueble'));
        console.log(data);
        this.navCtrl.push('InmuebleContactartDuenoPage');
    };
    DetInmueblePage.prototype.guardar_favoritos = function () {
        if (this.ruta_corazon == '../assets/imgs/corazon_activo.png') {
            this.ruta_corazon = '../assets/imgs/corazon_inactivo.png';
        }
        else {
            this.ruta_corazon = '../assets/imgs/corazon_activo.png';
        }
        var datos = { 'user': this.user.id, 'inmueble': this.det_inmueble.id };
        this.pr_inmueble.set_favoritos(datos).subscribe(function (result) {
            console.log(result);
        });
        // let favoritos=JSON.parse(localStorage.getItem('favoritos_inmueble'));
        // let id_inmueble=this.det_inmueble.id_inmueble;
        // let favoritos_guardados:any[]=[];
        // let i=0;
        // console.log(favoritos);
        // if(favoritos==undefined || favoritos==null){ /*si no existe los favoritos crea uno y sale*/
        //    favoritos_guardados.push({
        //      id_inmueble:this.det_inmueble.id_inmueble,
        //      imagen:this.det_inmueble.imagen,
        //      titulo:this.det_inmueble.titulo,
        //      precio:this.det_inmueble.precio,
        //      direccion:this.det_inmueble.direccion,
        //      lat_long:this.det_inmueble.lat_long,
        //      descripcion:this.det_inmueble.descripcion,
        //      terreno:this.det_inmueble.terreno,
        //      construido:this.det_inmueble.construido,
        //      recamaras:this.det_inmueble.recamaras,
        //      banios:this.det_inmueble.banios,
        //      cochera:this.det_inmueble.cochera,
        //      antiguedad:this.det_inmueble.antiguedad,
        //      latLong:this.det_inmueble.latLong,
        //      fecha_publicacion:this.det_inmueble.fecha_publicacion,
        //      fecha_vencimiento:this.det_inmueble.fecha_vencimiento
        //    });
        //    this.ruta_corazon='../assets/imgs/corazon_activo.png';
        //    localStorage.setItem('favoritos_inmueble',JSON.stringify(favoritos_guardados));
        //  }else{
        //    favoritos_guardados=favoritos; /* si existe tengo que cargar los favoritos del storage en favoritos guardados*/
        //    let i=0;
        //    let j=0;
        //    let k=0;
        //    let l=0;
        //    let longitud=favoritos_guardados.length;
        //    for (k = 0; k < longitud;) {
        //      for(let value_2 of favoritos_guardados){
        //        if(this.det_inmueble.id_inmueble==value_2.id_inmueble){
        //          k=longitud;
        //          j=1;
        //          l=i;
        //        }else{
        //          k++;
        //        }
        //        i++;
        //      }
        //    }
        //    if(j==1) { /*si existe saquelo de la lista*/
        //      favoritos_guardados.splice(l,1);
        //      this.ruta_corazon='../assets/imgs/corazon_inactivo.png';
        //      localStorage.setItem('favoritos_inmueble',JSON.stringify(favoritos_guardados));
        //    }else{ /*si no existe metalo a la lista de favoritos guardados*/
        //    	favoritos_guardados.push({
        //        id_inmueble:this.det_inmueble.id_inmueble,
        //        imagen:this.det_inmueble.imagen,
        //        titulo:this.det_inmueble.titulo,
        //        precio:this.det_inmueble.precio,
        //        direccion:this.det_inmueble.direccion,
        //        latLong:this.det_inmueble.lat_long,
        //        descripcion:this.det_inmueble.descripcion,
        //        terreno:this.det_inmueble.terreno,
        //        construido:this.det_inmueble.construido,
        //        recamaras:this.det_inmueble.recamaras,
        //        banios:this.det_inmueble.banios,
        //        cochera:this.det_inmueble.cochera,
        //        antiguedad:this.det_inmueble.antiguedad,
        //        fecha_publicacion:this.det_inmueble.fecha_publicacion,
        //        fecha_vencimiento:this.det_inmueble.fecha_vencimiento
        //      });
        //      this.ruta_corazon='../assets/imgs/corazon_activo.png';
        //      localStorage.setItem('favoritos_inmueble',JSON.stringify(favoritos_guardados));
        //    }
        //  }
    };
    __decorate([
        ViewChild(Slides),
        __metadata("design:type", Slides)
    ], DetInmueblePage.prototype, "slides", void 0);
    DetInmueblePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-det-inmueble',
            templateUrl: 'det-inmueble.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            PrRutasProvider,
            PhotoViewer,
            InAppBrowser,
            PrAlertToastProvider,
            SocialSharing,
            PrInmuebleProvider])
    ], DetInmueblePage);
    return DetInmueblePage;
}());
export { DetInmueblePage };
//# sourceMappingURL=det-inmueble.js.map