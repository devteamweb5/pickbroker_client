import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides  } from 'ionic-angular';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SocialSharing } from '@ionic-native/social-sharing';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrInmuebleProvider } from '../../providers/pr-inmueble/pr-inmueble';
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-det-inmueble',
  templateUrl: 'det-inmueble.html',
})
export class DetInmueblePage {
  @ViewChild(Slides) slides: Slides;
  imagenes=['blue'];
  inmueble;
  user;
  det_inmueble:any={
    titulo:'',
    id:'',
    imagen:'',
    precio:'',
    direccion:'',
    lat_long:'',
    descripcion:'',
    terreno:'',
    construido:'',
    recamaras:'',
    banios:'',
    cochera:'',
    antiguedad:'',
    fecha_publicacion:'',
    fecha_vencimiento:'',
  };
  caracteristica:any;
  servicio:any;
  ruta_imagenes:any;
  ruta_corazon='../assets/imgs/corazon_inactivo.png';
  map:any;
  lat:any;
  long:any;
  favoritos;
  prev;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public pr_rutas:PrRutasProvider,
    private photoViewer: PhotoViewer,
    public iab:InAppBrowser,
    public pr_alert_toast:PrAlertToastProvider,
    public social_sharing:SocialSharing,
    public pr_inmueble : PrInmuebleProvider,
    ) {
  	//this.ruta_imagenes=this.pr_rutas.get_ruta_imagenes();
  }

  ionViewDidLoad() {
    this.prev = this.navParams.get('prev');

    this.pr_alert_toast.dismis_loading();
    console.log('ionViewDidLoad PropiedadesDetallePage');
    this.get_det_inmueble();
    this.ver_like_inmueble();
  }
  mostrar_zoom(item){
    this.photoViewer.show(this.ruta_imagenes+item);
  }
  compartir(item){
    console.log(item);
    let user=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    let url='http://picbroker-landing.bsmx.tech/inmueble/'+item.id+'/'+user.id+'';
    let url_2=url.toString(); 
    let mensaje=null;
    let mensaje_2="Espere por favor";
    this.pr_alert_toast.show_loading(mensaje_2);
    this.social_sharing.share(mensaje,mensaje,mensaje,url).then(()=>{
      this.pr_alert_toast.dismis_loading();
      console.log('envio el mensaje');
    }).catch(()=>{});
  }
  abrir_navegador(item){
    let data_u=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    let id_usuario;
    for(let value of data_u) {
      id_usuario=value.id;
    }
    const browser = this.iab.create('http://picbroker-landing.bsmx.tech/inmueble/'+item+'/'+id_usuario);
    browser.show();
  }
  
  ver_like_inmueble(){
    this.user = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    this.pr_inmueble.get_favoritos(this.user.id).subscribe(result =>{
      console.log(result);
      this.favoritos = result;
       for (var i = 0; i < this.favoritos.length; ++i) {
      if (this.det_inmueble.id == this.favoritos[i].inmueble.id) {
        this.ruta_corazon = '../assets/imgs/corazon_activo.png';
      }
    }
    })
   
    // let favoritos=JSON.parse(localStorage.getItem('favoritos_inmueble'));
    // if(favoritos!=undefined || favoritos!=null ){
    //   if(favoritos.length==0){
    //     this.ruta_corazon='../assets/imgs/corazon_inactivo.png';
    //   }
    //   let i=0;
    //   for(let value of favoritos){
    //     if(this.det_inmueble.id_inmueble==value.id_inmueble) {
    //       this.ruta_corazon='../assets/imgs/corazon_activo.png';
    //       i=1;
    //     }else{
    //     	if(i==0){
    //         this.ruta_corazon='../assets/imgs/corazon_inactivo.png';
    //       }

    //     }
    //   }
    // }else{
    //   this.ruta_corazon='../assets/imgs/corazon_inactivo.png';
    // }
  }
  atras(){
    if (this.prev) {
      this.navCtrl.setRoot(this.prev);
    }else{
  	  this.navCtrl.pop();
    }
  }
  get_det_inmueble(){
  	let data=JSON.parse(localStorage.getItem('det_inmueble'));
  	console.log(data);
    this.det_inmueble.imagenes = data.imagenes;
    this.det_inmueble.imagen=data.det_inmuebles.imagen;
    this.det_inmueble.titulo=data.det_inmuebles.titulo;
    this.det_inmueble.id=data.det_inmuebles.id;
    this.det_inmueble.precio=data.det_inmuebles.precio;
    this.det_inmueble.direccion=data.det_inmuebles.direccion;
    this.det_inmueble.lat_long=data.det_inmuebles.latLong;
    let lat_explota= data.det_inmuebles.latLong.split(',');
    this.lat=lat_explota['0'];
    this.long=lat_explota['1'];
    this.det_inmueble.descripcion=data.det_inmuebles.descripcion;
    this.det_inmueble.terreno=data.det_inmuebles.terreno;
    this.det_inmueble.construido=data.det_inmuebles.construido;
    this.det_inmueble.recamaras=data.det_inmuebles.recamaras;
    this.det_inmueble.banios=data.det_inmuebles.banios;
    this.det_inmueble.cochera=data.det_inmuebles.cochera;
    this.det_inmueble.antiguedad=data.det_inmuebles.antiguedad;
    this.det_inmueble.fecha_publicacion=data.det_inmuebles.fecha_publicacion;
    this.det_inmueble.fecha_vencimiento=data.det_inmuebles.fecha_vencimiento;
    //this.imagenes=data.imagenes;
    //console.log(this.imagenes);
    this.caracteristica=data.caracteristicas;
    this.servicio=data.servicios;
    setTimeout(()=>{
      this.map=this.loadMap();
    },1000);
  }
  loadMap(location= new google.maps.LatLng(20.708799, -103.410071)){
    console.log('mapa cargado');
    let mapOptions={
      center: location,
      zoom:16, 
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    }
    let mapEl=document.getElementById('map_canvas');
    let map= new google.maps.Map(mapEl,mapOptions);
    // Wait the MAP_READY before using any methods.
    navigator.geolocation.getCurrentPosition(
      (position) => {
      	let newLatLng = new google.maps.LatLng(this.lat,this.long);
        map.setCenter(newLatLng);
        let lat=position.coords.latitude;
        let long=position.coords.longitude;
        var icono ="assets/imgs/pin.png";
        console.log(icono);

        let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: newLatLng,
          draggable: false,
          icon:icono
        });
      }
      );
    return map;
  }
  contactar(){
  	let data=JSON.parse(localStorage.getItem('det_inmueble'));
  	console.log(data);
  	this.navCtrl.push('InmuebleContactartDuenoPage');
  }
  guardar_favoritos(){
    if (this.ruta_corazon == '../assets/imgs/corazon_activo.png') {
      this.ruta_corazon = '../assets/imgs/corazon_inactivo.png';
    }else{
      this.ruta_corazon = '../assets/imgs/corazon_activo.png'
    }
    let datos = {'user':this.user.id ,'inmueble':this.det_inmueble.id}
    this.pr_inmueble.set_favoritos(datos).subscribe(result=>{
      console.log(result);
    })
  	// let favoritos=JSON.parse(localStorage.getItem('favoritos_inmueble'));
  	// let id_inmueble=this.det_inmueble.id_inmueble;
  	// let favoritos_guardados:any[]=[];
  	// let i=0;
  	// console.log(favoritos);
  	// if(favoritos==undefined || favoritos==null){ /*si no existe los favoritos crea uno y sale*/
   //    favoritos_guardados.push({
   //      id_inmueble:this.det_inmueble.id_inmueble,
   //      imagen:this.det_inmueble.imagen,
   //      titulo:this.det_inmueble.titulo,
   //      precio:this.det_inmueble.precio,
   //      direccion:this.det_inmueble.direccion,
   //      lat_long:this.det_inmueble.lat_long,
   //      descripcion:this.det_inmueble.descripcion,
   //      terreno:this.det_inmueble.terreno,
   //      construido:this.det_inmueble.construido,
   //      recamaras:this.det_inmueble.recamaras,
   //      banios:this.det_inmueble.banios,
   //      cochera:this.det_inmueble.cochera,
   //      antiguedad:this.det_inmueble.antiguedad,
   //      latLong:this.det_inmueble.latLong,
   //      fecha_publicacion:this.det_inmueble.fecha_publicacion,
   //      fecha_vencimiento:this.det_inmueble.fecha_vencimiento
   //    });
   //    this.ruta_corazon='../assets/imgs/corazon_activo.png';
   //    localStorage.setItem('favoritos_inmueble',JSON.stringify(favoritos_guardados));
   //  }else{
   //    favoritos_guardados=favoritos; /* si existe tengo que cargar los favoritos del storage en favoritos guardados*/
   //    let i=0;
   //    let j=0;
   //    let k=0;
   //    let l=0;
   //    let longitud=favoritos_guardados.length;
   //    for (k = 0; k < longitud;) {
   //      for(let value_2 of favoritos_guardados){
   //        if(this.det_inmueble.id_inmueble==value_2.id_inmueble){
   //          k=longitud;
   //          j=1;
   //          l=i;
   //        }else{
   //          k++;
   //        }
   //        i++;
   //      }
   //    }
   //    if(j==1) { /*si existe saquelo de la lista*/
   //      favoritos_guardados.splice(l,1);
   //      this.ruta_corazon='../assets/imgs/corazon_inactivo.png';
   //      localStorage.setItem('favoritos_inmueble',JSON.stringify(favoritos_guardados));
   //    }else{ /*si no existe metalo a la lista de favoritos guardados*/
   //    	favoritos_guardados.push({
   //        id_inmueble:this.det_inmueble.id_inmueble,
   //        imagen:this.det_inmueble.imagen,
   //        titulo:this.det_inmueble.titulo,
   //        precio:this.det_inmueble.precio,
   //        direccion:this.det_inmueble.direccion,
   //        latLong:this.det_inmueble.lat_long,
   //        descripcion:this.det_inmueble.descripcion,
   //        terreno:this.det_inmueble.terreno,
   //        construido:this.det_inmueble.construido,
   //        recamaras:this.det_inmueble.recamaras,
   //        banios:this.det_inmueble.banios,
   //        cochera:this.det_inmueble.cochera,
   //        antiguedad:this.det_inmueble.antiguedad,
   //        fecha_publicacion:this.det_inmueble.fecha_publicacion,
   //        fecha_vencimiento:this.det_inmueble.fecha_vencimiento
   //      });
   //      this.ruta_corazon='../assets/imgs/corazon_activo.png';
   //      localStorage.setItem('favoritos_inmueble',JSON.stringify(favoritos_guardados));

   //    }
   //  }

  }


}
