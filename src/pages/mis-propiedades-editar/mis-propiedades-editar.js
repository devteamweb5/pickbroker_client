var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController, ToastController, Events, ModalController } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrInmuebleProvider } from '../../providers/pr-inmueble/pr-inmueble';
import { PrServicioProvider } from '../../providers/pr-servicio/pr-servicio';
import { PrTipoInmueblePublicacionProvider } from '../../providers/pr-tipo-inmueble-publicacion/pr-tipo-inmueble-publicacion';
import { ImagePicker } from '@ionic-native/image-picker';
import { PrRutasProvider } from '../../providers/pr-rutas/pr-rutas';
import { FileTransfer } from '@ionic-native/file-transfer';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
var MisPropiedadesEditarPage = /** @class */ (function () {
    /*renta*/
    function MisPropiedadesEditarPage(navCtrl, navParams, camera, platform, file, filePath, actionSheetCtrl, toastCtrl, events, pr_alert_toast, modalCtrl, pr_inmueble, pr_servicio, pr_tipo_inmueble_publicacion, imagePicker, transfer, pr_rutas) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.camera = camera;
        this.platform = platform;
        this.file = file;
        this.filePath = filePath;
        this.actionSheetCtrl = actionSheetCtrl;
        this.toastCtrl = toastCtrl;
        this.events = events;
        this.pr_alert_toast = pr_alert_toast;
        this.modalCtrl = modalCtrl;
        this.pr_inmueble = pr_inmueble;
        this.pr_servicio = pr_servicio;
        this.pr_tipo_inmueble_publicacion = pr_tipo_inmueble_publicacion;
        this.imagePicker = imagePicker;
        this.transfer = transfer;
        this.pr_rutas = pr_rutas;
        this.data_propiedad = {
            id: '',
            id_usuario: '',
            id_status_publicacion: 3,
            id_tipo_publicacion: '',
            id_tipo_inmueble: '',
            titulo: '',
            inmobiliaria: '',
            precio: '',
            direccion: '',
            lat_long: '',
            descripcion: '',
            terreno: '',
            construido: '',
            recamaras: '',
            banios: '',
            cochera: '',
            antiguedad: '',
            fecha_publicacion: '',
            fecha_vencimiento: '',
            caracteristicas: null,
            servicios: null,
        };
        this.data_direccion = {
            street: '',
            num_ext: '',
            zip: '',
            colonia: '',
            municipio: '',
            ciudad: '',
            estado: '',
            id_tipo_propiedad: '',
            recamaras: '',
            banos: '',
            medios_banos: '',
            estacionamientos: '',
            area_construida: '',
            superficie_terreno: '',
            amenities: '',
            edad: '',
            latitud: '',
            longitud: '',
            user_id: '',
            administrativeArea: '',
            country: 'MX',
        };
        this.nueva_caracteristica = 0;
        this.caracteristica = [];
        this.servicio = null;
        this.mis_imagenes = [];
        this.fotos_agregadas = [];
        this.deshabilitado = false;
        this.delete_images = [];
    }
    MisPropiedadesEditarPage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    MisPropiedadesEditarPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad mis propiedades edit');
        var datos = { 'id': this.navParams.get('id') };
        console.log(datos);
        this.pr_inmueble.get_inmueble(datos).subscribe(function (result) {
            console.log(result);
            _this.data_propiedad.id = result.inmueble.id;
            _this.data_propiedad.id_usuario = result.inmueble.user.id;
            _this.data_propiedad.id_tipo_publicacion = result.inmueble.tipoPublicacion.id;
            _this.data_propiedad.id_tipo_inmueble = result.inmueble.tipoInmueble.id;
            _this.data_propiedad.titulo = result.inmueble.titulo;
            _this.data_propiedad.inmobiliaria = result.inmueble.inmobiliaria;
            // this.data_propiedad.precio = result.inmueble.precio;
            _this.data_propiedad.direccion = result.inmueble.direccion;
            _this.data_propiedad.lat_long = result.inmueble.latLong;
            _this.data_propiedad.descripcion = result.inmueble.descripcion;
            _this.data_propiedad.terreno = result.inmueble.terreno;
            _this.data_propiedad.construido = result.inmueble.construido;
            _this.data_propiedad.recamaras = result.inmueble.recamaras;
            _this.data_propiedad.banios = result.inmueble.banios;
            _this.data_propiedad.cochera = result.inmueble.cochera;
            _this.data_propiedad.antiguedad = result.inmueble.antiguedad;
            _this.data_propiedad.caracteristicas = result.inmueble.caracteristicas;
            _this.caracteristica = result.inmueble.caracteristicas ? result.inmueble.caracteristicas.split(',') : [];
            _this.servicio = result.inmueble.servicios ? result.inmueble.servicios.split(',') : [];
            console.log(_this.data_propiedad);
            console.log(_this.servicio);
            _this.mis_imagenes = result.imagenes;
            console.log(_this.mis_imagenes);
        });
        this.get_servicio_inmueble();
        this.tipo_publicacion();
        this.map = this.loadMap();
    };
    MisPropiedadesEditarPage.prototype.tipo_publicacion = function () {
        var _this = this;
        var mensaje = 'Cargando tipo de publicación';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_tipo_inmueble_publicacion.get_tipo_publicacion().subscribe(function (pr_tipo_inmueble_publicacion) {
            _this.pr_alert_toast.dismis_loading();
            var resultado = pr_tipo_inmueble_publicacion;
            _this.tipo_inmueble();
            if (resultado) {
                _this.data_tipo_publicacion = resultado;
                console.log(_this.data_tipo_publicacion);
            }
            else {
                var mensaje_1 = 'No existen tipo de publicaciones';
                _this.pr_alert_toast.mensaje_toast_pie(mensaje_1);
            }
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    MisPropiedadesEditarPage.prototype.deleteImage = function (index) {
        var prev = this.slides.getPreviousIndex();
        // let prev = this.slides.slidePrev();
        this.slides.slideTo(index - 1, 500);
        this.fotos_agregadas.splice(index, 1);
        console.log(this.fotos_agregadas);
    };
    MisPropiedadesEditarPage.prototype.deleteImageExist = function (index, id) {
        var prev = this.slides.getPreviousIndex();
        // let prev = this.slides.slidePrev();
        this.slides.slideTo(index - 1, 500);
        this.mis_imagenes.splice(index, 1);
        this.delete_images.push(id);
        console.log(this.mis_imagenes);
    };
    MisPropiedadesEditarPage.prototype.deleteImagesApi = function () {
        var datos = { 'images': this.delete_images };
        this.pr_inmueble.del_imagenes(datos).subscribe(function (result) {
            console.log(result);
        });
    };
    MisPropiedadesEditarPage.prototype.tipo_inmueble = function () {
        var _this = this;
        var mensaje = 'Cargando tipo de inmuebles';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_tipo_inmueble_publicacion.get_tipo_inmueble().subscribe(function (pr_tipo_inmueble_publicacion) {
            _this.pr_alert_toast.dismis_loading();
            var resultado = pr_tipo_inmueble_publicacion;
            if (resultado) {
                _this.data_tipo_inmueble = resultado;
            }
            else {
                var mensaje_2 = 'No existen tipo de inmuebles';
                _this.pr_alert_toast.mensaje_toast_pie(mensaje_2);
            }
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    MisPropiedadesEditarPage.prototype.agregar_caracteristica = function () {
        this.nueva_caracteristica = 1;
    };
    MisPropiedadesEditarPage.prototype.guardar_caracteristica = function () {
        this.nueva_caracteristica = 0;
        this.caracteristica.push(this.caracteristica_agregar);
        this.caracteristica_agregar = '';
        console.log(this.caracteristica);
    };
    MisPropiedadesEditarPage.prototype.guardar_servicio = function (item) {
        var nombre_servicio = item;
        this.nueva_caracteristica = 0;
        this.servicio = nombre_servicio;
        this.servicio_agregar = '';
        console.log(this.servicio);
    };
    MisPropiedadesEditarPage.prototype.get_servicio_inmueble = function () {
        var _this = this;
        this.pr_servicio.get_servicio().subscribe(function (pr_servicio) {
            console.log(pr_servicio);
            var resultado = pr_servicio;
            if (resultado) {
                _this.servicio_select = resultado;
            }
        }, function (err) {
            console.log(err);
        });
    };
    MisPropiedadesEditarPage.prototype.pickImage = function () {
        var _this = this;
        var options = {};
        this.imagePicker.getPictures(options).then(function (results) {
            results.forEach(function (file) {
                var filePath = file.replace('file://', 'http://localhost:8080/_file_');
                _this.fotos_agregadas.push({ 'file': file, 'filePath': filePath });
                console.log(_this.fotos_agregadas);
            });
        }, function (err) { });
    };
    MisPropiedadesEditarPage.prototype.pickCamera = function () {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.NATIVE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };
        this.camera.getPicture(options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            var filePath = imageData.replace('file://', 'http://localhost:8080/_file_');
            _this.fotos_agregadas.push({ 'file': imageData, 'filePath': filePath });
            console.log(_this.fotos_agregadas);
            _this.photo = imageData;
            console.log(_this.photo);
        }, function (err) {
            console.log(err);
            // Handle error
        });
    };
    MisPropiedadesEditarPage.prototype.selectMethodImage = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Seleccione un metodo',
            buttons: [
                {
                    text: 'Desde Galeria',
                    handler: function () {
                        _this.pickImage();
                    }
                }, {
                    text: 'Desde camara',
                    handler: function () {
                        _this.pickCamera();
                    }
                }
            ]
        });
        actionSheet.present();
    };
    // full example
    MisPropiedadesEditarPage.prototype.upload = function (idPropiedad) {
        var _this = this;
        this.pr_alert_toast.show_loading('subiendo Imagenes');
        console.log(this.fotos_agregadas);
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: 'file',
            fileName: 'name.jpg',
            params: { 'propiedad': idPropiedad },
            httpMethod: 'post',
            headers: {}
        };
        console.log(options);
        var imagenesSubidas = 0;
        for (var i = 0; i < this.fotos_agregadas.length; ++i) {
            fileTransfer.upload(this.fotos_agregadas[i].file, this.pr_rutas.route + 'imagenes/new', options)
                .then(function (data) {
                imagenesSubidas++;
                if (imagenesSubidas == _this.fotos_agregadas.length) {
                    _this.pr_alert_toast.dismis_loading();
                    _this.navCtrl.push('PropiedadGuardadaPage');
                }
                console.log(data);
                // success
            }, function (err) {
                console.log(err);
                if (imagenesSubidas == _this.fotos_agregadas.length) {
                    _this.pr_alert_toast.dismis_loading();
                    _this.navCtrl.push('PropiedadGuardadaPage');
                }
                // error
            });
        }
    };
    MisPropiedadesEditarPage.prototype.buscar_direccion = function () {
        var _this = this;
        var modal = this.modalCtrl.create('AutocompletarDireccionPage');
        modal.onDidDismiss(function (data) {
            if (data) {
                _this.data_propiedad.direccion = data.description;
                _this.getPlaceDetail_i(data.place_id);
            }
        });
        modal.present();
    };
    MisPropiedadesEditarPage.prototype.getPlaceDetail_i = function (place_id) {
        var self = this;
        var request = {
            placeId: place_id
        };
        this.placesService = new google.maps.places.PlacesService(this.map);
        this.placesService.getDetails(request, callback);
        function callback(place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                console.log('page > getPlaceDetail > place > ', place);
                var lat_i = place.geometry.location.lat();
                var lng_i = place.geometry.location.lng();
                var lat_lng = lat_i + ',' + lng_i;
                self.data_propiedad.latitud = lat_i;
                self.data_propiedad.longitud = lng_i;
                self.data_propiedad.lat_long = lat_lng;
                console.log(self.data_propiedad.lat_long);
            }
            else {
                console.log('page > getPlaceDetail > status > ', status);
            }
        }
    };
    MisPropiedadesEditarPage.prototype.loadMap = function (location) {
        var _this = this;
        if (location === void 0) { location = new google.maps.LatLng(20.674137, -103.346852); }
        var mapOptions = {
            center: location,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };
        var mapEl = document.getElementById('map_canvas');
        var map = new google.maps.Map(mapEl, mapOptions);
        // Wait the MAP_READY before using any methods.
        navigator.geolocation.getCurrentPosition(function (position) {
            var newLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            _this.map.setCenter(newLatLng);
            var marker = new google.maps.Marker({
                map: _this.map,
                animation: google.maps.Animation.DROP,
                position: newLatLng,
                draggable: false,
            });
        });
        return map;
    };
    MisPropiedadesEditarPage.prototype.calcular_precio = function () {
        var _this = this;
        if (this.data_propiedad.nombre == '') {
            var mensaje = 'Ingrese un nombre de inmobiliaria';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.titulo == '') {
            var mensaje = 'Ingrese un titulo';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.direccion == '') {
            var mensaje = 'Ingrese una direccion';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.descripcion == '') {
            var mensaje = 'Ingrese una descripción';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.terreno == '') {
            var mensaje = 'Ingrese los metros del terreno';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.construido == '') {
            var mensaje = 'Ingrese los metros construidoa';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.recamaras == '') {
            var mensaje = 'Ingrese cantidad de recamaras';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.banios == '') {
            var mensaje = 'Ingrese cantidad de baños';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.cochera == '') {
            var mensaje = 'Ingrese cantidad de cocheras';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else {
            var data = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
            this.data_propiedad.id_usuario = data.id;
            var mensaje = 'Calculando precio de la propiedad';
            this.pr_alert_toast.show_loading(mensaje);
            this.pr_inmueble.calculo_inmueble_yals(this.data_propiedad).subscribe(function (result) {
                _this.pr_alert_toast.dismis_loading();
                _this.data_propiedad.precio = Math.round(result);
                _this.deshabilitado = true;
                /*
                let resultado=pr_inmueble;
                if(resultado.mensaje=='datos_precio_inmueble'){
                  this.deshabilitado=true;
                  let data=resultado;
                  console.log(data.error);
                  if(data.error){
                    this.pr_alert_toast.mensaje_toast_pie(data.error);
                  }else{
                    if(this.data_propiedad.id_tipo_publicacion==1){
                      //renta
                      this.data_propiedad.precio=Math.round(data.valuacion_renta.valuacion);
                      this.deshabilitado=true;
                    }else if(this.data_propiedad.id_tipo_publicacion==2){
                      //venta
                      this.deshabilitado=true;
                      this.data_propiedad.precio=Math.round(data.valuacion.valuacion);
                    }else if(this.data_propiedad.id_tipo_publicacion==3){
                      //renta
                      this.deshabilitado=true;
                      this.data_propiedad.precio=Math.round(data.valuacion_renta.valuacion);
                    }
                  }
                  console.log(this.data_propiedad.id_tipo_publicacion);
                }
                */
            }, function (err) {
                console.log('el error ' + err);
            });
        }
    };
    MisPropiedadesEditarPage.prototype.guardar_inmueble = function () {
        var _this = this;
        if (this.data_propiedad.nombre == '') {
            var mensaje = 'Ingrese un nombre de inmobiliaria';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.titulo == '') {
            var mensaje = 'Ingrese un titulo';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.precio == '') {
            var mensaje = 'Ingrese un precio';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.direccion == '') {
            var mensaje = 'Ingrese una direccion';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.descripcion == '') {
            var mensaje = 'Ingrese una descripción';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.terreno == '') {
            var mensaje = 'Ingrese los metros del terreno';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.construido == '') {
            var mensaje = 'Ingrese los metros construidoa';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.recamaras == '') {
            var mensaje = 'Ingrese cantidad de recamaras';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.banios == '') {
            var mensaje = 'Ingrese cantidad de baños';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else if (this.data_propiedad.cochera == '') {
            var mensaje = 'Ingrese cantidad de cocheras';
            this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        else {
            this.data_propiedad.caracteristicas = this.caracteristica;
            this.data_propiedad.servicios = this.servicio;
            console.log(this.data_propiedad);
            var mensaje = 'Guardando propiedad';
            this.pr_alert_toast.show_loading(mensaje);
            this.pr_inmueble.editar_inmueble(this.data_propiedad).subscribe(function (result) {
                console.log(result);
                _this.pr_alert_toast.dismis_loading();
                _this.deleteImagesApi();
                _this.upload(result.id);
                /*
                let resultado=pr_inmueble;
                if(resultado.mensaje=='inmuebles_guadados_y_registrados'){
                  let id_inmueble=resultado.id_inmueble;
                  // aqui guarda las caracteristicas del inmueble
                  let data_caracteristica={
                    id_inmueble:id_inmueble,
                    caracteristica:''
                  }
                  let data_servicio={
                    id_inmueble:id_inmueble,
                    id_tipo_servicio:''
                  }
                  localStorage.setItem('inmueble_guardado',JSON.stringify(data_servicio));
                  for(let value of this.caracteristica) {
                    data_caracteristica.caracteristica=value;
                    this.pr_inmueble.guardar_caracteristica(data_caracteristica).subscribe(
                      pr_inmueble => {
                        let resultado=pr_inmueble;
                        if(resultado.mensaje=='caracteristica_guardada'){
                        }
                      },
                      err => {console.log('el error '+err);
                    },
                    );
                  }
                  let longitud_servicio=this.servicio.length;
                  let i=0;
                  this.pr_alert_toast.dismis_loading();
                  for(let value of this.servicio){
                    for(let value_2 of this.servicio_select){
                      if(value==value_2.descripcion){
                        data_servicio.id_tipo_servicio=value_2.id;
                        this.pr_inmueble.guardar_servicio(data_servicio).subscribe(
                          pr_inmueble =>{
                            let resultado=pr_inmueble;
                            if(resultado.mensaje=='servicio_guardada'){
                              i++;
                              if(longitud_servicio==i){
                                this.subir_imagen_propiedad(id_inmueble);
                                this.navCtrl.push('PropiedadGuardadaPage');
                              }
                            }
                          },
                          err => {console.log('el error '+err);
                        },
                        );
                      }
                    }
                  }
                }
                */
            }, function (err) {
                console.log(err);
            });
        }
    };
    __decorate([
        ViewChild(Slides),
        __metadata("design:type", Slides)
    ], MisPropiedadesEditarPage.prototype, "slides", void 0);
    MisPropiedadesEditarPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-mis-propiedades-editar',
            templateUrl: 'mis-propiedades-editar.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            Camera,
            Platform,
            File,
            FilePath,
            ActionSheetController,
            ToastController,
            Events,
            PrAlertToastProvider,
            ModalController,
            PrInmuebleProvider,
            PrServicioProvider,
            PrTipoInmueblePublicacionProvider,
            ImagePicker,
            FileTransfer,
            PrRutasProvider])
    ], MisPropiedadesEditarPage);
    return MisPropiedadesEditarPage;
}());
export { MisPropiedadesEditarPage };
//# sourceMappingURL=mis-propiedades-editar.js.map