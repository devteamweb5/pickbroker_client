import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MisPropiedadesEditarPage } from './mis-propiedades-editar';
import {IonTagsInputModule} from "ionic-tags-input";

@NgModule({
  declarations: [
    MisPropiedadesEditarPage,
  ],
  imports: [
    IonicPageModule.forChild(MisPropiedadesEditarPage),
    IonTagsInputModule
  ],
})
export class MisPropiedadesEditarPageModule {}
