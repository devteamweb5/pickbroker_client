var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrTipoInmueblePublicacionProvider } from '../../providers/pr-tipo-inmueble-publicacion/pr-tipo-inmueble-publicacion';
import { PrInmuebleProvider } from '../../providers/pr-inmueble/pr-inmueble';
var BuscarPropiedadPage = /** @class */ (function () {
    function BuscarPropiedadPage(navCtrl, navParams, pr_alert_toast, pr_tipo_inmueble_publicacion, pr_inmueble) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_tipo_inmueble_publicacion = pr_tipo_inmueble_publicacion;
        this.pr_inmueble = pr_inmueble;
        this.data_propiedad = {
            id_inmueble: '',
            min: '',
            max: '',
            tipo: '',
            terreno: '',
            construido: '',
            recamaras: '',
            banios: '',
            cochera: ''
        };
    }
    BuscarPropiedadPage.prototype.ionViewDidLoad = function () {
        this.tipo_inmueble();
    };
    BuscarPropiedadPage.prototype.atras = function () {
        this.navCtrl.setRoot('HomePage');
    };
    BuscarPropiedadPage.prototype.tipo_inmueble = function () {
        var _this = this;
        var mensaje = 'Cargando tipo de inmuebles';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_tipo_inmueble_publicacion.get_tipo_inmueble().subscribe(function (result) {
            _this.pr_alert_toast.dismis_loading();
            _this.data_tipo_inmueble = result;
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    BuscarPropiedadPage.prototype.buscar = function () {
        var _this = this;
        if (this.data_propiedad.id_inmueble == '') {
            if (this.data_propiedad.precio_desde == '') {
                var mensaje = 'Ingrese un precio inicial';
                this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }
            else if (this.data_propiedad.precio_hasta == '') {
                var mensaje = 'Ingrese un precio final';
                this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }
            else if (this.data_propiedad.id_tipo_inmueble == '') {
                var mensaje = 'Ingrese un tipo de inmueble';
                this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }
            /*
            else if(this.data_propiedad.terreno==''){
              let mensaje='Ingrese los metros del terreno';
              this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }else if(this.data_propiedad.construido==''){
              let mensaje='Ingrese los metros construido ';
              this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }else if(this.data_propiedad.recamaras==''){
              let mensaje='Ingrese el número de recamaras';
              this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }else if(this.data_propiedad.banios==''){
              let mensaje='Ingrese el número baños';
              this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }else if(this.data_propiedad.cochera==''){
              let mensaje='Ingrese número de cocheras';
              this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }
            */
            else {
                this.pr_inmueble.buscar_inmueble_parametros(this.data_propiedad).subscribe(function (result) {
                    if (result) {
                        localStorage.setItem('resultados', JSON.stringify(result));
                        _this.pr_alert_toast.dismis_loading();
                        _this.navCtrl.push('BuscarPropiedadResultadosPage');
                    }
                    else {
                        var mensaje = 'No hay resultados';
                        _this.pr_alert_toast.mensaje_toast_pie(mensaje);
                    }
                }, function (err) {
                    console.log(err);
                });
            }
        }
        else {
            var mensaje = 'Cargando';
            this.pr_alert_toast.show_loading(mensaje);
            this.pr_inmueble.buscar_inmueble_parametros(this.data_propiedad).subscribe(function (result) {
                if (result) {
                    localStorage.setItem('resultados', JSON.stringify(result));
                    _this.pr_alert_toast.dismis_loading();
                    _this.navCtrl.push('BuscarPropiedadResultadosPage');
                }
                else {
                    var mensaje_1 = 'No hay resultados';
                    _this.pr_alert_toast.mensaje_toast_pie(mensaje_1);
                }
            }, function (err) {
                console.log('el error ' + err);
            });
            /*va el buscar por id inmueble*/
        }
    };
    BuscarPropiedadPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-buscar-propiedad',
            templateUrl: 'buscar-propiedad.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrAlertToastProvider, PrTipoInmueblePublicacionProvider, PrInmuebleProvider])
    ], BuscarPropiedadPage);
    return BuscarPropiedadPage;
}());
export { BuscarPropiedadPage };
//# sourceMappingURL=buscar-propiedad.js.map