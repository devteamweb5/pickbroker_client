import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrTipoInmueblePublicacionProvider} from '../../providers/pr-tipo-inmueble-publicacion/pr-tipo-inmueble-publicacion';
import {PrInmuebleProvider} from '../../providers/pr-inmueble/pr-inmueble';
@IonicPage()
@Component({
  selector: 'page-buscar-propiedad',
  templateUrl: 'buscar-propiedad.html',
})
export class BuscarPropiedadPage {
	data_propiedad:any={
    id_inmueble:'',
    min:'',
    max:'',
    tipo:'',
    terreno:'',
    construido:'',
    recamaras:'',
    banios:'',
    cochera:''
  }
  data_tipo_inmueble:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_alert_toast:PrAlertToastProvider, public pr_tipo_inmueble_publicacion:PrTipoInmueblePublicacionProvider, public pr_inmueble:PrInmuebleProvider){
  }

  ionViewDidLoad(){
  	this.tipo_inmueble();
  }
  atras(){
  	this.navCtrl.setRoot('HomePage');
  }
  tipo_inmueble(){
    let mensaje='Cargando tipo de inmuebles';
    this.pr_alert_toast.show_loading(mensaje);
    this.pr_tipo_inmueble_publicacion.get_tipo_inmueble().subscribe(
      result => {
        this.pr_alert_toast.dismis_loading();
        this.data_tipo_inmueble=result;
      },
      err => {console.log('el error '+err);
    },
    );
  }
  buscar(){
    this.pr_alert_toast.show_loading('Buscando...');
    if(this.data_propiedad.id_inmueble==''){
      if(this.data_propiedad.precio_desde==''){
        let mensaje='Ingrese un precio inicial';
        this.pr_alert_toast.mensaje_toast_pie(mensaje);
      }else if(this.data_propiedad.precio_hasta==''){
        let mensaje='Ingrese un precio final';
        this.pr_alert_toast.mensaje_toast_pie(mensaje);
      }else if(this.data_propiedad.id_tipo_inmueble==''){
        let mensaje='Ingrese un tipo de inmueble';
        this.pr_alert_toast.mensaje_toast_pie(mensaje);
      }
        /*
        else if(this.data_propiedad.terreno==''){
          let mensaje='Ingrese los metros del terreno';
          this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }else if(this.data_propiedad.construido==''){
          let mensaje='Ingrese los metros construido ';
          this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }else if(this.data_propiedad.recamaras==''){
          let mensaje='Ingrese el número de recamaras';
          this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }else if(this.data_propiedad.banios==''){
          let mensaje='Ingrese el número baños';
          this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }else if(this.data_propiedad.cochera==''){
          let mensaje='Ingrese número de cocheras';
          this.pr_alert_toast.mensaje_toast_pie(mensaje);
        }
        */
        else{
          this.pr_inmueble.buscar_inmueble_parametros(this.data_propiedad).subscribe(
            result => {
              if(result){
                localStorage.setItem('resultados',JSON.stringify(result));
                this.pr_alert_toast.dismis_loading();
                this.navCtrl.push('BuscarPropiedadResultadosPage');
              }else{
                let mensaje='No hay resultados';
                this.pr_alert_toast.mensaje_toast_pie(mensaje);
              }
            },
            err => {console.log(err);
            },
            );
        } 
      }else{
        let mensaje='Cargando';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_inmueble.buscar_inmueble_parametros(this.data_propiedad).subscribe(
          result => {
            if(result){
              localStorage.setItem('resultados',JSON.stringify(result));
              this.pr_alert_toast.dismis_loading();
              this.navCtrl.push('BuscarPropiedadResultadosPage');

            }else{
              let mensaje='No hay resultados';
              this.pr_alert_toast.mensaje_toast_pie(mensaje);
            }
          },
          err => {console.log('el error '+err);
        },
        );
        /*va el buscar por id inmueble*/
      }
    }
  }
