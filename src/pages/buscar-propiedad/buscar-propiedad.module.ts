import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuscarPropiedadPage } from './buscar-propiedad';

@NgModule({
  declarations: [
    BuscarPropiedadPage,
  ],
  imports: [
    IonicPageModule.forChild(BuscarPropiedadPage),
  ],
})
export class BuscarPropiedadPageModule {}
