import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropiedadRutaDetallePage } from './propiedad-ruta-detalle';

@NgModule({
  declarations: [
    PropiedadRutaDetallePage,
  ],
  imports: [
    IonicPageModule.forChild(PropiedadRutaDetallePage),
  ],
})
export class PropiedadRutaDetallePageModule {}
