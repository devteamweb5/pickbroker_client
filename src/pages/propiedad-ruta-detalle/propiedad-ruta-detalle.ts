import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrRutasProvider} from '../../providers/pr-rutas/pr-rutas';
import { PrPropiedadRutaProvider } from '../../providers/pr-propiedad-ruta/pr-propiedad-ruta'
declare var google: any;
@IonicPage()
@Component({
  selector: 'page-propiedad-ruta-detalle',
  templateUrl: 'propiedad-ruta-detalle.html',
})
export class PropiedadRutaDetallePage {
	inmuebles_ruta:any={
		imagen:'',
    nombre:'',
    direccion:'',
    lat:'',
    long:'',

	}
  propiedad;
  imagenes;
	ruta_imagenes:any;
  map:any;

  constructor(
   public navCtrl: NavController,
   public navParams: NavParams,
   public pr_rutas:PrRutasProvider,
   public pr_propiedad_ruta :PrPropiedadRutaProvider,
   ){
  	this.ruta_imagenes=this.pr_rutas.get_ruta_imagenes();
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad PropiedadRutaDetallePage');
    this.get_propiedad_ruta();
  }
  atras(){
  	this.navCtrl.setRoot('PropiedadRutaMapaPage');
  }
  get_images(id){
    let datos = {'id':id};
    this.pr_propiedad_ruta.get_images(datos).subscribe(result=>{
      this.imagenes = result
      console.log(this.imagenes);
    });
  }
  get_propiedad_ruta(){
    let data=JSON.parse(localStorage.getItem('propiedad_seleccionada'));
    console.log(data);
    this.get_images(data.id);
    this.inmuebles_ruta.imagen=data.imagen;
    this.inmuebles_ruta.nombre=data.nombre;
    this.inmuebles_ruta.comentarios = data.comentarios;
    let lat_long=data.latLong.split(',');
    this.inmuebles_ruta.lat=parseFloat(lat_long['0']);
    this.inmuebles_ruta.long=parseFloat(lat_long['1']);
    console.log(this.inmuebles_ruta.lat);
    console.log(this.inmuebles_ruta.long);
    this.inmuebles_ruta.direccion=data.direccion;
    console.log(this.inmuebles_ruta);
    setTimeout(()=>{
     this.map=this.loadMap();;
     },500);
  }
  loadMap(location= new google.maps.LatLng(20.708799, -103.410071)){
    console.log('mapa cargado');
    let mapOptions={
      center: location,
      zoom:16, 
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    }
    let mapEl=document.getElementById('map_canvas');
    let map= new google.maps.Map(mapEl,mapOptions);
    // Wait the MAP_READY before using any methods.
    navigator.geolocation.getCurrentPosition(
      (position) => {
        let newLatLng = new google.maps.LatLng(this.inmuebles_ruta.lat,this.inmuebles_ruta.long);
        map.setCenter(newLatLng);
        let lat=position.coords.latitude;
        let long=position.coords.longitude;
        var icono ="assets/imgs/pin.png";
        console.log(icono);

        let marker = new google.maps.Marker({
          map: this.map,
          animation: google.maps.Animation.DROP,
          position: newLatLng,
          draggable: false,
          icon:icono
        });
      }
      );
    return map;
  }

}
