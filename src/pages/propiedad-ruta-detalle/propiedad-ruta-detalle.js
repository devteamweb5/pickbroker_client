var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrRutasProvider } from '../../providers/pr-rutas/pr-rutas';
var PropiedadRutaDetallePage = /** @class */ (function () {
    function PropiedadRutaDetallePage(navCtrl, navParams, pr_rutas) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_rutas = pr_rutas;
        this.inmuebles_ruta = {
            imagen: '',
            nombre: '',
            direccion: '',
            lat: '',
            long: '',
        };
        this.ruta_imagenes = this.pr_rutas.get_ruta_imagenes();
    }
    PropiedadRutaDetallePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PropiedadRutaDetallePage');
        this.get_propiedad_ruta();
    };
    PropiedadRutaDetallePage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    PropiedadRutaDetallePage.prototype.get_propiedad_ruta = function () {
        var _this = this;
        var data = JSON.parse(localStorage.getItem('propiedad_seleccionada'));
        this.inmuebles_ruta.imagen = data.imagen;
        this.inmuebles_ruta.nombre = data.nombre;
        var lat_long = data.latLong.split(',');
        this.inmuebles_ruta.lat = parseFloat(lat_long['0']);
        this.inmuebles_ruta.long = parseFloat(lat_long['1']);
        console.log(this.inmuebles_ruta.lat);
        console.log(this.inmuebles_ruta.long);
        this.inmuebles_ruta.direccion = data.direccion;
        console.log(this.inmuebles_ruta);
        setTimeout(function () {
            _this.map = _this.loadMap();
            ;
        }, 500);
    };
    PropiedadRutaDetallePage.prototype.loadMap = function (location) {
        var _this = this;
        if (location === void 0) { location = new google.maps.LatLng(this.inmuebles_ruta.lat, this.inmuebles_ruta.long); }
        var mapOptions = {
            center: location,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };
        var mapEl = document.getElementById('map_canvas');
        var map = new google.maps.Map(mapEl, mapOptions);
        // Wait the MAP_READY before using any methods. 
        navigator.geolocation.getCurrentPosition(function (position) {
            var newLatLng = new google.maps.LatLng(_this.inmuebles_ruta.lat, _this.inmuebles_ruta.long);
            _this.loadMap().setCenter(newLatLng);
            var icono = "assets/imgs/pin.png";
            var marker = new google.maps.Marker({
                map: _this.loadMap(),
                animation: google.maps.Animation.DROP,
                position: newLatLng,
                draggable: false,
                icon: icono
            });
        });
        return map;
    };
    PropiedadRutaDetallePage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-propiedad-ruta-detalle',
            templateUrl: 'propiedad-ruta-detalle.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrRutasProvider])
    ], PropiedadRutaDetallePage);
    return PropiedadRutaDetallePage;
}());
export { PropiedadRutaDetallePage };
//# sourceMappingURL=propiedad-ruta-detalle.js.map