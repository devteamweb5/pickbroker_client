import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DirectorioServiciosDetallePage } from './directorio-servicios-detalle';

@NgModule({
  declarations: [
    DirectorioServiciosDetallePage,
  ],
  imports: [
    IonicPageModule.forChild(DirectorioServiciosDetallePage),
  ],
})
export class DirectorioServiciosDetallePageModule {}
