import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactoEmergenciaAgregarPage } from './contacto-emergencia-agregar';

@NgModule({
  declarations: [
    ContactoEmergenciaAgregarPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactoEmergenciaAgregarPage),
  ],
})
export class ContactoEmergenciaAgregarPageModule {}
