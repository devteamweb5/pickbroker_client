import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrContactoEmergenciaProvider} from '../../providers/pr-contacto-emergencia/pr-contacto-emergencia';

@IonicPage()
@Component({
  selector: 'page-contacto-emergencia-agregar',
  templateUrl: 'contacto-emergencia-agregar.html',
})
export class ContactoEmergenciaAgregarPage {
	data_cliente:any={
		user:'',
		nombre:'',
		telefono:'',
		email:''
	}
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_alert_toast:PrAlertToastProvider, public pr_contacto_emergencia:PrContactoEmergenciaProvider) {
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad ContactoEmergenciaAgregarPage');
  }
  atras(){
  	this.navCtrl.pop();
  }
  guardar_contacto(){
  	if(this.data_cliente.nombre=='') {
  		let mensaje='Ingrese un nombre';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);		
  	}else if(!this.data_cliente.telefono){
  		let mensaje='Ingrese un telefono';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}else if(!this.data_cliente.email){
  		let mensaje='Ingrese un email';
  		this.pr_alert_toast.mensaje_toast_pie(mensaje);
  	}
  	let data_u=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    this.data_cliente.user=data_u.id;
    let mensaje='Guardando';
    this.pr_alert_toast.show_loading(mensaje);
    this.pr_contacto_emergencia.guardar_contactos_emergencia(this.data_cliente).subscribe(
      result => {
        this.pr_alert_toast.dismis_loading();
        this.pr_alert_toast.mensaje_toast_pie('Contacto guardado');
        this.navCtrl.pop();
      },
      err => {console.log('el error '+err);
    },
    );
  }

}
