var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrContactoEmergenciaProvider } from '../../providers/pr-contacto-emergencia/pr-contacto-emergencia';
var ContactoEmergenciaAgregarPage = /** @class */ (function () {
    function ContactoEmergenciaAgregarPage(navCtrl, navParams, pr_alert_toast, pr_contacto_emergencia) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_contacto_emergencia = pr_contacto_emergencia;
        this.data_cliente = {
            user: '',
            nombre: '',
            telefono: '',
            email: ''
        };
    }
    ContactoEmergenciaAgregarPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactoEmergenciaAgregarPage');
    };
    ContactoEmergenciaAgregarPage.prototype.atras = function () {
        this.navCtrl.pop();
    };
    ContactoEmergenciaAgregarPage.prototype.guardar_contacto = function () {
        var _this = this;
        if (this.data_cliente.nombre == '') {
            var mensaje_1 = 'Ingrese un nombre';
            this.pr_alert_toast.mensaje_toast_pie(mensaje_1);
        }
        else if (!this.data_cliente.telefono) {
            var mensaje_2 = 'Ingrese un telefono';
            this.pr_alert_toast.mensaje_toast_pie(mensaje_2);
        }
        else if (!this.data_cliente.email) {
            var mensaje_3 = 'Ingrese un email';
            this.pr_alert_toast.mensaje_toast_pie(mensaje_3);
        }
        var data_u = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        this.data_cliente.user = data_u.id;
        var mensaje = 'Guardando';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_contacto_emergencia.guardar_contactos_emergencia(this.data_cliente).subscribe(function (result) {
            _this.pr_alert_toast.dismis_loading();
            _this.pr_alert_toast.mensaje_toast_pie('Contacto guardado');
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    ContactoEmergenciaAgregarPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-contacto-emergencia-agregar',
            templateUrl: 'contacto-emergencia-agregar.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrAlertToastProvider, PrContactoEmergenciaProvider])
    ], ContactoEmergenciaAgregarPage);
    return ContactoEmergenciaAgregarPage;
}());
export { ContactoEmergenciaAgregarPage };
//# sourceMappingURL=contacto-emergencia-agregar.js.map