import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CitaEnviadaPage } from './cita-enviada';

@NgModule({
  declarations: [
    CitaEnviadaPage,
  ],
  imports: [
    IonicPageModule.forChild(CitaEnviadaPage),
  ],
})
export class CitaEnviadaPageModule {}
