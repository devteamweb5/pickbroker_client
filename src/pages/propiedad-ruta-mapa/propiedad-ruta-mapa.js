var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrAlertToastProvider } from '../../providers/pr-alert-toast/pr-alert-toast';
import { PrPropiedadRutaProvider } from '../../providers/pr-propiedad-ruta/pr-propiedad-ruta';
var PropiedadRutaMapaPage = /** @class */ (function () {
    function PropiedadRutaMapaPage(navCtrl, navParams, pr_alert_toast, pr_propiedad_ruta) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.pr_alert_toast = pr_alert_toast;
        this.pr_propiedad_ruta = pr_propiedad_ruta;
        this.muestra = 'none';
    }
    PropiedadRutaMapaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PropiedadRutaMapaPage');
    };
    PropiedadRutaMapaPage.prototype.atras = function () {
        this.navCtrl.setRoot('HomePage');
    };
    PropiedadRutaMapaPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        setTimeout(function () {
            _this.map = _this.loadMap();
            ;
        }, 500);
    };
    PropiedadRutaMapaPage.prototype.nueva_propiedad = function () {
        this.navCtrl.push('PropiedadRutaGuardarPage');
    };
    PropiedadRutaMapaPage.prototype.loadMap = function (location) {
        if (location === void 0) { location = new google.maps.LatLng(31.69036380000001, -106.42454780000003); }
        var mapOptions = {
            center: location,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };
        var mapEl = document.getElementById('map_canvas');
        var map = new google.maps.Map(mapEl, mapOptions);
        // Wait the MAP_READY before using any methods.
        /*
        navigator.geolocation.getCurrentPosition(
         (position) => {
           let newLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
           this.map.setCenter(newLatLng);
         }
        );
        */
        this.get_propiedades_ruta();
        return map;
    };
    PropiedadRutaMapaPage.prototype.get_propiedades_ruta = function () {
        var _this = this;
        var data_u = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        var data = { "user": data_u.id };
        var mensaje = 'Cargando';
        this.pr_alert_toast.show_loading(mensaje);
        this.pr_propiedad_ruta.get_propiedad_ruta_id_usuario(data).subscribe(function (result) {
            _this.pr_alert_toast.dismis_loading();
            console.log(result);
            var latitude;
            var longitude;
            var lat_long;
            for (var _i = 0, result_1 = result; _i < result_1.length; _i++) {
                var value = result_1[_i];
                console.log(value);
                lat_long = value.latLong.split(',');
                latitude = lat_long['0'];
                longitude = lat_long['1'];
                var newLatLng = new google.maps.LatLng(latitude, longitude);
                _this.Position_Marker(newLatLng, value);
            }
        }, function (err) {
            console.log('el error ' + err);
        });
    };
    PropiedadRutaMapaPage.prototype.Position_Marker = function (item, item_2) {
        var _this = this;
        var lat_lng = item;
        var datos = item_2;
        console.log('entra en position_marker');
        var icono = "assets/imgs/pin.png";
        var marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: lat_lng,
            draggable: false,
            icon: icono
        });
        google.maps.event.addListener(marker, 'click', function () {
            _this.mostrar_propiedad_ruta(datos);
        });
    };
    PropiedadRutaMapaPage.prototype.mostrar_propiedad_ruta = function (item) {
        this.item_sel = item;
        console.log(this.item_sel);
        this.nombre_propiedad = 'mi propiedad';
        this.direccion = this.item_sel.direccion;
        var lat_long = this.item_sel.latLong.split(',');
        this.lat_propiedad = lat_long['0'];
        this.lng_propiedad = lat_long['1'];
        this.muestra = 'block';
        var mapa = document.getElementById('mapa_2');
        localStorage.setItem('propiedad_seleccionada', JSON.stringify(this.item_sel));
        mapa.classList.add('bounceIn');
    };
    PropiedadRutaMapaPage.prototype.seleccionar_propiedad = function () {
        this.navCtrl.push('PropiedadRutaDetallePage');
    };
    PropiedadRutaMapaPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-propiedad-ruta-mapa',
            templateUrl: 'propiedad-ruta-mapa.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, PrAlertToastProvider, PrPropiedadRutaProvider])
    ], PropiedadRutaMapaPage);
    return PropiedadRutaMapaPage;
}());
export { PropiedadRutaMapaPage };
//# sourceMappingURL=propiedad-ruta-mapa.js.map