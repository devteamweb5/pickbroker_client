import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {PrAlertToastProvider} from '../../providers/pr-alert-toast/pr-alert-toast';
import {PrPropiedadRutaProvider} from '../../providers/pr-propiedad-ruta/pr-propiedad-ruta';
declare var google: any;


@IonicPage()
@Component({
  selector: 'page-propiedad-ruta-mapa',
  templateUrl: 'propiedad-ruta-mapa.html',
})
export class PropiedadRutaMapaPage {
	 map:any;
	 item_sel:any;
	 nombre_propiedad:any;
	 direccion:any;
	 lat_propiedad:any;
	 lng_propiedad:any;
	 muestra:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public pr_alert_toast:PrAlertToastProvider, public pr_propiedad_ruta:PrPropiedadRutaProvider) {
  	this.muestra='none';
  }
  ionViewDidLoad(){
    console.log('ionViewDidLoad PropiedadRutaMapaPage');
  }
  atras(){
    this.navCtrl.setRoot('HomePage');
  }
  ionViewWillEnter(){
    setTimeout(()=>{
     this.map=this.loadMap();;
     },500);
  }
  nueva_propiedad(){
  	this.navCtrl.push('PropiedadRutaGuardarPage');
  }
  loadMap(location= new google.maps.LatLng(31.69036380000001,-106.42454780000003)){
    let mapOptions={
        center: location,
        zoom:16, 
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true
    }
    let mapEl=document.getElementById('map_canvas');
     let map= new google.maps.Map(mapEl,mapOptions);
     // Wait the MAP_READY before using any methods.
     
     navigator.geolocation.getCurrentPosition(
      (position) => {
        let newLatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        this.map.setCenter(newLatLng);
        this.get_propiedades_ruta();
      }
     );
     
    return map;
  }
  get_propiedades_ruta(){
  	let data_u=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    let data = {"user":data_u.id}
    let mensaje='Cargando';
    this.pr_alert_toast.show_loading(mensaje);
     this.pr_propiedad_ruta.get_propiedad_ruta_id_usuario(data).subscribe(
        result => {
          this.pr_alert_toast.dismis_loading();
            console.log(result);
            let latitude;
            let longitude;
            let lat_long;
            for(let value of result) {
              console.log(value)
            	lat_long=value.latLong.split(',');
              latitude=lat_long['0'];
              longitude=lat_long['1'];
              let newLatLng = new google.maps.LatLng(latitude, longitude);
              this.Position_Marker(newLatLng,value);
             }
        },
        err => {console.log('el error '+err);
        },
      );
  }
  Position_Marker(item,item_2){
   let lat_lng=item;
   let datos=item_2;
    console.log('entra en position_marker');
   var icono ="assets/imgs/pin.png";
   let marker = new google.maps.Marker({
    map: this.map,
    animation: google.maps.Animation.DROP,
    position: lat_lng,
    draggable: false,
    icon:icono
  });
    google.maps.event.addListener(marker, 'click', () => {
      this.mostrar_propiedad_ruta(datos);   
    });
  }
   mostrar_propiedad_ruta(item){
    this.item_sel=item;
    this.nombre_propiedad='mi propiedad';
    this.direccion=this.item_sel.direccion;
    let lat_long=this.item_sel.latLong.split(',');
    this.lat_propiedad=lat_long['0'];
    this.lng_propiedad=lat_long['1'];
    this.muestra='block';
    let mapa=document.getElementById('mapa_2');
     localStorage.setItem('propiedad_seleccionada',JSON.stringify(this.item_sel));
     mapa.classList.add('bounceIn');
  }
  seleccionar_propiedad(){
  	this.navCtrl.setRoot('PropiedadRutaDetallePage');
  }

}
