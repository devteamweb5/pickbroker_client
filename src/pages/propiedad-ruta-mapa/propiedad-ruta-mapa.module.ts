import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropiedadRutaMapaPage } from './propiedad-ruta-mapa';

@NgModule({
  declarations: [
    PropiedadRutaMapaPage,
  ],
  imports: [
    IonicPageModule.forChild(PropiedadRutaMapaPage),
  ],
})
export class PropiedadRutaMapaPageModule {}
