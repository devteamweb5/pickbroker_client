import { Component, ViewChild } from '@angular/core';
import { Nav,Platform,  AlertController, LoadingController, Events, ActionSheetController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { OneSignal } from '@ionic-native/onesignal';
import {PrRutasProvider} from '../providers/pr-rutas/pr-rutas';
import {PrLoginProvider} from '../providers/pr-login/pr-login';
import { Device } from '@ionic-native/device';
declare var google: any;

import * as firebase from 'firebase';
var config = {
  apiKey: "AIzaSyBU5t3t8rwBtT-VfNCO_7tIcbeYxGbFUQU",
  authDomain: "pickbroker-16fe3.firebaseapp.com",
  databaseURL: "https://pickbroker-16fe3.firebaseio.com",
  projectId: "pickbroker-16fe3",
  storageBucket: "pickbroker-16fe3.appspot.com",
  messagingSenderId: "844572135696",
  appId: "1:844572135696:web:c5865a9a69370370"
}
@Component({
  templateUrl: 'app.html'
})
export class MyApp{
  @ViewChild(Nav) nav: Nav;
  rootPage:any =(localStorage.getItem('data_cliente_pickbroker')) ? 'HomePage' : 'LoginPage';
  loader:any;
  nombre:any;
  imagen:any;
  ruta_imagenes:any;
  myAddress;
  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public oneSignal: OneSignal,
    public alertCtrl:AlertController,
    public actionSheetCtrl:ActionSheetController,
    public pr_rutas:PrRutasProvider,
    public pr_login:PrLoginProvider,
    public loadingCtrl:LoadingController,
    public events: Events,
    public device:Device)
  {
    if (localStorage.getItem('data_cliente_pickbroker')) {
      let user=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
      this.nombre = user.name;
      this.imagen = user.image;
    }
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log(position);
        let geocoder = new google.maps.Geocoder();
        var latlng = {lat:position.coords.latitude, lng:position.coords.longitude};
        geocoder.geocode({'location': latlng},(results, status) => {
          this.myAddress = results[1].formatted_address;
          console.log(results);
          console.log(status);
        });
      }
      );
    this.ruta_imagenes=this.pr_rutas.get_ruta_imagenes();
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.events.subscribe('user:created', (item) =>{
        let data=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        if(data) {
          this.nombre = data.name;
          this.imagen = data.image;
        }
      });
      firebase.initializeApp(config);
      if (platform.is('cordova')){
        this.notificaciones();
      }else{
        this.pr_login.set_serial(1234);
        this.pr_login.set_token(1);
      }
    });
  }
  private notificaciones(){
    this.oneSignal.startInit('23545c1a-11e7-4b74-af1e-d855f5c39cc3', '844572135696'); //(appId_onesignal,googleProjectNumber)
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    this.oneSignal.handleNotificationOpened()
    .subscribe(jsonData =>{
      let datos_adicionales=jsonData.notification.payload.additionalData;
      let data=JSON.stringify(datos_adicionales);
      let status_cita=datos_adicionales.id_status_cita;
      if(status_cita==1) {
        let alert = this.alertCtrl.create({
          title: jsonData.notification.payload.title,
          subTitle: jsonData.notification.payload.body,
          buttons: [
          {
            text: 'Si',
            handler: data =>{
              this.enviar_mensaje_socio(datos_adicionales);
            }
          },
          {
            text: 'No',
            handler: data => {
            }
          }
          ]
        });
        alert.present();
      }else{
        let alert = this.alertCtrl.create({
          title: jsonData.notification.payload.title,
          subTitle: jsonData.notification.payload.body,
          buttons: ['OK']
        });
        alert.present();
      }
      console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    });
    this.oneSignal.endInit();
    this.presentLoading_carga();
    this.oneSignal.getIds().then((id)=>{
      let el_id=id.userId; /*el id para guardarlo en el token de la base de datos*/
      this.pr_login.set_token(el_id);
      this.pr_login.set_serial(this.device.uuid);
      this.loader.dismiss();
    })
  }
  login(){
    let data=JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
    console.log(data)
    if(data) {
      for(let value of data) {
        this.nombre=value.nombre;
        this.imagen=value.imagen;
      }
    }
  }
  presentLoading_carga(){
    this.loader = this.loadingCtrl.create({
      content: "Cargando, espere un momento por favor..."
    });
    this.loader.present();
  }
  perfil(){
    this.nav.push('PerfilPage');
  }
  home(){
    this.nav.setRoot('HomePage');
  }
  
  servicios(){
    this.nav.setRoot('ServicioListaPage');
  }
  historial_llamadas(){
    this.nav.push('LlamadasHistorialPage');
  }
  propiedades(){
    this.nav.setRoot('MisPropiedadesListaPage');
  }
  propiedades_favoritas(){
    this.nav.setRoot('PropiedadRutaMapaPage');
  }
  propiedades_favoritas_2(){
    this.nav.setRoot('PropiedadFavoritaPage');
  }
  buscar_propiedades(){
    this.nav.setRoot('BuscarPropiedadPage');
  }
  citas(){
    this.nav.setRoot('CitaListaPage');
  }
  citas_pendientes(){
    this.nav.setRoot('CitasPendientesPage');
  }
  mensajes_pendientes(){
    this.nav.setRoot('MensajePendientesChatPage');
  }
  contacto_emergencia(){
    this.nav.setRoot('ContactoEmergenciaListaPage');
  }
  enviar_mensaje_socio(item){
    let datos=item;
    localStorage.setItem('datos_cita_mensaje',JSON.stringify(datos));
  }
  preguntas_frecuentes(){
    this.nav.setRoot('PreguntasFrecuentesPage');
  }
  politicas_privacidad(){
    this.nav.push('PoliticasPrivacidadPage');
  }
  cerrar_sesion(){
    localStorage.removeItem('data_cliente_pickbroker');
    this.nav.setRoot('LoginPage');
  }
}

