var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { OneSignal } from '@ionic-native/onesignal';
import { HttpModule } from '@angular/http';
import { Device } from '@ionic-native/device';
import { MyApp } from './app.component';
import { PrAlertToastProvider } from '../providers/pr-alert-toast/pr-alert-toast';
import { PrLoginProvider } from '../providers/pr-login/pr-login';
import { PrRutasProvider } from '../providers/pr-rutas/pr-rutas';
import { PrInmuebleProvider } from '../providers/pr-inmueble/pr-inmueble';
import { PrTerminosPoliticasProvider } from '../providers/pr-terminos-politicas/pr-terminos-politicas';
import { PrPreguntasFrecuentesProvider } from '../providers/pr-preguntas-frecuentes/pr-preguntas-frecuentes';
import { PrPerfilProvider } from '../providers/pr-perfil/pr-perfil';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { PrServicioProvider } from '../providers/pr-servicio/pr-servicio';
import { PrTipoInmueblePublicacionProvider } from '../providers/pr-tipo-inmueble-publicacion/pr-tipo-inmueble-publicacion';
import { CallNumber } from '@ionic-native/call-number';
import { PrLlamadasProvider } from '../providers/pr-llamadas/pr-llamadas';
import { PrCitasProvider } from '../providers/pr-citas/pr-citas';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { PrContactoEmergenciaProvider } from '../providers/pr-contacto-emergencia/pr-contacto-emergencia';
import { PrPropiedadRutaProvider } from '../providers/pr-propiedad-ruta/pr-propiedad-ruta';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SocialSharing } from '@ionic-native/social-sharing';
import { PrMembresiaProvider } from '../providers/pr-membresia/pr-membresia';
import { PrTarjetasProvider } from '../providers/pr-tarjetas/pr-tarjetas';
import { ImagePicker } from '@ionic-native/image-picker';
import { FileTransfer } from '@ionic-native/file-transfer';
import { WebView } from '@ionic-native/ionic-webview/ngx';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp,
            ],
            imports: [
                BrowserModule,
                HttpModule,
                IonicModule.forRoot(MyApp)
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp,
            ],
            providers: [
                StatusBar,
                SplashScreen,
                OneSignal,
                { provide: ErrorHandler, useClass: IonicErrorHandler },
                PrAlertToastProvider,
                PrLoginProvider,
                PrRutasProvider,
                Device,
                PrInmuebleProvider,
                PrTerminosPoliticasProvider,
                PrPreguntasFrecuentesProvider,
                PrPerfilProvider,
                File,
                Transfer,
                FilePath,
                Camera,
                PrServicioProvider,
                PrTipoInmueblePublicacionProvider,
                CallNumber,
                PrLlamadasProvider,
                PrCitasProvider,
                LaunchNavigator,
                PrContactoEmergenciaProvider,
                PrPropiedadRutaProvider,
                PhotoViewer,
                InAppBrowser,
                SocialSharing,
                PrMembresiaProvider,
                PrTarjetasProvider,
                ImagePicker,
                FileTransfer,
                WebView,
            ]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map