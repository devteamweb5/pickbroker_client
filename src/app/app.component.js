var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController, LoadingController, Events, ActionSheetController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { OneSignal } from '@ionic-native/onesignal';
import { PrRutasProvider } from '../providers/pr-rutas/pr-rutas';
import { PrLoginProvider } from '../providers/pr-login/pr-login';
import { Device } from '@ionic-native/device';
import * as firebase from 'firebase';
var config = {
    apiKey: "AIzaSyBU5t3t8rwBtT-VfNCO_7tIcbeYxGbFUQU",
    authDomain: "pickbroker-16fe3.firebaseapp.com",
    databaseURL: "https://pickbroker-16fe3.firebaseio.com",
    projectId: "pickbroker-16fe3",
    storageBucket: "pickbroker-16fe3.appspot.com",
    messagingSenderId: "844572135696",
    appId: "1:844572135696:web:c5865a9a69370370"
};
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, oneSignal, alertCtrl, actionSheetCtrl, pr_rutas, pr_login, loadingCtrl, events, device) {
        var _this = this;
        this.oneSignal = oneSignal;
        this.alertCtrl = alertCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.pr_rutas = pr_rutas;
        this.pr_login = pr_login;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
        this.device = device;
        this.rootPage = (localStorage.getItem('data_cliente_pickbroker')) ? 'HomePage' : 'LoginPage';
        navigator.geolocation.getCurrentPosition(function (position) {
            console.log(position);
            var geocoder = new google.maps.Geocoder();
            var latlng = { lat: position.coords.latitude, lng: position.coords.longitude };
            geocoder.geocode({ 'location': latlng }, function (results, status) {
                _this.myAddress = results[1].formatted_address;
                console.log(results);
                console.log(status);
            });
        });
        this.ruta_imagenes = this.pr_rutas.get_ruta_imagenes();
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            _this.events.subscribe('user:created', function (item) {
                var data = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
                if (data) {
                    _this.nombre = data.name;
                    _this.imagen = data.image;
                }
            });
            firebase.initializeApp(config);
            if (platform.is('cordova')) {
                _this.notificaciones();
            }
            else {
                _this.pr_login.set_serial(1234);
                _this.pr_login.set_token(1);
            }
        });
    }
    MyApp.prototype.notificaciones = function () {
        var _this = this;
        this.oneSignal.startInit('23545c1a-11e7-4b74-af1e-d855f5c39cc3', '844572135696'); //(appId_onesignal,googleProjectNumber)
        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
        this.oneSignal.handleNotificationOpened()
            .subscribe(function (jsonData) {
            var datos_adicionales = jsonData.notification.payload.additionalData;
            var data = JSON.stringify(datos_adicionales);
            var status_cita = datos_adicionales.id_status_cita;
            if (status_cita == 1) {
                var alert_1 = _this.alertCtrl.create({
                    title: jsonData.notification.payload.title,
                    subTitle: jsonData.notification.payload.body,
                    buttons: [
                        {
                            text: 'Si',
                            handler: function (data) {
                                _this.enviar_mensaje_socio(datos_adicionales);
                            }
                        },
                        {
                            text: 'No',
                            handler: function (data) {
                            }
                        }
                    ]
                });
                alert_1.present();
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: jsonData.notification.payload.title,
                    subTitle: jsonData.notification.payload.body,
                    buttons: ['OK']
                });
                alert_2.present();
            }
            console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
        });
        this.oneSignal.endInit();
        this.presentLoading_carga();
        this.oneSignal.getIds().then(function (id) {
            var el_id = id.userId; /*el id para guardarlo en el token de la base de datos*/
            _this.pr_login.set_token(el_id);
            _this.pr_login.set_serial(_this.device.uuid);
            _this.loader.dismiss();
        });
    };
    MyApp.prototype.login = function () {
        var data = JSON.parse(localStorage.getItem('data_cliente_pickbroker'));
        console.log(data);
        if (data) {
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                var value = data_1[_i];
                this.nombre = value.nombre;
                this.imagen = value.imagen;
            }
        }
    };
    MyApp.prototype.presentLoading_carga = function () {
        this.loader = this.loadingCtrl.create({
            content: "Cargando, espere un momento por favor..."
        });
        this.loader.present();
    };
    MyApp.prototype.perfil = function () {
        this.nav.push('PerfilPage');
    };
    MyApp.prototype.home = function () {
        this.nav.setRoot('HomePage');
    };
    MyApp.prototype.servicios = function () {
        this.nav.setRoot('ServicioListaPage');
    };
    MyApp.prototype.historial_llamadas = function () {
        this.nav.push('LlamadasHistorialPage');
    };
    MyApp.prototype.propiedades = function () {
        this.nav.setRoot('MisPropiedadesListaPage');
    };
    MyApp.prototype.propiedades_favoritas = function () {
        this.nav.setRoot('PropiedadRutaMapaPage');
    };
    MyApp.prototype.propiedades_favoritas_2 = function () {
        this.nav.setRoot('PropiedadFavoritaPage');
    };
    MyApp.prototype.buscar_propiedades = function () {
        this.nav.setRoot('BuscarPropiedadPage');
    };
    MyApp.prototype.citas = function () {
        this.nav.setRoot('CitaListaPage');
    };
    MyApp.prototype.citas_pendientes = function () {
        this.nav.setRoot('CitasPendientesPage');
    };
    MyApp.prototype.mensajes_pendientes = function () {
        this.nav.setRoot('MensajePendientesChatPage');
    };
    MyApp.prototype.contacto_emergencia = function () {
        this.nav.setRoot('ContactoEmergenciaListaPage');
    };
    MyApp.prototype.enviar_mensaje_socio = function (item) {
        var datos = item;
        localStorage.setItem('datos_cita_mensaje', JSON.stringify(datos));
    };
    MyApp.prototype.preguntas_frecuentes = function () {
        this.nav.setRoot('PreguntasFrecuentesPage');
    };
    MyApp.prototype.politicas_privacidad = function () {
        this.nav.push('PoliticasPrivacidadPage');
    };
    MyApp.prototype.cerrar_sesion = function () {
        localStorage.removeItem('data_cliente_pickbroker');
        this.nav.setRoot('LoginPage');
    };
    __decorate([
        ViewChild(Nav),
        __metadata("design:type", Nav)
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Component({
            templateUrl: 'app.html'
        }),
        __metadata("design:paramtypes", [Platform, StatusBar, SplashScreen, OneSignal, AlertController, ActionSheetController, PrRutasProvider, PrLoginProvider, LoadingController, Events, Device])
    ], MyApp);
    return MyApp;
}());
export { MyApp };
//# sourceMappingURL=app.component.js.map